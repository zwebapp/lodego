<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
ob_start();
include("config.php");
$id=$_GET["id"];
if(!is_numeric($id)) die("");
$flag = true;
$image_types = Array 
(
	array("image/jpeg","jpg"),
	array("image/pjpeg","jpg"),
	array("image/bmp","bmp"),
	array("image/gif","gif"),
	array("image/x-png","png")
);
foreach($image_types as $image_type)
{
	if(file_exists("uploaded_images/".$id.".".$image_type[1])) 
	{
				Header ("Content-type: ".$image_type[0]);
				$handle = fopen("uploaded_images/".$id.".".$image_type[1], "rb");
				$contents = fread($handle, filesize("uploaded_images/".$id.".".$image_type[1]));
				fclose($handle);
				print $contents;
				$flag=false;
	}
}

if($flag)
{
				mysql_connect("$DBHost","$DBUser","$DBPass");
				mysql_select_db ($DBName) or die ("DB does not exist or access is denied!");
				
				$sql = "SELECT * FROM ".$DBprefix."image WHERE image_id=".$id;
				
				$result = mysql_query ($sql);
				
				mysql_close();
				
				if (mysql_num_rows ($result)>0) 
				{
					$row = @mysql_fetch_array ($result);
					$image_type = $row["image_type"];
					$image = $row["image"];
					Header ("Content-type: $image_type");
					print $image;
				}
				else
				{
					header ("Content-type: image/gif");
					$handle = fopen("images/no_pic.gif", "rb");
					$contents = fread($handle, filesize("images/no_pic.gif"));
					fclose($handle);
					print $contents;
				
				}
				
}
ob_end_flush();
?>