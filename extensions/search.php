<?php

if(!defined('IN_SCRIPT')) die("");
$RESULTS_PER_PAGE = $website->GetParam("RESULTS_PER_PAGE");
$NUMBER_OF_CATEGORIES_PER_ROW = $website->GetParam("NUMBER_OF_CATEGORIES_PER_ROW");

if(isset($_REQUEST["location"]))
{
	$location=trim($_REQUEST["location"]);
	
	$website->ms_i(str_replace("-","",$location));
	
	$location=str_replace("-",".",$location);
		
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
		".$DBprefix."listings.status = 1
		AND
		 (".$DBprefix."listings.location='".$location."' 
		 OR 
		 ".$DBprefix."listings.location LIKE '".$location.".%'
		 )
		
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date
	");
}
else
if(isset($_REQUEST["refine"]))
{

	$category=trim($_REQUEST["category"]);
	
	$website->ms_i(str_replace("-","",$category));
	
	$category=str_replace("-",".",$category);
		
	$arrPValues = array();
	$link_cat = $selected_cat_id = $category; 
	
	$where_string = "cat_id='".$link_cat."'";
	$str_cat_items = explode(".",$link_cat);
	
	for($i=(sizeof($str_cat_items)-1);$i>=0;$i--)
	{
		$link_cat = substr($link_cat,0,strlen($link_cat)-strlen($str_cat_items[$i])-1);
		$where_string .= " OR cat_id='".$link_cat."'";
	}

	
	$category_fields_table= $database->DataTable("fields","WHERE ".$where_string);
	
	while($category_fields = mysql_fetch_array($category_fields_table))
	{
		if(isset($category_fields["id"]))
		{
			$arr_cat_fields = unserialize(stripslashes($category_fields["fields"]));
			$current_cat = str_replace(".","-",$category_fields["cat_id"]);
			
			if(is_array($arr_cat_fields))
			{
				$iFCounter = 0;
			
				foreach($arr_cat_fields as $arr_cat_field)
				{	
				
					if(isset($_POST["pfield-".$current_cat."-".$iFCounter])&&trim($_POST["pfield-".$current_cat."-".$iFCounter])!="")
					{
						$arrPValues[$arr_cat_field[0]]=trim(strip_tags($_POST["pfield-".$current_cat."-".$iFCounter]));
						$iFCounter++;
					}
					$iFCounter++;
				}	
			}
		}
	}	
	
	$fields_where = "";
	
	foreach($arrPValues as $key=>$value)
	{
		$fields_where .= "fields LIKE '%\"".$key."\";s:%:\"%".$value."%\"%' AND ";
	}
	
	
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".$fields_where."
		".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
		".$DBprefix."listings.status = 1
		AND
		(
		link_category='".$category."' 
		OR 
		link_category LIKE '".$category.".%'
		)
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date
	");
}
else
if(isset($_REQUEST["category"]))
{

	$category=trim($_REQUEST["category"]);
	
	$website->ms_i(str_replace("-","",$category));
	
	$category=str_replace("-",".",$category);
		
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
		".$DBprefix."listings.status = 1
		AND
		(
		link_category='".$category."' 
		OR 
		link_category LIKE '".$category.".%'
		)
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date
	");
}
else
if(isset($_REQUEST["user_ads"]))
{
	$user = $_REQUEST["user"];
	$website->ms_ew($user);
	
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
		".$DBprefix."listings.status = 1
		AND
		username='".$user."' 
		
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date
	");
	
	$arr_users = $database->DataArray("users","username='".$user."'");
	
	if(isset($arr_users["id"]))
	{
		$user_title = $arr_users["business_name"]." ".$M_COUPONS_DISCOUNTS;
		$website->Title(stripslashes(strip_tags($user_title)));
		$website->MetaDescription($website->text_words(stripslashes(strip_tags($user_title)),30));
		$website->MetaKeywords($website->format_keywords($website->text_words(stripslashes(strip_tags($user_title)),20)));

	?>
		<h3>
		<?php
		echo $user_title;
		?>
		</h3>
		<hr/>
	<?php
	}
	
}
else
if(isset($_REQUEST["search_by"]))
{
	$search_by = $website->sanitize(trim($_REQUEST["search_by"]));
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
		".$DBprefix."listings.status = 1
		AND
		(
			title like '%".mysql_real_escape_string($search_by)."%' 
			OR 
			description LIKE '%".mysql_real_escape_string($search_by)."%'
			OR 
			business_name LIKE '%".mysql_real_escape_string($search_by)."%'
			
		)
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date
	");
	
	$website->Title($M_SEARCH_BY." - ".$search_by);
	$website->MetaDescription($M_SEARCH_BY." - ".$search_by);
	$website->MetaKeywords("");
	
	?>
	<h3><?php echo $M_SEARCH_BY;?>: <?php echo $search_by;?></h3>
	<div class="clear"></div>
	<br/>
	
	
	<?php
}
else
{
	die("No search parameters defined");
}


///start selected location
if(isset($location))
{
	if(file_exists("locations/locations_array.php")) include_once("locations/locations_array.php");
	elseif(file_exists("../locations/locations_array.php")) include_once("../locations/locations_array.php");
		
	$selected_location_name = $website->show_full_location($location);
	
	$website->Title($selected_location_name." ".$website->GetParam("SEO_APPEND_TITLE"));
	$website->MetaDescription($selected_location_name." ".$website->GetParam("SEO_APPEND_DESCRIPTION"));
	$website->MetaKeywords($website->format_keywords($selected_location_name." ".$website->GetParam("SEO_APPEND_KEYWORDS")));
			
	echo $M_SELECTED_LOCATION.": <b>
				
		".$selected_location_name."
		
		</b><br/><br/>";
	$selected_region=$location;
	$l_slice=array();
	$array_l=explode(".",$selected_region);

	
	switch (sizeof($array_l)) 
	{
		case 1:
			if(isset($l1[$array_l[0]])) $l_slice=$l1[$array_l[0]];
        break;
		case 2:
			if(isset($l2[$array_l[0]][$array_l[1]])) $l_slice=$l2[$array_l[0]][$array_l[1]];
        break;
		case 3:
			if(isset($l3[$array_l[0]][$array_l[1]][$array_l[2]]))  $l_slice=$l3[$array_l[0]][$array_l[1]][$array_l[2]];
		case 4:
			if(isset($l4[$array_l[0]][$array_l[1]][$array_l[2]][$array_l[3]]))  $l_slice=$l4[$array_l[0]][$array_l[1]][$array_l[2]][$array_l[3]];
        break;
  	}
	
	$iCounter = 0;
	if(isset($l_slice)&&is_array($l_slice))
	{
		foreach($l_slice as $c_key=>$l_slice_element)
		{
						
			if($website->GetParam("SEO_URLS")==1)
			{
				$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_LOCATION:"location")."-".$website->format_str($l_slice_element)."-".str_replace(".","-",$selected_region."-".$c_key).".html";
			}
			else
			{
				$strLink = "index.php?mod=search&location=".str_replace(".","-",$selected_region."-".$c_key).($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
			}
			
			if($iCounter==($NUMBER_OF_CATEGORIES_PER_ROW*5))
			{
				echo "<div id=\"sub_locations\" style=\"display:none\">";
			}
			
			echo "<div style=\"float:left;width:".round(100/$NUMBER_OF_CATEGORIES_PER_ROW,2)."%\">\n";	
				
			echo "<b><a href=\"".$strLink."\" style='text-decoration:none;font-weight:400'>".$l_slice_element."</a></b>";
			echo "</div>";
			
			if(($iCounter+1)%$NUMBER_OF_CATEGORIES_PER_ROW==0)
			{
				echo "<div class=\"clear\"></div>";
			}
			$iCounter ++;
		}
		
		if($iCounter>($NUMBER_OF_CATEGORIES_PER_ROW*5))
		{
			echo "</div>";
			?>
			<div class="clear"></div>
			<script>
			function ShowSubLocations()
			{
				document.getElementById("sub_locations").style.display="block";
				document.getElementById("sub_loc_points").innerHTML="";
				document.getElementById("sub_loc_show").innerHTML="";
			}
			
			</script>
			<span class="lfloat" id="sub_loc_points">...........</span>
			<span class="rfloat" id="sub_loc_show" style="position:relative;left:-10px;top:-5px"><a href="javascript:ShowSubLocations()"><?php echo $M_SEE_ALL;?></a></span>
			
			<div class="clear"></div>
			<br>
			<?php
		}
	}

}
///end selected location



///start selected category
if(isset($category))
{

	
	$arr_listings_count = array();

	if($website->GetParam("SHOW_LISTINGS_NUMBER")==1)
	{

		$count_listings = $database->Query
		("
			SELECT count(id) c, 
			link_category 
			FROM ".$DBprefix."listings
			WHERE
			".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
			status=1 AND
			link_category LIKE '".$category.".%'
			GROUP BY link_category
		");

		while($count_listing = mysql_fetch_array($count_listings))
		{
			$c_link_category = $count_listing["link_category"];
			$c_level = substr_count($category, '.');
			
			$c_items = array_slice(explode('.', $c_link_category),0,$c_level+2);
			$category_id = implode('.',$c_items);
			if(!isset($arr_listings_count[$category_id]))
			{
				$arr_listings_count[$category_id]=0;
			}
			$arr_listings_count[$category_id]  += $count_listing["c"];
		}

	}

	if(file_exists('categories/categories_'.strtolower($website->lang).'.php'))
	{
		$categories_content = file_get_contents('categories/categories_'.strtolower($website->lang).'.php');
	}
	else
	{
		$categories_content = file_get_contents('categories/categories_en.php');
	}

	$arrCategories = explode("\n", trim($categories_content));

	$categories=array();
	
	foreach($arrCategories as $str_category)
	{
		list($key,$value)=explode(". ",$str_category);
		$categories["".trim($key)]=trim($value);
	}
	asort($categories);
	echo "<h3>".$M_CATEGORY.": ";
	$seo_category_title="";
	if(substr_count($category,".")>0)
	{
		$category_items = explode(".",$category);
		$current_id="";
		$b_first = true;
		for($i=0;$i<sizeof($category_items)-1;$i++)
		{
			if(!$b_first) $current_id.=".";
			$current_id .= $category_items[$i];
			
			$b_first = false;
			
			$current_category = $categories[$current_id];
			
			if($website->GetParam("SEO_URLS")==1)
			{
				$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_CATEGORY:"category")."-".$website->format_str($current_category)."-".str_replace(".","-",$current_id).".html";
			}
			else
			{
				$strLink = "index.php?mod=search&category=".str_replace(".","-",$current_id).($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
			}
			
			echo "<a href=\"".$strLink."\">".$current_category."</a>"." > ";
			$seo_category_title.=$current_category." - ";
		}
	}
	if(!isset($categories[trim($category)]))
	{
		die("<script>document.location.href='index.php';</script>");
	}
	else
	{
		echo $categories[trim($category)];
	}
	$seo_category_title.=$categories[trim($category)];
	
	echo "</h3>";
	
	$website->Title($seo_category_title." ".$website->GetParam("SEO_APPEND_TITLE"));
	$website->MetaDescription($seo_category_title." ".$website->GetParam("SEO_APPEND_DESCRIPTION"));
	$website->MetaKeywords($website->format_keywords($seo_category_title." ".$website->GetParam("SEO_APPEND_KEYWORDS")));
	
	?>
	<hr/>
	<?php	
	
	$i_substr_cat=substr_count($category, ".");
	
	foreach($categories as $key=>$value)
	{
		$i_substr_key=substr_count($key, ".");
		
		if($i_substr_key != ($i_substr_cat+1))
		{
			continue;
		}
		
		if(strpos($key, $category.".", 0) === 0)
		{
		
			if($website->GetParam("SEO_URLS")==1)
			{
				$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_CATEGORY:"category")."-".$website->format_str($value)."-".str_replace(".","-",$key).".html";
			}
			else
			{
				$strLink = "index.php?mod=search&category=".str_replace(".","-",$key).($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
			}
			
			echo "\n<div class=\"col-md-4 no-left-padding margin-bottom-10\">\n";
			
			echo "\n<a class=\"sub-cat-result\" href=\"".$strLink."\" title=\"".trim($value)."\" >".trim($value)."</a>";
		
			if($website->GetParam("SHOW_LISTINGS_NUMBER")==1)
			{
				echo " (".(isset($arr_listings_count[$key])?$arr_listings_count[$key]:"0").")";
			}
		
			echo "</div>";
		}
	}
	?>
	<div class="clear"></div>
	<br/>
	<br/>
	<?php
}///end category


$iNResults = mysql_num_rows($SearchTable);
	
if($iNResults == 0)
{
	echo "<br/><i class=\"sub-text\">".$M_NO_SITES_IN_CAT."</i><br/><br/><br/><br/><br/>";
}
else
{
		
	$iTotResults = 0;

	if(!isset($_REQUEST["num"]))
	{
		$num = 0;
	}
	else
	{
		$website->ms_i($_REQUEST["num"]);
		$num = $_REQUEST["num"] - 1;
	}
		

	$i_listings_counter = 0;


	while($listing = mysql_fetch_array($SearchTable))
	{
		
		if($iTotResults>=$num*$RESULTS_PER_PAGE&&$iTotResults<($num+1)*$RESULTS_PER_PAGE)
		{
				
			$i_listings_counter++;
						
			$adFeatured = false;
			
			if($listing["featured"]==1)
			{
				$adFeatured = true;
			}
			
			if($website->GetParam("SEO_URLS")==1)
			{
				$strLink = ($MULTI_LANGUAGE_SITE?$M_SEO_AD:"ad")."-".$website->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
			}
			else
			{
				$strLink = "index.php?mod=details&id=".$listing["id"].($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
			}
			?>
			
			<a class="result-link" href="<?php echo $strLink;?>">
		<div class="<?php if($adFeatured) echo "featured_";?>listing">
	
			<?php
			if(trim($listing["images"])!="")
			{
			?>
			<div class="col-sm-3 image-wrapper">
				<div class="result-image">
					<?php
					echo $website->show_pic($listing["images"],"small");
					?>
				</div>
			</div>
			<?php
			}
			?>
		  
			<div class="col-sm-9">
				
				<span class="<?php if($adFeatured) echo "featured_";?>listing_title"><?php echo stripslashes(strip_tags($listing["title"]));?></span>
				
				<?php
				if(trim($listing["featured"]) == "1")
				{
				?>
					<hr class="featured-hr"/>
				<?php
				}
				else
				{
				?>
					<div class="smooth-separator"></div>
				<?php
				}
				?>
				
				<?php
				if(trim($listing["fields"]) != "")
				{
				?>
					<div class="listing_description">
					<?php 
					$listing_fields = unserialize($listing["fields"]);
			
					if(is_array($listing_fields))
					{
						foreach($listing_fields as $key=>$value)
						{
						?>
							<span class="img-right-margin"><?php echo $key;?>: <?php echo $value;?></span>
						<?php
						}
					}
					
					?>
					</div>
				<?php
				}
				?>
				
				
				<?php
				if(trim($listing["description"]) != "")
				{
				?>
					<div class="listing_description">
					<?php echo $website->text_words(stripslashes(strip_tags($listing["description"])),30);?>
					</div>
				<?php
				}
				?>
				<div class="clear"></div>
				<span class="listing_posted_date"><?php echo $M_POSTED_ON;?>: <?php echo date($website->GetParam("DATE_FORMAT"),strip_tags($listing["date"]));?></span>
				
			
			</div>
			
			<div class="clear"></div>
			
			
		</div>
		</a>
		<div class="clear"></div>
		<hr/>	
		<?php
			
		}
		$iTotResults++;
	}

	$strSearchString = "";
				
	foreach($_GET as $key=>$value) 
	{ 
		if($key != "num"&&$key!="i_start")
		{
			$strSearchString .= $key."=".$value."&";
		}
	}
	if(isset($_POST["mod"]))
	$strSearchString .= "mod=".$_POST["mod"]."&";
	
	if(isset($_POST["search_by"]))
	$strSearchString .= "search_by=".$_POST["search_by"]."&";


	if(ceil($iTotResults/$RESULTS_PER_PAGE) > 1)
	{

		echo "<br/>";

		
		$inCounter = 0;
		
		if(($num+1) != 1)
		{
			echo "&nbsp; <a class=\"pagination-link\" href=\"index.php?".$strSearchString."num=1\"><<</a> ";
			
			echo "&nbsp; <a class=\"pagination-link\" href=\"index.php?".$strSearchString."num=".($num)."\"><</a> &nbsp;";
		}
		
		$iStartNumber = ($num+1);
		
		if($iStartNumber > (ceil($iTotResults/$RESULTS_PER_PAGE) - 4))
		{
			$iStartNumber = (ceil($iTotResults/$RESULTS_PER_PAGE) - 4);
		}
		
		if($iStartNumber>3&&($num+1)<(ceil($iTotResults/$RESULTS_PER_PAGE) - 2))
		{
			$iStartNumber=$iStartNumber-2;
		}
		
		if($iStartNumber < 1)
		{
			$iStartNumber = 1;
		}
		
		for($i= $iStartNumber ;$i<=ceil($iTotResults/$RESULTS_PER_PAGE);$i++)
		{
			if($inCounter>=5)
			{
				break;
			}
			
			if($i == ($num+1))
			{
				echo "<b>".$i."</b> ";
			}
			else
			{
				echo "<a class=\"pagination-link\" href=\"index.php?".$strSearchString."num=".$i."\">".$i."</a> ";
			}
							
			
			$inCounter++;
		}
		
		if(($num+1)<ceil($iTotResults/$RESULTS_PER_PAGE))
		{
			echo "&nbsp; <a class=\"pagination-link\" href=\"".($website->GetParam("SEO_URLS")==1?"http://".$DOMAIN_NAME."/":"")."index.php?".$strSearchString."num=".($num+2)."\">></a> ";
			
			echo "&nbsp; <a class=\"pagination-link\" href=\"".($website->GetParam("SEO_URLS")==1?"http://".$DOMAIN_NAME."/":"")."index.php?".$strSearchString."num=".(ceil($iTotResults/$RESULTS_PER_PAGE))."\">>></a> ";
		}
		
		
	}
}
?>