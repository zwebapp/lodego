<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<div class="deal-day">
<?php
if($this->params[80]!=0)
{
	$listing = $this->db->DataArray("listings","id=".($this->params[80]));

	if(isset($listing["id"]))
	{
		if($this->GetParam("SEO_URLS")==1)
		{
			$strLink = ($MULTI_LANGUAGE_SITE?$M_SEO_AD:"ad")."-".$this->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
		}
		else
		{
			$strLink = "index.php?mod=details&id=".$listing["id"];
		}
		
		echo $M_DEAL_DAY." > <a class=\"underline-link\" href=\"".$strLink."\">".strip_tags(stripslashes($listing["title"]))." ...</a>";
	}
}			
?>
</div>