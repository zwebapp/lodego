<?php
if(!defined('IN_SCRIPT')) die("");
?>
<br/>
<?php

$arr_listings_count = array();

if($this->GetParam("SHOW_LISTINGS_NUMBER")==1)
{

	$count_listings = $this->db->Query
	("
		SELECT count(id) c, 
		link_category 
		FROM ".$DBprefix."listings
		WHERE
		".($this->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$this->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
		status=1
		GROUP BY link_category
	");

	while($count_listing = mysql_fetch_array($count_listings))
	{
		$strCat = explode(".",$count_listing["link_category"],2);
		
		if(!isset($arr_listings_count[$strCat[0]]))
		{
			$arr_listings_count[$strCat[0]]=0;
		}
		$arr_listings_count[$strCat[0]]  += $count_listing["c"];
	}

}



$NUMBER_OF_CATEGORIES_PER_ROW = $this->GetParam("NUMBER_OF_CATEGORIES_PER_ROW");

if(file_exists('categories/categories_'.strtolower($this->lang).'.php'))
{
	$categories_content = file_get_contents('categories/categories_'.strtolower($this->lang).'.php');
}
else
{
	$categories_content = file_get_contents('categories/categories_en.php');
}

$arrCategories = explode("\n", trim($categories_content));

$b_first_sub_category = true;
$i_sub_counter=0;
$i_category_counter=0;

foreach($arrCategories as $strCategory)
{
	list($key,$value)=explode(". ",$strCategory);
	
	if($this->GetParam("SEO_URLS")==1)
	{
		$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_CATEGORY:"category")."-".$this->format_str($value)."-".str_replace(".","-",$key).".html";
	}
	else
	{
		$strLink = "index.php?mod=search&category=".str_replace(".","-",$key).($MULTI_LANGUAGE_SITE?"&lang=".$this->lang:"");
	}
		
	if(substr_count($key, '.') == 0)
	{
		if($i_category_counter!=0) echo "\n</div>";
		
		if(($i_category_counter % $NUMBER_OF_CATEGORIES_PER_ROW) == 0)
		{
			echo "\n<div class=\"clear\"></div>";
		}
		
		echo "\n<div class=\"home-category\" style=\"width:".(round(100/$NUMBER_OF_CATEGORIES_PER_ROW,2)-5)."%\">\n";
		
		echo "\n<span class=\"category_link\">";
		echo "<a href=\"".$strLink."\" title=\"".str_replace("&","&amp;",trim($value))."\">".str_replace("&","&amp;",trim($value))."</a>";
		if($this->GetParam("SHOW_LISTINGS_NUMBER")==1)
		{
			echo " <span class=\"category-count\">(".(isset($arr_listings_count[$key])?$arr_listings_count[$key]:"0").")</span>";
		}
		echo "</span>";
	
		$b_first_sub_category = true;
		$i_sub_counter=0;
		$i_category_counter++;
	}
	else
	{
		
		if($i_sub_counter<6)
		{
			if(!$b_first_sub_category) echo ", ";
													
			echo "\n<a href=\"".$strLink."\" title=\"".trim($value)."\" class=\"sub_category_link\">".trim($value)."</a>";
		}
		if($i_sub_counter==6) echo "...";
		$b_first_sub_category = false;
		$i_sub_counter++;
	}
	
}

echo "</div><div class=\"clear\"></div>";
?>