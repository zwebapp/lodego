<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<br/>
<?php
if(!defined('IN_SCRIPT')) die("");
$NUMBER_OF_CATEGORIES_PER_ROW = $website->GetParam("NUMBER_OF_CATEGORIES_PER_ROW");
$b_first_sub_category = true;
$i_sub_counter=0;
$i_category_counter=0;
								
if(!isset($l))
{
	include_once("locations/locations_array.php");
}
asort($l);
foreach($l as $key=>$value)
{
	if(!is_string($value)) continue;
	
	if($website->GetParam("SEO_URLS")==1)
	{
		$strLink = ($MULTI_LANGUAGE_SITE?$M_SEO_LOCATION:"location")."-".$website->format_str($value)."-".str_replace(".","-",$key).".html";
	}
	else
	{
		$strLink = "index.php?mod=search&location=".str_replace(".","-",$key).($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
	}
	
	$b_first_sub_category = true;
	$i_sub_counter=0;
	
	echo "\n<div class=\"home-category\" style=\"width:".(round(100/$NUMBER_OF_CATEGORIES_PER_ROW,2)-5)."%\">\n";
		
	echo "\n<span class=\"category_link\">";
	
	if($website->GetParam("SEO_URLS")==1)
	{
		$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_LOCATION:"location")."-".$website->format_str($value)."-".str_replace(".","-",$key).".html";
	}
	else
	{
		$strLink = "index.php?mod=search&location=".str_replace(".","-",$key).($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
	}
	 
	echo "\n<a href=\"".$strLink."\" class=\"category_link\">".trim($value)."</a>";
	
	echo "</span>\n";
	
	if(isset($l1[$key]))
	{
		foreach($l1[$key] as $sub_key=>$sub_location)
		{
			if(!is_string($sub_location)) continue;
	
			if($website->GetParam("SEO_URLS")==1)
			{
				$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_LOCATION:"location")."-".$website->format_str($sub_location)."-".str_replace(".","-",$key."-".$sub_key).".html";
			}
			else
			{
				$strLink = "index.php?mod=search&location=".str_replace(".","-",$key."-".$sub_key).($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
			}
	
			if($i_sub_counter>=8)
			{
				echo "...";
				break;
			}
			
			if(!$b_first_sub_category) echo ", ";
			
			echo "\n<a href=\"".$strLink."\" title=\"".trim($value)."\" class=\"sub_category_link\">".stripslashes(trim($sub_location))."</a>";
			$b_first_sub_category = false;
			$i_sub_counter++;
		}
	}
	
	echo "</div>\n";
	
	$i_category_counter++;
	
	if(($i_category_counter % $NUMBER_OF_CATEGORIES_PER_ROW) == 0)
	{
		echo "\n<div class=\"clear\"></div>";
	}
		
	
}



?>
<div class="clear"></div>	
<!--
<br/>
<a href="index.php<?php if($MULTI_LANGUAGE_SITE) echo "?lang=".$website->lang;?>"><?php echo $M_CLICK_BROWSE_CATEGORY;?></a>
<br/>
-->
<br/>