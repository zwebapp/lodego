<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
if(!defined('IN_SCRIPT')) die("");
$show_form = true;
$process_error="";
$website->Title($M_MODIFY_AD);
$website->MetaDescription("");
$website->MetaKeywords("");
if(!isset($_REQUEST["ad"]))
{
?>
	<h2><?php echo $M_PLEASE_ENTER_CODE_AD;?></h2>
	
	<form id="main" onsubmit="return gSubmitForm(this)" action="index.php" method="post"  enctype="multipart/form-data">
	<?php
	if(isset($_REQUEST["mod"]))
	{
	?>
	<input type="hidden" name="mod" value="<?php echo $_REQUEST["mod"];?>"/>
	<?php
	}
	else
	{
	?>
	<input type="hidden" name="page" value="<?php echo $_REQUEST["page"];?>"/>
	<?php
	}
	?>
	
	
				
<fieldset>
	<input id="ad" name="ad" style="background:white !important" placeholder="<?php echo $M_CODE;?>" type="text" required/>
	<button type="submit"><?php echo $M_SEND;?></button>
</fieldset>	

<?php	
}
else
{
	$ad = $database->DataArray("listings","code='".$_REQUEST["ad"]."'");
	
	if(!isset($ad["id"])) die("Such ad doesn't exist");



if(isset($_REQUEST["delete"]))
{
	
	$database->SQLDelete("listings","code",array("'".$ad["code"]."'"));
	
	echo "<br/><h2>".$M_AD_DELETED_SUCCESS."</h2>";
	echo "<br/><br/><br/><br/><br/>";
	$show_form = false;
}

if(isset($_POST["ProceedSend"]))
{
	
	if(trim($_POST["title"])=="")
	{
		$process_error=$M_PLEASE_ENTER_TITLE;
	}
	else
	
	if(trim($_POST["link_category"])=="")
	{
		$process_error=$M_PLEASE_SELECT_CATEGORY;
	}
	
	else
	{
		
		$website->ms_i($_POST["package"]);
		
		$selected_package=$database->DataArray("packages","id=".$_REQUEST["package"]);
	
			
		
		
		$database->SQLUpdate
		(
			"listings",
			array
			(
				"package",
			
				"title",
				
				"description",
				"link_category",
				"name",
				"phone",
				"email",
				"date",
				"featured",
				"expires",
				"ip",
				"location",
				"user_type"
			),
			array
			(
				$_POST["package"],
				
				$_POST["title"],
				$_POST["description"],
				$_POST["link_category"],
				$_POST["name"],
				$_POST["phone"],
				$_POST["email"],
				time(),
				$selected_package["featured"],
				time()+$selected_package["days"]*24*3600,
				$_SERVER["REMOTE_ADDR"],
				$_POST["location"],
				$_POST["user_type"]
			),
			"code='".$ad["code"]."'"
		);
		
		$ad = $database->DataArray("listings","code='".$_REQUEST["ad"]."'");
	
	
		if($selected_package["price"]==0)
		{
		?>
			<h4>
			<?php echo $M_MODIFICATIONS_SAVED;?>
			</h4>
		<?php
		}
		else
		{
		?>
			<h4>
			<?php
			echo nl2br(stripslashes($website->GetParam("SUCCESS_MESSAGE_PAID")));
			?>
			<br/><br/>
			<?php echo $M_PLEASE_SELECT_PAYMENT;?>
			<br/><br/>
			<?php
			if(trim($website->GetParam("PAYPAL_ID")) !="")
			{
			?>	
				<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
				<input type="hidden" name="cmd" value="_xclick">
				<input type="hidden" name="business" value="<?php echo $website->GetParam("PAYPAL_ID");?>">
				<input type="hidden" name="currency_code" value="<?php echo $website->GetParam("CURRENCY_CODE");?>">
				<input type="hidden" name="item_name" value="<?php echo $M_PAYMENT_FOR_SUBMISSION;?> <?php echo $_POST["url"];?> on <?php echo $DOMAIN_NAME;?>">
				<input type="hidden" name="item_number" value="<?php echo $i_submit_id;?>">
				<input type="hidden" name="amount" value="<?php echo number_format($selected_package["price"], 2, '.', '');?>">
				<input type="image"  src="images/paypal.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
				</form>
			<?php
			}
			?>
			</h4>
		<?php
		
		}
		
	}
	
	
	

}


if($show_form)
{
?>
<a class="button-link" href="index.php?mod=modify&delete=1&ad=<?php echo $ad["code"];?>"><?php echo $M_DELETE_AD;?></a>
				
<div class="clear"></div>


<h2>
<?php




if($process_error=="")
{
	if(isset($_POST["ProceedSend"]))
	{
	?>
	<?php echo $M_MODIFICATIONS_SAVED;?>
	<?php
	}
	else
	{
	?>
	<?php echo $M_MODIFY_AD;?>
	<?php
	}
}
else
{
	echo $process_error;
}
?>
</h2>

<br/>

<form id="main" action="index.php" method="post"  enctype="multipart/form-data">
<input type="hidden" name="ad" value="<?php echo $ad["code"];?>"/>
<?php
if(isset($_REQUEST["mod"]))
{
?>
<input type="hidden" name="mod" value="<?php echo $_REQUEST["mod"];?>"/>
<?php
}
else
{
?>
<input type="hidden" name="page" value="<?php echo $_REQUEST["page"];?>"/>
<?php
}
?>
<input type="hidden" name="ProceedSend" value="1"/>

	
<fieldset>
		<legend><?php echo $M_WEBSITE_DETAILS;?></legend>
		<ol>
			<li>
				<label for="category"><?php echo $M_CATEGORY;?>(*)</label>
				
				<select name="link_category" id="link_category" required>
				<option value=""><?php echo $M_PLEASE_SELECT;?></option>
				
				<?php
				if(file_exists('categories/categories_'.strtolower($website->lang).'.php'))
				{
					$categories_content = file_get_contents('categories/categories_'.strtolower($website->lang).'.php');
				}
				else
				{
					$categories_content = file_get_contents('categories/categories_en.php');
				}

				$arrCategories = explode("\n", trim($categories_content));

				$categories=array();
				
				foreach($arrCategories as $str_category)
				{
					list($key,$value)=explode(". ",$str_category);
					$categories[trim($key)]=trim($value);
					
					$i_level = substr_count($key, ".");
					
					echo "<option ".($ad["link_category"]==trim($key)?"selected":"")." value=\"".trim($key)."\">".str_repeat("|&nbsp;&nbsp;&nbsp;&nbsp;", $i_level)."|__".trim($value)."</option>";
				}
				
				
				?>
				
				
				</select>
			</li>
			<li>
				<label for="title"><?php echo $M_TITLE;?>(*)</label>
				<input id="title" name="title"  <?php echo "value=\"".stripslashes($ad["title"])."\"";?> type="text" placeholder="<?php echo $M_WEBSITE_TITLE;?>" required autofocus/>
			</li>
			
			<li>
				<label for="description"><?php echo $M_DESCRIPTION;?>(*)
				<br>
				
				</label>
				<textarea id="description" name="description" rows="10"> <?php echo stripslashes($ad["description"]);?></textarea>
				<br>
				<span class="sub-text" style="position:relative;left:120px"><?php echo $M_CHARS_MAX;?></span>
			</li>
			
			<li>
				<label for="user_type"><?php echo $M_LOCATION;?></label>
				
				
				<select name="location" id="location">
					<option value=""><?php echo $M_PLEASE_SELECT;?></option>
				
				<?php
				$locations_content = file_get_contents('locations/locations.php');
				

				$arrLocations = explode("\n", trim($locations_content));
		
				foreach($arrLocations as $str_location)
				{
					$loc_items = explode(". ",$str_location);
					
					if(sizeof($loc_items)==2)
					{
						list($key,$value)=$loc_items;
						
						$i_level = substr_count($key, ".");
						
						echo "<option ".($ad["location"]==trim($key)?"selected":"")." value=\"".trim($key)."\">".str_repeat("|&nbsp;&nbsp;&nbsp;&nbsp;", $i_level)."|__".trim($value)."</option>";
					}
				}
				
				
				?>
				
				
				</select>
			</li>
		</ol>
	</fieldset>
	<!--
	<fieldset>
		<legend>Images</legend>
		<ol>
			<li>
				
				
				<label for="images">Please select</label>
				
				
				<input type="file" name="images[]" id="images"  multiple="multiple"/>
				<br/>
				<span class="sub-text">You can select up to 10 images.</span>
				
			</li>
			
			
		</ol>
	</fieldset>
	-->
	<fieldset>
		<legend><?php echo $M_YOUR_DETAILS;?></legend>
		<ol>
			<li>
				<label for="user_type"><?php echo $M_I_AM;?></label>
				
				
				<select name="user_type" id="user_type">
					<option <?php if($ad["user_type"]=="1") echo "selected";?> value="1"><?php echo $M_PRIVATE_PERSON;?></option>
					<option <?php if($ad["user_type"]=="2") echo "selected";?> value="2"><?php echo $M_COMPANY_PRO;?></option>
				</select>
			</li>
			<li>
				<label for="name"><?php echo $M_YOUR_NAME;?>(*)</label>
				<input id="name" <?php  echo "value=\"".$ad["name"]."\"";?> name="name" type="text" required/>
			</li>
			<li>
				<label for="email"><?php echo $M_YOUR_EMAIL;?>(*)</label>
				<input id="email" <?php  echo "value=\"".$ad["email"]."\"";?> name="email" type="email" required/>
				<br/>
				<span class="sub-text"><?php echo $M_EMAIL_PUBLISHED;?></span>
			</li>
			
			<li>
				<label for="phone"><?php echo $M_YOUR_PHONE;?></label>
				<input id="phone" <?php  echo "value=\"".$ad["phone"]."\"";?> name="phone" placeholder="" type="text"/>
			</li>
		
		</ol>
	</fieldset>
	
	
	<fieldset>
		<legend><?php echo $M_CHOOSE_PACKAGE;?></legend>
		<ol>
		<?php
		$listing_packages = $database->DataTable("packages","WHERE active=1 ORDER BY price,id");
		$b_first_package = true;
		while($listing_package = mysql_fetch_array($listing_packages))
		{
			
		?>
			<li style="line-height:1.0em">
				<br/>
				<input name="package" value="<?php echo $listing_package["id"];?>" <?php if(isset($_REQUEST["package"])&&$_REQUEST["package"]==$listing_package["id"]) echo "checked";?> <?php if($b_first_package) echo "checked";?> class="form-radio" type="radio"/>
				<b><?php echo stripslashes($listing_package["name"]);?></b>
				,&nbsp;   
				<?php echo $M_PRICE;?>: 
				<b>
				<?php 
				if($listing_package["price"]==0)
				{
					echo "FREE!";
				}
				else
				{
					echo $website->GetParam("WEBSITE_CURRENCY").number_format($listing_package["price"],2,'.',',');
				}
				?>
				</b>
			<br/><br/>
			<span class="sub-text">
			<?php echo stripslashes($listing_package["description"]);?>
			</span>
			<br/><br/>
			</li>
		<?php
			$b_first_package = false;
		}
		?>
			
			
		</ol>
	</fieldset>

	
	(*) <?php echo $M_REQUIRED_FIELDS;?>
	<fieldset>
		<button type="submit"><?php echo $M_SAVE;?></button>
	</fieldset>
</form>
<?php
}
?>
<!--[if !IE]>
<script>
document.getElementById('title').setCustomValidity('<?php echo $M_PLEASE_ENTER_TITLE;?>');
document.getElementById('url').setCustomValidity('<?php echo $M_PLEASE_ENTER_URL;?>');
document.getElementById('category').setCustomValidity('<?php echo $M_PLEASE_SELECT_CATEGORY;?>');
document.getElementById('name').setCustomValidity('<?php echo $M_PLEASE_ENTER_NAME;?>');
document.getElementById('email').setCustomValidity('<?php echo $M_PLEASE_ENTER_EMAIL;?>');
document.getElementById('code').setCustomValidity('<?php echo $M_PLEASE_ENTER_CODE;?>');
</script>
<![endif]-->

<?php
}
?>