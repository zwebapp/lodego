<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
if(!defined('IN_SCRIPT')) die("");
?>
<h3><?php echo $M_TOP_RATED;?></h3>
<hr/>

<?php
$RESULTS_PER_PAGE = $website->GetParam("RESULTS_PER_PAGE");

$SearchTable = $database->Query
("
	SELECT * FROM ".$DBprefix."listings
	WHERE 
	".$DBprefix."listings.status = 1
	AND rating>0
	ORDER BY 
	".$DBprefix."listings.rating DESC
	LIMIT 0,".$RESULTS_PER_PAGE."
");



$iNResults = mysql_num_rows($SearchTable);
	
if($iNResults == 0)
{
	echo "<br/><i class=\"sub-text\">".$M_NO_RESULTS_FOUND."</i>";
}
else
{

	while($listing = mysql_fetch_array($SearchTable))
	{
				
		$adFeatured = false;
		
		if($listing["featured"]==1)
		{
			$adFeatured = true;
		}
		
		
		if($website->GetParam("SEO_URLS")==1)
		{
			$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_AD:"listing")."-".$website->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
		}
		else
		{
			$strLink = "index.php?mod=details&id=".$listing["id"].($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
		}
		?>
		<a class="result-link"  href="<?php echo $strLink;?>">
		<div class="<?php if($adFeatured) echo "featured_";?>listing">
	
			<?php
			if(trim($listing["images"])!="")
			{
			?>
			<div class="col-sm-3 image-wrapper">
				<div class="result-image">
					<?php
					echo $website->show_pic($listing["images"],"small");
					?>
				</div>
			</div>
			<?php
			}
			?>
		  
			<div class="col-sm-9">
				
				<span class="lfloat <?php if($adFeatured) echo "featured_";?>listing_title"><?php echo stripslashes(strip_tags($listing["title"]));?></span>
				<?php
				echo $website->show_stars($listing["rating"],"result-stars");
				?>
				<div class="clear"></div>
				
				<?php
				if(trim($listing["address"]) != "")
				{
				?>
					<span class="sub-text">
						<?php echo $listing["address"];?>
					</span>
					
				<?php
				}
				if(trim($listing["featured"]) == "1")
				{
				?>
					<hr class="featured-hr"/>
				<?php
				}
				else
				{
				?>
					<div class="smooth-separator"></div>
				<?php
				}
				if(trim($listing["description"]) != "")
				{
				?>
					<div class="listing_description">
					<?php echo $website->text_words(stripslashes(strip_tags($listing["description"])),35);?>
					</div>
				<?php
				}
				?>
				
				<div class="clear"></div>
				<span class="listing_posted_date">
					<?php echo $M_RATING;?>: 
					<?php echo $listing["rating"];?>
					<?php echo $M_OF_5;?>
				</span>
			</div>
			</a>
			<div class="clear"></div>
			
		</div>
		
		<hr class="no-top-margin"/>	
		
		<?php
	
	}

}
?>