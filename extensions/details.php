<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<br/>
<?php
if(!defined('IN_SCRIPT')) die("");
$id=$_REQUEST["id"];

$website->ms_i($id);

$listing = $database->DataArray("listings","id=".$id);
$process_error="";

if(isset($_POST["SendOfferFriend"]))
{
	if
	(
		$website->GetParam("USE_CAPTCHA_IMAGES")==1
		&& ( (strtoupper($_POST['code2']) != $_SESSION['code2'])|| trim($_POST['code2']) == "" )
	)
	{
		$process_error=$M_WRONG_CODE;
	}
	else
	{
		if($_POST["sender_name"]!=""&&$_POST["email_address"]!="")
		{
			$SYSTEM_EMAIL_FROM=$website->GetParam("SYSTEM_EMAIL_FROM");
			$SYSTEM_EMAIL_ADDRESS=$website->GetParam("SYSTEM_EMAIL_ADDRESS");
		
			$headers  = "From: \"".$SYSTEM_EMAIL_FROM."\"<".$SYSTEM_EMAIL_ADDRESS.">\n";
						
			mail
			(
			
				strip_tags($_POST["email_address"]),
				$M_LISTING_BY." ".strip_tags(stripslashes($_POST["sender_name"])),
				strip_tags(stripslashes($_POST["sender_name"]))."\n\n".$strLink."\n\n".strip_tags(stripslashes($_POST["friend_description"])), 
				$headers
			);
			
			
			echo "<h3>".$M_EMAIL_SENT_SUCCESS.": ".strip_tags($_POST["email_address"])."</h3><br/>";
		}
	}

}
else
if(isset($_POST["ContactAdvertiser"]))
{
	if
	(
		$website->GetParam("USE_CAPTCHA_IMAGES")==1
		&& ( (strtoupper($_POST['code']) != $_SESSION['code'])|| trim($_POST['code']) == "" )
	)
	{
		$process_error=$M_WRONG_CODE;
	}
	else
	{
		if($_POST["name"]!=""&&$_POST["description"]!=""&&$_POST["email"]!="")
		{
			$headers  = "From: \"".stripslashes($_POST["name"])."\"<".$_POST["email"].">\n";
					
			$email_subject = $M_QUESTION_FOR_YOUR_AD." '".stripslashes($listing["title"])."'";
			
			$email_text = $M_SENT_BY.": ".stripslashes($_POST["name"]).
			", ".$M_EMAIL.": ".$_POST["email"];
			if($_POST["name"]!="")
			{
				$email_text .= ", ".$M_PHONE.": ".$_POST["phone"];
			}
			
			$email_text .= "\n\n".stripslashes($_POST["description"]);

			mail
			(
				$listing["email"],
				$email_subject,
				$email_text, 
				$headers
			);
		}
	}

}
else
{
	$database->Query("UPDATE ".$DBprefix."listings SET visits=visits+1 WHERE id=".$id);
}
	
$website->Title(stripslashes($listing["title"])." - ".$website->show_category($listing["link_category"])." ".$M_CLASSIFIED_AD);
$website->MetaDescription($website->text_words(stripslashes(strip_tags($listing["description"])),30));
$website->MetaKeywords($website->format_keywords($website->text_words(stripslashes(strip_tags($listing["description"])),20)));
?>
<script>
var contact_advertiser = false;
function ShowContactForm()
{
	if(!contact_advertiser)
	{
		document.getElementById("contact-advertiser").style.display="block";
		contact_advertiser = true;
	}
	else
	{
		document.getElementById("contact-advertiser").style.display="none";
		contact_advertiser = false;
	}
}
</script>


<?php

if(isset($_POST["ContactAdvertiser"])&&$process_error=="")
{
?>
	<h3><?php echo $M_YOUR_MESSAGE_SENT;?></h3><br/>
<?php
}
else
{
?>
<button type="button"  data-toggle="collapse" data-target=".adv-collapse" class="rfloat btn btn-xs btn-default btn-gradient"><?php echo $M_CONTACT_ADVERTISER;?></button>

<?php
}
?>	
		
<div class="clear"></div>


<div id="contact-advertiser"  class="text-left collapse adv-collapse">
	<br/>
	
	<h3>
	<?php 
	if($process_error=="")
	{
	
		echo $M_CONTACT." ".strip_tags(stripslashes($listing["business_name"]));
	
	}
	else
	{
		echo $process_error;
	}
	?>
	</h3>
	
	<form id="main" action="index.php" method="post"  enctype="multipart/form-data">
	<?php
	if(isset($_REQUEST["mod"]))
	{
	?>
	<input type="hidden" name="mod" value="<?php echo $_REQUEST["mod"];?>"/>
	<?php
	}
	else
	{
	?>
	<input type="hidden" name="page" value="<?php echo $_REQUEST["page"];?>"/>
	<?php
	}
	?>
	<input type="hidden" name="id" value="<?php echo $id;?>"/>
	
	<input type="hidden" name="ContactAdvertiser" value="1"/>
	<fieldset>
		<legend><?php echo $M_ENTER_MESSAGE_OR_QUESTIONS;?></legend>
		<ol>
			<li>
				<label for="description"><?php echo $M_MESSAGE_TEXT;?>(*)
				<br>
				
				</label>
				<textarea id="description" name="description" rows="8"> <?php if(isset($_REQUEST["description"])) echo stripslashes($_REQUEST["description"]);?></textarea>
			</li>
	</ol>
	</fieldset>
	<fieldset>
		<legend><?php echo $M_YOUR_DETAILS;?></legend>
		<ol>
			
			<li>
				<label for="name"><?php echo $M_YOUR_NAME;?>(*)</label>
				<input id="name" <?php if(isset($_REQUEST["name"])) echo "value=\"".$_REQUEST["name"]."\"";?> name="name" placeholder="<?php echo $M_FIRST_AND_LAST;?>" type="text" required/>
			</li>
			<li>
				<label for="email"><?php echo $M_YOUR_EMAIL;?>(*)</label>
				<input id="email" <?php if(isset($_REQUEST["email"])) echo "value=\"".$_REQUEST["email"]."\"";?> name="email" placeholder="example@domain.com" type="email" required/>
				
			</li>
			<li>
				<label for="phone"><?php echo $M_YOUR_PHONE;?></label>
				<input id="phone" <?php if(isset($_REQUEST["phone"])) echo "value=\"".$_REQUEST["phone"]."\"";?> name="phone" placeholder="" type="text"/>
			</li>
			<?php
			if($website->GetParam("USE_CAPTCHA_IMAGES")==1)
			{
			?>
			<li>
				<label for="code">
				<img src="include/sec_image.php" width="100" height="30"/>
				</label>
				<input id="code" name="code" placeholder="<?php echo $M_PLEASE_ENTER_CODE;?>" type="text" required/>
			</li>
			<?php
			}
			?>
		</ol>
	</fieldset>
	<fieldset>
		<button type="submit"><?php echo $M_SEND;?></button>
	</fieldset>
</form>

<br/><br/>
</div>



<h2 class="lfloat"><?php echo stripslashes(strip_tags($listing["title"]));?></h2>
<div class="clear"></div>
<?php



$reviews = $database->DataArray_Query("SELECT count(id) as number,avg(vote) as vote FROM ".$DBprefix."comments WHERE listing_id=".$id);
echo $website->show_stars($reviews["vote"]);
if($website->GetParam("SEO_URLS")==1)
{
	$review_link = ($MULTI_LANGUAGE_SITE?$M_SEO_AD:"reviews")."-".$website->format_str($listing["title"])."-".$id.".html";
	$write_review_link = ($MULTI_LANGUAGE_SITE?$M_SEO_WRITE_REVIEW:"write-review")."-".$website->format_str($listing["title"])."-".$id.".html";
}
else
{
	$review_link = "index.php?mod=reviews&id=".$id.($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
	$write_review_link = "index.php?write=1&mod=reviews&id=".$id.($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
}
echo " <span style=\"position:relative;top:-5px;left:10px\">(<a href=\"".$review_link."\">".$reviews["number"]. " ".$M_REVIEWS."</a>)</span>";
?>

<div class="clear"></div>
<br/>

<?php
if($listing["offer_type"]==1&&$listing["coupon"]!="")
{
?>
<h4 class="lfloat padding-3"><?php echo $M_COUPON_CODE;?>:</h4>
<?php
}
?>	
<?php
if(($listing["offer_type"]==1&&$listing["coupon"]!="")||$listing["offer_type"]==2)
{
?>
<div class="lfloat coupon-wrap rounded-borders">
	<div class="coupon-show rounded-borders">
		
		<h4>
		<?php
		if($listing["offer_type"]==1)
		{
		?>
			<?php echo strip_tags(stripslashes($listing["coupon"]));?>
		<?php
		}
		else
		if($listing["offer_type"]==2)
		{
		?>
			<a class="white-link" target="_blank" href="http://<?php echo strip_tags(stripslashes($listing["offer_link"]));?>"><?php echo $M_PRINT_COUPON;?></a>
		<?php
		}
		?>
		
		</h4>
	</div>
</div>
<?php
}
?>


<div class="clear"></div>
<br/>
	
	<?php
	$listing["images"]=str_replace(",,",",",$listing["images"]);
	$images=explode(",",$listing["images"]);
	?>	
		
	<div class="tabbable">
          <ul class="nav nav-tabs">
            <li class="active">
				<a href="#tab-details" data-toggle="tab"><?php echo $M_DETAILS;?></a>
			</li>
			<?php
			if($listing["latitude"]!=""&&$listing["longitude"]!="")
			{
			?>
            <li><a href="#tab-contact" onclick="javascript:show_map()" data-toggle="tab"><?php echo $M_MAP_LOCATION;?></a></li>
			<?php
			}
			?>
			<li><a href="#tab-share" data-toggle="tab"><?php echo $M_SHARE;?></a></li>
			<li><a href="<?php echo $write_review_link;?>"><?php echo $M_WRITE_A_REVIEW;?></a></li>
		
          
          </ul>
         <div class="tab-content">
		  <br/>
            <div class="tab-pane active" id="tab-details">
			
				<div class="row">
				
					<div class="col-md-<?php if(trim($listing["images"])!="") echo "8";else echo "12";?>">
					
					<?php
					if(trim($listing["fields"])!="")
					{
						$listing_fields = unserialize($listing["fields"]);
						
						if(is_array($listing_fields))
						{
							foreach($listing_fields as $key=>$value)
							{
							?>
							
							<?php echo $key;?>: <b><?php echo $value;?></b>
							<br/><br/>
							<?php
							}
						}
					}
					?>
		
					<?php echo nl2br(stripslashes(strip_tags($listing["description"])));?>
				
				
					
		
					<br/><br/>
					
					
					<?php
					if(trim($listing["address"])!="")
					{
					?>
						<?php echo $M_ADDRESS;?>:
						<br/>
						<b>
						<?php
						echo strip_tags(stripslashes($listing["address"]));
						?>
						</b>
					<?php
					}
					?>

				
		<br/>
		
		<?php
		if(trim($listing["offer_expires"]) != "")
		{
		?>
			<?php echo $M_OFFER_EXPIRES;?>: 
			<b><?php echo stripslashes(strip_tags($listing["offer_expires"]));?></b>
			
		<?php
		}
		?>
		<br/>
		
		<?php
		if(trim($listing["location"])!="")
		{
		?>
		<br/>
		<?php echo $M_LOCATION;?>: <b><?php echo $website->show_full_location($listing["location"]);?></b>
		<?php
		}
		?>
		<br/>
		<?php
		if(trim($listing["url"]) != "")
		{
		?>
			<br/><br/>
			<a class="btn btn-sm btn-default btn-gradient" target="_blank" rel="nofollow" href="http://<?php echo strip_tags(stripslashes($listing["url"]));?>"><?php echo $M_OPEN." ".strip_tags(stripslashes($listing["business_name"]))." ".$M_SITE;?></a>
			<br/>
		<?php
		}
		?>
		</div>
			<?php
				if(trim($listing["images"])!="")
				{
				?>
				
					<div class="col-md-4">
						<div class="final-result-image">
						<?php
							
						echo "<a href=\"uploaded_images/".$images[0].".jpg\" rel=\"prettyPhoto[ad_gal]\">";
						echo $website->show_pic($listing["images"],"big",stripslashes(strip_tags($listing["title"])));
						echo "</a>";
						?>
						</div>
					</div>
				<?php
				}
				?>					
				</div>	
				
				<div class="clear"></div>
				<br/><br/>
		</div>
		
		<div class="tab-pane" id="tab-contact">
			
				<?php
				if(trim($listing["location"])!="")
				{
				?>
				<br/><br/>
				<?php echo $M_LOCATION;?>: <b><?php echo $website->show_full_location($listing["location"]);?></b>
				<?php
				}
				?>
				
				<br/>
				
				<!--Google Maps-->
				  <?php
				if($listing["latitude"]!=""&&$listing["latitude"]!="0"&&$listing["latitude"]!="0.00"&&$listing["longitude"]!=""&&$listing["longitude"]!="0"&&$listing["longitude"]!="0.00")
				{
				?>
								
				<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false">
				</script>
				<script type="text/javascript">
				
				function show_map()
				{

					var Latlng = new google.maps.LatLng(<?php echo $listing["latitude"];?>,<?php echo $listing["longitude"];?>);

					var Options = {
						zoom: 15,
						center: Latlng,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					};

					var Map = new google.maps.Map(document.getElementById("map"), Options);

					var Marker = new google.maps.Marker({
						position: Latlng,
						map:Map,
						title:"The Map"
					});

					Marker.setMap(Map);
					
					Map.setCenter(new google.maps.LatLng(<?php echo $listing["latitude"];?>,<?php echo $listing["longitude"];?>));
					
					
				  }

				</script>
				
				<br/>
				<div id="map" style="width: 500px; height: 300px"></div>
				<br/>
				<?php
				}
				?>
				
				<!--end Google Maps-->
				<?php
				if($listing["address"]!="")
				{
					echo $listing["address"];
				}
				?>
				<br/><br/>
				
							
			</div><!--end tab-contact-->
			
			<div class="tab-pane" id="tab-share">
				
				
				<?php
				$str_current_url=$website->current_url();
				?>
				<img src="images/googleplus.gif" alt="Share on Google+"/>
				<a class="underline-link" rel="nofollow" href="http://plus.google.com/share?url=<?php echo $str_current_url;?>" target="_blank">
				<?php echo $M_SHARE_AD;?> Google+
				</a>
				<br/><br/>
				<img src="images/facebook.gif" alt="Share on Facebook"/>
				<a class="underline-link" rel="nofollow" href="http://www.facebook.com/sharer.php?u=<?php echo $str_current_url;?>" target="_blank">
				<?php echo $M_SHARE_AD;?> Facebook
				</a>
				<br/><br/>
				<img src="images/twitter.gif" alt="Share on Twitter"/>
				<a class="underline-link" rel="nofollow" href="http://www.twitter.com/intent/tweet?text=<?php echo stripslashes(strip_tags($listing["title"]));?>&url=<?php echo $str_current_url;?>" target="_blank">
				<?php echo $M_SHARE_AD;?> Twitter
				</a>
				<br/><br/>
				<br/>
				
				<!--recommend to a friend-->
				<form id="main" action="index.php" method="post"  enctype="multipart/form-data">
				
				<input type="hidden" name="mod" value="details"/>
				
				<input type="hidden" name="id" value="<?php echo $id;?>"/>
				
				<input type="hidden" name="SendOfferFriend" value="1"/>
				<fieldset>
					<legend><?php echo $M_RECOMMEND_LISTING;?></legend>
					<ol>
						
						<li>
							<label for="sender_name"><?php echo $M_YOUR_NAME;?>(*)</label>
							<input id="sender_name" <?php if(isset($_REQUEST["sender_name"])) echo "value=\"".$_REQUEST["sender_name"]."\"";?> name="sender_name" placeholder="<?php echo $M_FIRST_AND_LAST;?>" type="text" required/>
						</li>
						<li>
							<label for="email_address"><?php echo $EMAIL_SEND_FRIEND;?>(*)</label>
							<input id="email_address" <?php if(isset($_REQUEST["email_address"])) echo "value=\"".$_REQUEST["email_address"]."\"";?> name="email_address" placeholder="your_friend@email.com" type="email" required/>
							
						</li>
						<li>
							<label for="friend_description"><?php echo $M_MESSAGE_TEXT;?>(*)
							<br>
					
							</label>
							<textarea id="friend_description" name="friend_description" rows="8"> <?php if(isset($_REQUEST["friend_description"])) echo stripslashes($_REQUEST["friend_description"]);?></textarea>
						</li>
						<?php
						if($website->GetParam("USE_CAPTCHA_IMAGES")==1)
						{
						?>
						<li>
							<label for="code">
							<img src="include/sec_image2.php" width="100" height="30"/>
							</label>
							<input id="code2" name="code2" placeholder="<?php echo $M_PLEASE_ENTER_CODE;?>" type="text" required/>
						</li>
						<?php
						}
						?>
					</ol>
				</fieldset>
				<fieldset>
				<button type="submit"><?php echo $M_SUBMIT;?></button>
				</fieldset>
				</form>
				<!--end recommend to a friend-->
			
			</div><!--end tab share-->
          
		</div>
	</div>
	
		<!--end Google Maps-->
		
		
		
		<div class="clear"></div>
		
		<?php
		$str_category = $website->show_category($listing["link_category"]);
	
		if($website->GetParam("SEO_URLS")==1)
		{
			$strCatLink = "http://".$DOMAIN_NAME."/category-".$website->format_str($str_category)."-".str_replace(".","-",$listing["link_category"]).".html";
		}
		else
		{
			$strCatLink = "index.php?mod=search&category=".str_replace(".","-",$listing["link_category"]);
		}
		?>
		<hr/>
		<?php echo $M_LISTED_IN;?> <a href="<?php echo $strCatLink;?>" class="underline-link"><b><?php echo $str_category;?></b></a> category

		<br/><br/>
		<div class="well">
		
		<div class="col-md-6">
		<?php
		if(trim($listing["business_name"])!="")
		{
		?>
			<b><?php echo strip_tags(stripslashes($listing["business_name"]));?></b>
		<?php
		}
		else
		{
		?>
			<b><?php echo strip_tags(stripslashes($listing["name"]));?></b>
		<?php
		}
		?>
		
		<?php
		if($listing["url"]!="")
		{
		?>
		<br/><a rel="nofollow" href="http://<?php echo strip_tags($listing["url"]);?>"><?php echo strip_tags($listing["url"]);?></a>
		<?php
		}
		?>
		
		<?php
		if($listing["phone"]!="")
		{
		?>
		<br/><img src="images/phone_icon.png" alt="phone icon" /> <?php echo strip_tags($listing["phone"]);?>
		<?php
		}
		?>
		
		</div>
	
		<div class="col-md-3 text-center">
			<b><?php echo $M_POSTED_ON;?></b>
			<br/>
			
			<?php echo date($website->GetParam("DATE_FORMAT"),strip_tags($listing["date"]));?>
			
		</div>
		<div class="col-md-3 text-center">
			<b><?php echo $M_TOTAL_VISITS;?></b>
			<br/>
			<?php echo $listing["visits"];?>
			
		</div>
		
		<div class="clear"></div>
		
		</div>

	


<div class="clear"></div>
<br/><br/>
