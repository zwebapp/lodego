<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
if (!defined('IN_SCRIPT')) die("");
if(isset($_REQUEST["id"]))
{
	$news_id = $_REQUEST["id"];
	$website->ms_i($news_id);
	$arrNews = $database->DataArray("news","id=".$news_id);
	$website->Title(strip_tags(stripslashes($arrNews["title"])));
	$website->MetaDescription($website->text_words(strip_tags(stripslashes($arrNews["html"])),25));
	$website->MetaKeywords($website->format_keywords($website->text_words(strip_tags(stripslashes($arrNews["html"])),35)));
	
	echo "<div class=\"news-date\">".date($website->GetParam("DATE_FORMAT"),$arrNews["date"])."</div><br>";
	
	echo '
	<h2 class="news-title">
		'.stripslashes($arrNews["title"]).'
	</h2>
	<div class="news-content">
		'.stripslashes($arrNews["html"]).'
	</div>';
}
else
{
	$tableNews=$database->DataTable("news","WHERE active='YES' ORDER BY id DESC");

	while($arrNews = mysql_fetch_array($tableNews))
	{
			
		echo "
		<span class=\"rfloat listing_posted_date\">".$M_POSTED_ON.": ".date($website->GetParam("DATE_FORMAT"),strip_tags($arrNews["date"]))."</span>
		
		<a href=\"".($website->GetParam("SEO_URLS")==1?"http://".$DOMAIN_NAME."/news-".$arrNews["id"]."-".$website->format_str($arrNews["title"]).".html":"index.php?mod=news&id=".$arrNews["id"])."\"><h4>".stripslashes(strip_tags($arrNews["title"]))."</h4></a>
		<span class=\"sub-text\">
		".$website->text_words(stripslashes(strip_tags($arrNews["html"])),40);
		
		if(trim(strip_tags($arrNews["html"]))!="")
		{
			echo "<a title=\"".stripslashes($arrNews["title"])."\" href=\"".($website->GetParam("SEO_URLS")==1?"http://".$DOMAIN_NAME."/news-".$arrNews["id"]."-".$website->format_str($arrNews["title"]).".html":"index.php?mod=news&id=".$arrNews["id"])."\">...</a>";
		}
		echo "
		</span>
		<div class=\"clear\"></div>
		
		<hr/>		
		<br/>";
	}

}
?>