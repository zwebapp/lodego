<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<h3><?php echo $M_USERS_REGISTRATION;?></h3>
<hr/>

<?php 
$website->Title($M_USERS_REGISTRATION);
$website->MetaDescription("");
$website->MetaKeywords("");
$Step = 1;

if(isset($_POST["ProceedSend"]))
{

	$username=$_POST["username"];
	
	if
	(
		$website->GetParam("USE_CAPTCHA_IMAGES")==1
		&& ( (strtoupper($_POST['code']) != $_SESSION['code'])|| trim($_POST['code']) == "" )
	)
	{
		echo "<i><span class=\"warning_text\">".$M_WRONG_CODE."</span></i><br><br>";
		$Step = 1;
	}
	else
	if($database->SQLCount("users","WHERE username='".mysql_real_escape_string($username)."'") > 0)
	{
		echo "<i><span class=\"warning_text\">".$M_USERNAME_TAKEN."</span></i><br><br>";
																		
		$Step = 1;
	}
	else
	{
		$database->SQLInsert
		(
			"users",
			array("business_name","website","username","password","name","user_phone","user_email"),
			array( $_POST["business_name"],str_ireplace("http://","",$_POST["website"]),$username,$_POST["password"],$_POST["name"],$_POST["user_phone"],$_POST["user_email"])
		);	
		
		
		$Step = 2;	
	}
				
}

?>


<script>
function SubmitForm2(x)
{

	if(x.business_name.value=="")
	{
		alert("<?php echo $M_ENTER_BUSINESS_NAME;?>");
		x.business_name.focus();
		return false;
	}	
	
	if(x.username.value=="")
	{
		alert("<?php echo $ENTER_USERNAME;?>");
		x.username.focus();
		return false;
	}	
	
	if(x.password.value=="")
	{
		alert("<?php echo $ENTER_PASSWORD;?>");
		x.password.focus();
		return false;
	}	
	
	
	if(x.name.value=="")
	{
		alert("<?php echo $M_PLEASE_ENTER_NAME;?>");
		x.name.focus();
		return false;
	}	

	if(x.user_email.value=="")
	{
		alert("<?php echo $M_PLEASE_ENTER_EMAIL;?>");
		x.user_email.focus();
		return false;
	}	

	
	if(!CheckValidEmail(x.user_email.value) )
	{
		alert(x.user_email.value+" <?php echo $NOT_VALID_EMAIL;?>");
		x.user_email.focus();
		return false;
	}
	
	return true;
}

function CheckValidEmail(strEmail) 
{
		if (strEmail.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
		{
			return true;
		}
		else
		{
			return false;
		}
}
</script>


<?php
if($Step == 2)
{
?>

<h4><?php echo $M_ACCOUNT_CREATED;?></h4>
<br/>
<form class="no-margin" action="USERS/loginaction.php" method="post" onsubmit="return ValidateLoginForm(this)">
<input type="hidden" name="Email" value="<?php echo $username;?>"/>
<input type="hidden" name="Password" value="<?php echo $_POST["password"];?>"/>
<input type="submit" class="ibutton" value="<?php echo $M_LOG_IN_ADMIN_PANEL;?>">
</form>		


<?php
}
else
{
?>

<?php echo $M_SIGNUP_EXPL;?>
<br><br>

<form id="main" action="index.php" method="post" onsubmit="return SubmitForm2(this)">

<?php
if(isset($_REQUEST["mod"]))
{
?>
<input type="hidden" name="mod" value="<?php echo $_REQUEST["mod"];?>"/>
<?php
}
else
{
?>
<input type="hidden" name="page" value="<?php echo $_REQUEST["page"];?>"/>
<?php
}
?>

<input type="hidden" name="ProceedSend" value="1">

<fieldset>
	<ol>
	<li>
		<label for="business_name"><?php echo $M_BUSINESS_NAME;?>(*):</label>
		
		<input type="text" name="business_name" required value="<?php echo $this->get_param("business_name");?>" size="30">
	</li>	
	<li>
		<label for="username"><?php echo $M_USERNAME;?>(*):</label>
		
		<input type="text" name="username" required value="<?php echo $this->get_param("username");?>" size="30">
	</li>	
	
	<li>
		<label for="password">
			<?php echo $M_PASSWORD;?>(*): 
		</label>
		<input type="password" required name="password" size="30">
	</li>
	<li>
		<label for="name">
		<?php echo $M_NAME;?>(*): 
		</label>
		<input type="text" required placeholder="<?php echo $M_FIRST_AND_LAST;?>" name="name" value="<?php echo $this->get_param("name");?>" size="30">
	</li>
	<li>
		<label for="website">
		<?php echo $M_STORE_WEBSITE;?>: 
		</label>
		<input type="text" name="website" value="<?php echo $this->get_param("website");?>" size="30">
	</li>
	
	<li>
		<label for="user_email">
		<?php echo $M_EMAIL;?>(*):
		</label>
		 <input type="email" required name="user_email" value="<?php echo $this->get_param("user_email");?>" size="30">
	</li>
	<li>	
		<label for="user_phone">
		<?php echo $M_PHONE;?>:
		</label>
		<input type="text" name="user_phone" value="<?php echo $this->get_param("user_phone");?>" size="30">
	</li>
	
					
<?php
if($website->GetParam("USE_CAPTCHA_IMAGES")==1)
{
?>
<li>
	<label for="code">
	<img src="include/sec_image.php" width="100" height="30"/>
	</label>
	<input id="code" name="code" placeholder="<?php echo $M_PLEASE_ENTER_CODE;?>" type="text" required/>
</li>
<?php
}
?>
</ol>
</fieldset>
				
(*) <?php echo $M_REQUIRED_FIELDS;?>
<fieldset>
<button type="submit"><?php echo $M_SUBMIT;?></button>
</fieldset>
</form>

		<?php
		}
		?>


