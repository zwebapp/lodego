<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
if(!defined('IN_SCRIPT')) die("");
$NUMBER_OF_FEATURED_LISTINGS= $this->GetParam("NUMBER_OF_FEATURED_LISTINGS");
$SearchTable = $this->db->Query
("
	SELECT * FROM ".$DBprefix."listings
	WHERE 
	status = 1
	
	".($this->GetParam("ADS_EXPIRE")!=-1?" AND date>".(time()-$this->GetParam("ADS_EXPIRE")*86400):"")."
	
	ORDER BY id DESC
	LIMIT 0,".$NUMBER_OF_FEATURED_LISTINGS."
");

while($listing = mysql_fetch_array($SearchTable))
{	

	if($this->GetParam("SEO_URLS")==1)
	{
		$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_AD:"ad")."-".$this->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
	}
	else
	{
		$strLink = "index.php?mod=details&id=".$listing["id"];
	}
?>

  <a href="<?php echo $strLink;?>" class="list-group-item">
           
	<?php
	if($listing["images"]!="")
	{
		$images = explode(",",$listing["images"]);
		
		echo "<a href=\"".$strLink."\"><img align=\"left\" src=\"thumbnails/".$images[0].".jpg\" width=\"50\" alt=\"".stripslashes(strip_tags($listing["title"]))."\" class=\"img-shadow img-right-margin\" /></a>";
	}
	?>
	
	<h4 class="no-top-margin">
		<?php echo stripslashes(strip_tags($listing["title"]));?>
	</h4>
	<span class="sub-text">
	<?php echo $this->text_words(stripslashes(strip_tags($listing["description"])),13);?>
	</span>
	
	
	</a>
	
<?php
}
?>
