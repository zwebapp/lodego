<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<br/>
<?php
if(isset($_REQUEST["id"]))
{
	$id=$_REQUEST["id"];
}
else
{	
	die("The listing id was not set.");
}

$website->ms_i($id);

$listing = $database->DataArray("listings","id=".$id." ");

if($website->GetParam("SEO_URLS")==1)
{
	$strLink = ($MULTI_LANGUAGE_SITE?$M_SEO_AD:"ad")."-".$website->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
}
else
{
	$strLink = "index.php?mod=details&id=".$listing["id"];
}
										

$website->Title(stripslashes($listing["title"])." - ".$M_USER_OPINIONS);
$website->MetaDescription($website->text_words($M_USER_OPINIONS." - ".stripslashes(strip_tags($listing["description"])),30));
$website->MetaKeywords($website->format_keywords($website->text_words($M_USER_OPINIONS." ".stripslashes(strip_tags($listing["description"])),20)));



$strErrorSecCode="";
$showCommentsForm = true;


if(isset($_REQUEST["add_comment"]))
{
	$pos_http = strpos(strtolower($_POST["comment"]), "http");
	$pos_url = strpos(strtolower($_POST["comment"]), "url=");
	$pos_a = strpos(strtolower($_POST["comment"]), "<a ");
	$actual_length = strlen($_POST["comment"]);
	$stripped_length = strlen(strip_tags($_POST["comment"])); 


	if($actual_length != $stripped_length || $pos_http !== false || $pos_url !== false || $pos_a !== false)
	{
		$strErrorSecCode = " ";
	}
	
	else
	if
	(
		$website->GetParam("USE_CAPTCHA_IMAGES")==1
		&& ( (strtoupper($_POST['code']) != $_SESSION['code'])|| trim($_POST['code']) == "" )
	)
	{
		$strErrorSecCode = $M_WRONG_CODE;
	 }
	 else
	 if($database->SQLCount("comments","WHERE ip='".$_SERVER["REMOTE_ADDR"]."' AND id=".$id)>0)
	 {
		$strErrorSecCode = $M_ALREADY_POSTED;
	 }
	 else
	 {
		$database->SQLInsert
		(
			"comments",
			array("vote","ip","title","author","email","html","listing_id","date","status"),
			array($_POST["rate_product"],$_SERVER["REMOTE_ADDR"],strip_tags($_POST["title"]),strip_tags($_POST["author"]),strip_tags($_POST["email"]),strip_tags($_POST["comment"]),$id,time(),"1")
		);
		
		$website->UpdateListingRating($id);
						
		$showCommentsForm = false;
		echo "<br/><h3>".$M_YOUR_REVIEW_POSTED."</h3><br/>";
	 }
}
?>

<script>
var post_review = false;
function ShowPostForm()
{
	if(!post_review)
	{
		document.getElementById("post-review").style.display="block";
		post_review = true;
	}
	else
	{
		document.getElementById("post-review").style.display="none";
		post_review = false;
	}
}
</script>

<?php
if($showCommentsForm)
{
?>

<?php
if(!isset($_REQUEST["write"]))
{
?>
<a class="btn btn-success pull-right" href="javascript:ShowPostForm()"><?php echo $M_POST_REVIEW;?></a>
<div class="clear"></div>

<?php
}
?>

<script>
function ValidateForm(x)
{

	if(x.author.value==""){
		alert("<?php echo $M_PLEASE_ENTER_NAME;?>");
		x.author.focus();
		return false;
	}
	
	
	if(x.email.value=="")
	{
		alert("<?php echo $M_PLEASE_ENTER_EMAIL;?>");
		x.email.focus();
		return false;
	}
	
	if(x.answer2.value=="")
	{
		alert("<?php echo $M_PLEASE_ENTER_ANSWER;?>");
		x.answer2.focus();
		return false;
	}
	
	if(x.title.value=="")
	{
		alert("<?php echo $M_PLEASE_ENTER_THE_TITLE;?>");
		x.title.focus();
		return false;
	}
	
	if(x.comment.value=="")
	{
		alert("<?php echo $M_PLEASE_ENTER_COMMENTS;?>");
		x.comment.focus();
		return false;
	}
	
	
	return true;
}
</script>

<div id="post-review" <?php if(!isset($_REQUEST["write"])&&$strErrorSecCode=="") echo 'style="display:none"';?>>
	
<?php
	$rate_product="";
	if(isset($_POST["rate_product"]))
	{
		$rate_product=$_POST["rate_product"];
	}
	
	echo '
	<h3>'.($strErrorSecCode!=""?$strErrorSecCode:$M_WRITE_A_REVIEW." ".$M_FOR." \"".strip_tags(stripslashes($listing["title"]))."\"").'</h3>
	<br/>		
		<form  id="main" method="post" action="index.php"  onsubmit="return ValidateForm(this)">
		<input type=hidden name="id" value="'.$id.'">
		<input type="hidden" name="add_comment" value="1">
		<input type="hidden" name="mod" value="reviews">
			
						
			<fieldset>
				<legend>'.$M_REVIEW_DETAILS.'</legend>
				<ol>
				<li>
					<label for="rate_product">
					 '.$M_RATE_WEBSITE.'
					</label>
					<input type="radio" style="position:relative;top:7px" class="lfloat" value="5" '.($rate_product==""||$rate_product=="5"?"checked":"").' name="rate_product">
					<span class="lfloat" style="margin-right:15px">5</span>
					
					<input type="radio" style="position:relative;top:7px" class="lfloat" value="4" '.($rate_product=="4"?"checked":"").' name="rate_product">
					<span class="lfloat" style="margin-right:15px">4</span>
					
					<input type="radio" style="position:relative;top:7px" class="lfloat" value="3" '.($rate_product=="3"?"checked":"").' name="rate_product">
					<span class="lfloat" style="margin-right:15px">3</span>
					
					<input type="radio" style="position:relative;top:7px" class="lfloat" value="2" '.($rate_product=="2"?"checked":"").' name="rate_product">
					<span class="lfloat" style="margin-right:15px">2</span>
					
					<input type="radio" style="position:relative;top:7px" class="lfloat" value="1" '.($rate_product=="1"?"checked":"").' name="rate_product">
					<span class="lfloat" style="margin-right:15px">1</span>
					<div class="clear"></div>
				</li>
				<li>
					<label for="title">
					'.$M_REVIEW_TITLE.'
					</label>
					<input   id="title" name="title" value="'.(isset($_POST["title"])?$_POST["title"]:"").'" size="40" required/>
			
				</li>
				<li>
					<label for="comment">
					'.$M_COMMENTS.'
					</label>
					<textarea  id="comment" name="comment" rows="10" required cols="40">'.(isset($_POST["comment"])?$_POST["comment"]:"").'</textarea>
		
				</li>
				</ol>
			</fieldset>
			
			<fieldset>
				<legend>'.$M_Y_DETAILS.'</legend>
				<ol>
				<li>
					<label for="author">
					'.$M_NAME.'
					</label>
					<input type="text"  id="author" name="author" value="'.(isset($_POST["author"])?$_POST["author"]:"").'" size="40" required/>
				</li>
				
				<li>
					<label for="email">
					'.$M_EMAIL.'
					</label>
					<input type="email" id="email" name="email" value="'.(isset($_POST["email"])?$_POST["email"]:"").'" size="40"/>
				</li>
				
			
			';
								
		
			if($website->GetParam("USE_CAPTCHA_IMAGES")==1)
			{
			?>
			<li>
				<label for="code">
				<img src="include/sec_image.php" width="100" height="30"/>
				</label>
				<input id="code" name="code" placeholder="<?php echo $M_PLEASE_ENTER_CODE;?>" type="text" required/>
			</li>
			<?php
			}
			?>
						
			</ol>
		</fieldset>
	<fieldset>
		<button type="submit"><?php echo $M_SEND;?></button>
	</fieldset>			
	</form>
	<br/>
</div>					
<div class="clear"></div>

<?php
}

echo $M_GO_BACK_TO." <b><a style=\"text-decoration:underline\" href=\"".$strLink."\">".strip_tags(stripslashes($listing["title"]))."</a></b>";

//user opinions and reviews
echo "<br/><h3>".$M_USER_OPINIONS." ".$M_FOR." ".strip_tags(stripslashes($listing["title"]))."</h3>
<br/>";

function show_stars($vote)
{
	global $DOMAIN_NAME,$USE_ABSOLUTE_URLS;
	
	$result = "<div>";
	
	for($x=0;$x<floor($vote);$x++)
	{
		$result .= "<img src=\"".($USE_ABSOLUTE_URLS?"http://www.".$DOMAIN_NAME."/":"")."images/full-star.gif\" width=\"13\" height=\"12\" style=\"float:left\">";
	}
	
	for($c=0;$c<ceil(fmod($vote, 1) );$c++)
	{
		$result .= "<img src=\"images/half-star.gif\" width=\"13\" height=\"12\" style=\"float:left\">";
	}
	
	for($v=($c+$x);$v<5;$v++)
	{
		$result .= "<img src=\"images/empty-star.gif\" width=\"13\" height=\"12\" style=\"float:left\">";
	}

	$result .= "</div>";
	
	return $result;
}

$tableComments = $database->DataTable("comments","WHERE listing_id=".$id." ORDER BY id DESC");

if(mysql_num_rows($tableComments) == 0)											
{
	echo "<br/><i>".$M_STILL_NO_REVIEWS."</i><br/><br/><br/><br/><br/><br/><br/>";
}

while($arrComment = mysql_fetch_array($tableComments))
{
	echo show_stars($arrComment["vote"]);
	echo '<b style="position:relative;left:10px;top:-5px">'.strip_tags(stripslashes($arrComment["title"])).'</b>';
	echo "<br/>".$M_BY." <b>".$arrComment["author"]."</b>";
	echo ', '.date($website->GetParam("DATE_FORMAT"), $arrComment["date"]);
	echo '<hr style="margin:5px"/>';
	echo '
	
	'.strip_tags(stripslashes($arrComment["html"])).'
	';	
	
	
			
		
	echo '<br><br><br>';
}



?>

<?php

//end user opinions and reviews

?>