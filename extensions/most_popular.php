<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
if(!defined('IN_SCRIPT')) die("");
?>
<h3><?php echo $M_MOST_POPULAR;?></h3>
<hr/>
<?php
$RESULTS_PER_PAGE = $website->GetParam("RESULTS_PER_PAGE");

$SearchTable = $database->Query
("
	SELECT * FROM ".$DBprefix."listings
	WHERE 
	".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
	".$DBprefix."listings.status = 1
	
	ORDER BY 
	
	".$DBprefix."listings.visits DESC
	LIMIT 0,".$RESULTS_PER_PAGE."
");



$iNResults = mysql_num_rows($SearchTable);
	
if($iNResults == 0)
{
	echo "<br/><i class=\"sub-text\">".$M_NO_RESULTS_FOUND."</i>";
}
else
{
		
	$iTotResults = 0;

	if(!isset($_REQUEST["num"]))
	{
		$num = 0;
	}
	else
	{
		$website->ms_i($_REQUEST["num"]);
		$num = $_REQUEST["num"] - 1;
	}
		

	$i_listings_counter = 0;


	while($listing = mysql_fetch_array($SearchTable))
	{
		$i_listings_counter++;
						
		$adFeatured = false;
		
		if($listing["featured"]==1)
		{
			$adFeatured = true;
		}
		
		
		if($website->GetParam("SEO_URLS")==1)
		{
			$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_AD:"ad")."-".$website->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
		}
		else
		{
			$strLink = "index.php?mod=details&id=".$listing["id"].($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
		}
		?>
		<a href="<?php echo $strLink;?>">
		<div class="<?php if($adFeatured) echo "featured_";?>listing">
	
			<?php
			if(trim($listing["images"])!="")
			{
			?>
			<div class="col-sm-3 image-wrapper">
				<div class="result-image">
					<?php
					echo $website->show_pic($listing["images"],"small");
					?>
				</div>
			</div>
			<?php
			}
			?>
		  
			<div class="col-sm-9">
				
				<span class="<?php if($adFeatured) echo "featured_";?>listing_title"><?php echo stripslashes(strip_tags($listing["title"]));?></span>
				
				<?php
				if(trim($listing["featured"]) == "1")
				{
				?>
					<hr class="featured-hr"/>
				<?php
				}
				else
				{
				?>
					<div class="smooth-separator"></div>
				<?php
				}
				?>
				
				<?php
				if(trim($listing["fields"]) != "")
				{
				?>
					<div class="listing_description">
					<?php 
					$listing_fields = unserialize($listing["fields"]);
			
					if(is_array($listing_fields))
					{
						foreach($listing_fields as $key=>$value)
						{
						?>
							<span class="img-right-margin"><?php echo $key;?>: <?php echo $value;?></span>
						<?php
						}
					}
					
					?>
					</div>
				<?php
				}
				?>
				
				
				<?php
				if(trim($listing["description"]) != "")
				{
				?>
					<div class="listing_description">
					<?php echo $website->text_words(stripslashes(strip_tags($listing["description"])),30);?>
					</div>
				<?php
				}
				?>
				<div class="clear"></div>
				<span class="listing_posted_date"><?php echo $M_TOTAL_HITS;?>: <?php echo $listing["visits"];?></span>
				
			
			</div>
			
			<div class="clear"></div>
			
			
		</div>
		</a>
		<div class="clear"></div>
		<hr/>	
		
		<?php
			
		
		$iTotResults++;
	}

}
?>