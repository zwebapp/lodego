<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
$website->Title("Verify your email address");
$website->MetaDescription($website->text_words(stripslashes(strip_tags($listing["description"])),30));
$website->MetaKeywords($website->format_keywords($website->text_words(stripslashes(strip_tags($listing["description"])),20)));

$id=$_REQUEST["id"];
$website->ms_i($id);

$listing = $database->DataArray("listings","id=".$id);

if($_REQUEST["code"] ==  md5($DOMAIN_NAME.$listing["code"]) )
{
	$database->SQLUpdate
		(
			"listings",
			array
			(
				"email_verified"
			),
			array
			(
				"1"
			),
			"id=".$id.""
		);
	?>
		<h3>Your email address was verified successfully!</h3>
	<?php
}
else
{
?>
	<h3>There was an error while validating your email address!</h3>
<?php
}

?>