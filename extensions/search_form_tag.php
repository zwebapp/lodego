<form id="search-form" method="post" action="index.php">
<input type="hidden" name="mod" value="search"/>
<?php
if($MULTI_LANGUAGE_SITE)
{
?>
<input type="hidden" name="lang" value="<?php echo $this->lang;?>"/>
<?php
}
?>
<input type="text" class="search-form-field" name="search_by" value="" placeholder="<?php echo $M_SEARCH;?>" required autofocus/>

<input type="image" class="search-form-image" alt="search icon" src="images/search-button.png"/>
</form>