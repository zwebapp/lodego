<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
if(!defined('IN_SCRIPT')) die("");
?>
<div class="margin-top-5">
		
		<a href="#" class="underline-link pull-right" type="button" data-toggle="collapse" data-target=".cat-collapse">
			<?php echo $M_BROWSE_DEALS_LOCATION;?>
		</a>
		<div class="clear"></div>
</div>

	<div class="container">
	
		<div class="collapse cat-collapse text-left">
			<div class="container">
				<hr class="clear"/>
				<br/>
<?php
$NUMBER_OF_CATEGORIES_PER_ROW = $this->GetParam("NUMBER_OF_CATEGORIES_PER_ROW");
$b_first_sub_category = true;
$i_sub_counter=0;
$i_category_counter=0;
								
if(!isset($l))
{
	include_once("locations/locations_array.php");
}
asort($l);
foreach($l as $key=>$value)
{
	if(!is_string($value)) continue;
	
	if($this->GetParam("SEO_URLS")==1)
	{
		$strLink = ($MULTI_LANGUAGE_SITE?$M_SEO_LOCATION:"location")."-".$this->format_str($value)."-".str_replace(".","-",$key).".html";
	}
	else
	{
		$strLink = "index.php?mod=search&location=".str_replace(".","-",$key).($MULTI_LANGUAGE_SITE?"&lang=".$this->lang:"");
	}
	
	$b_first_sub_category = true;
	$i_sub_counter=0;
	
	echo "\n<div class=\"col-sm-4 no-left-padding margin-bottom-10\">\n";
		
	echo "\n<span class=\"category_link\">";
	
	if($this->GetParam("SEO_URLS")==1)
	{
		$strLink = ($MULTI_LANGUAGE_SITE?$M_SEO_LOCATION:"location")."-".$this->format_str($value)."-".str_replace(".","-",$key).".html";
	}
	else
	{
		$strLink = "index.php?mod=search&location=".str_replace(".","-",$key).($MULTI_LANGUAGE_SITE?"&lang=".$this->lang:"");
	}
	 
	echo "\n<a href=\"".$strLink."\" class=\"category_link\">".trim($value)."</a>";
	
	echo "</span>\n";
	
	if(isset($l1[$key]))
	{
		foreach($l1[$key] as $sub_key=>$sub_location)
		{
			if(!is_string($sub_location)) continue;
		
			if($i_sub_counter>=8)
			{
				echo "...";
				break;
			}
			
			if(!$b_first_sub_category) echo ", ";
			
			echo "\n<span class=\"sub-text\">".stripslashes(trim($sub_location))."</span>";
			$b_first_sub_category = false;
			$i_sub_counter++;
		}
	}
	
	echo "</div>\n";
	
	$i_category_counter++;
	
	if(($i_category_counter % $NUMBER_OF_CATEGORIES_PER_ROW) == 0)
	{
		echo "\n<div class=\"clear\"></div>";
	}
		
	
}



?>
	<div class="clear"></div>	
	<hr/>
		</div>
	</div>
	

</div>
<div class="clear margin-top-10"></div>
