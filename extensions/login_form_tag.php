<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<script src="js/login.js"></script>
<div id="main-login-form">
	<a href="javascript:HideLogin()"><img class="close-login-icon" alt="close" src="images/closeicon.png"/></a>
	<h3 class="lfloat">
	<?php
	if(isset($_REQUEST["error"])&&$_REQUEST["error"]=="login")
	{
		echo $LOGIN_ERROR_MESSAGE;
	}
	else
	if(isset($_REQUEST["error"])&&$_REQUEST["error"]=="no")
	{
		echo $LOGIN_EMPTY_FIELD_MESSAGE;
	}
	else
	if(isset($_REQUEST["error"])&&$_REQUEST["error"]=="expired")
	{
		echo $LOGIN_EXPIRED_MESSAGE;
	}
	else
	{
	  echo $M_LOGIN;
	}
	?>
	</h3>
	
		<hr class="clear"/>
		<?php

		$logged_user_type="";

		$AUTH = false;
		
		if((isset($_COOKIE["AuthU"]))&&$_COOKIE["AuthU"]!="")
		{ 
			$logged_user_type="agent";
			$ProceedApply = true;
			$arrLoginItems = explode("~", $_COOKIE["AuthU"]);
			
			$username = $arrLoginItems[0];
			$password = $arrLoginItems[1];
			
			$AUTH = true;
			
			?>
			
			<form action="USERS/logout.php" method="post" class="no-margin">
			<input type="hidden" name="proceed" value="d"/>
			<input type="button" class="ibutton" value=" <?php echo $MY_AREA;?> " onclick="document.location.href='USERS/index.php'">
			&nbsp;
			<input type="submit" class="ibutton" value=" <?php echo $M_LOGOUT;?> "/>
			</form>
			<?php
		}
		else
		{
			?>

			<script>
			function ValidateLoginForm(x)
			{
				if(x.Email.value==""){
					alert("<?php echo $USERNAME_EMPTY_FIELD_MESSAGE;?>");
					x.Email.focus();
					return false;
				}
				else
				if(x.Password.value==""){
					alert("<?php echo $PASSWORD_EMPTY_FIELD_MESSAGE;?>!");
					x.Password.focus();
					return false;
				}
				return true;
			}
			</script>
			<?php

			if(isset($_REQUEST["return_url"])&&$_REQUEST["return_url"]!="")
			{

			}
			else
			{
				$return_url="";
				if(isset($_REQUEST["return_category"])) $return_url.="&category=".$_REQUEST["return_category"];
				if(isset($_REQUEST["return_action"])) $return_url.="&action=".$_REQUEST["return_action"];
				
			}


				
			?>
			
			<form class="no-margin" action="USERS/loginaction.php" method="post" onsubmit="return ValidateLoginForm(this)">
			
			<table class="bcollapse">
			<?php
			if(isset($_REQUEST["return_url"]))
			{
			?>
			<input type="hidden" name="return_url" value="<?php echo $_REQUEST["return_url"];?>">
			<?php
			}
			?>
				<tr>
				
					<td ><?php echo $M_USERNAME;?>:</td>
					<td><input type="text" size="40" class="form-field" name="Email"/></td>
					
				</tr>
				<tr>
					<td ><?php echo $M_PASSWORD;?>:</td>
					<td><input  size="40" class="form-field" type="password" name="Password"/></td>
					
				</tr>
				<tr>
					<td colspan="2">  
					<br/>
					<a class="lfloat margin-20" href="<?php echo ($this->GetParam("SEO_URLS")?'mod-forgotten_password.html':'index.php?mod=forgotten_password');?>"><?php echo $M_FORGOTTEN_PASSWORD;?></a> 
					
					
					<input type="submit" class="ibutton rfloat margin-20" value="<?php echo $M_LOGIN;?>"/></td>
				</tr>
				
			</table>
			</form>
		<?php
		}
		?>
	<br/>
	
	<br/>
</div>
<?php
if(isset($_REQUEST["show_login"]))
{
	echo "
		<script>document.getElementById('main-login-form').style.display='block';</script>
	";
}

if(isset($_REQUEST["error"]))
{
			
	echo "
		<script>document.getElementById('main-login-form').style.display='block';</script>
	";
}	
?>