<h3><?php echo $M_FEATURED_ADS;?></h3>
<hr/>

<?php
if(!defined('IN_SCRIPT')) die("");
$inner_code = "";
$indicator_code = "";
$SearchTable = $database->Query
("
	SELECT * FROM ".$DBprefix."listings
	WHERE 
	status = 1
	AND
	".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
	images<>''
	ORDER BY RAND()
	LIMIT 0,5
");
$i_carousel_counter = 1;
while($listing = mysql_fetch_array($SearchTable))
{
	if($website->GetParam("SEO_URLS")==1)
	{
		$strLink = "http://".$DOMAIN_NAME."/".($MULTI_LANGUAGE_SITE?$M_SEO_AD:"ad")."-".$website->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
	}
	else
	{
		$strLink = "index.php?mod=details&id=".$listing["id"];
	}
	
	if($listing["images"]!="")
	{
		$images = explode(",",$listing["images"]);
	}	

	$indicator_code .= '<li data-target="#myCarousel" data-slide-to="'.($i_carousel_counter-1).'" '.($i_carousel_counter == 1?'class="active"':'').'><img src="thumbnails/'.$images[0].'.jpg"  alt="" class="img-shadow indicator-image"/></li>';
		
	$inner_code .= '<div class="item '.($i_carousel_counter==1?"active":"").' slide-back-'.$i_carousel_counter.'">
		<div class="container">
			<div class="carousel-caption">';
			
			$inner_code .= "
			<a class=\"carousel-link\" href=\"".$strLink."\">
			<img class=\"img-shadow rfloat img-right-margin\" src=\"thumbnails/".$images[0].".jpg\" width=\"150\" alt=\"".stripslashes(strip_tags($listing["title"]))."\"/>";
			 $inner_code .= '<h1 class="inline-display">'.stripslashes(strip_tags($listing["title"])).'</h1>
			  <p>'.$website->text_words(stripslashes(strip_tags($listing["description"])),23).'</p>
			  </a>
			 <br/>
			</div>
		</div>
	</div>';

	$i_carousel_counter++;
}
?>		
	
<?php
if(mysql_num_rows($SearchTable) > 0)
{
?>
<div id="myCarousel" class="carousel slide">
  
	<ol class="carousel-indicators">
		<?php
			echo $indicator_code;
		?>
	</ol>
	<div class="carousel-inner">
	 	<?php
			echo $inner_code;
		?>
	</div>
	
	  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><img src="images/carousel-arrow-left.png" alt="left" class="carousel-icon"/></a>
	  <a class="right carousel-control" href="#myCarousel" data-slide="next"><img src="images/carousel-arrow-right.png" alt="right" class="carousel-icon"/></a>
</div>
<?php
}
?>	
<br/><br/>
<h3><?php echo $M_BROWSE_CATEGORY;?></h3>
<hr/>

<?php

$arr_listings_count = array();

if($website->GetParam("SHOW_LISTINGS_NUMBER")==1)
{

	$count_listings = $database->Query
	("
		SELECT count(id) c, 
		link_category 
		FROM ".$DBprefix."listings
		WHERE
		".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
		status=1
		GROUP BY link_category
	");

	while($count_listing = mysql_fetch_array($count_listings))
	{
		$strCat = explode(".",$count_listing["link_category"],2);
		
		if(!isset($arr_listings_count[$strCat[0]]))
		{
			$arr_listings_count[$strCat[0]]=0;
		}
		$arr_listings_count[$strCat[0]]  += $count_listing["c"];
	}

}



$NUMBER_OF_CATEGORIES_PER_ROW = $website->GetParam("NUMBER_OF_CATEGORIES_PER_ROW");

if(file_exists('categories/categories_'.strtolower($website->lang).'.php'))
{
	$categories_content = file_get_contents('categories/categories_'.strtolower($website->lang).'.php');
}
else
{
	$categories_content = file_get_contents('categories/categories_en.php');
}

$cat_lines = explode("\n", trim($categories_content));

$b_first_sub_category = true;
$i_sub_counter=0;
$i_category_counter=0;

$arr_categories=array();

foreach($cat_lines as $strCategory)
{
	list($key,$value)=explode(". ",$strCategory);
	$arr_categories[trim($key)]=trim($value);
	
	if($website->GetParam("SEO_URLS")==1)
	{
		$strLink = ($MULTI_LANGUAGE_SITE?$M_SEO_CATEGORY:"category")."-".$website->format_str($value)."-".str_replace(".","-",$key).".html";
	
	}
	else
	{
		$strLink = "index.php?mod=search&category=".str_replace(".","-",$key).($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
	}
		
	if(substr_count($key, '.') == 0)
	{
		if($i_category_counter!=0) echo "\n</div>";
		
		if($i_category_counter!=0 && ($i_category_counter % $NUMBER_OF_CATEGORIES_PER_ROW) == 0)
		{
			echo "\n<div class=\"clear\"></div><br/>";
		}
		
		echo "\n<div class=\"col-md-4 no-left-padding\" >\n";
		
		echo "\n<span class=\"category_link\">";
		echo "<a href=\"".$strLink."\" title=\"".trim($value)."\">".trim($value)."</a>";
		if($website->GetParam("SHOW_LISTINGS_NUMBER")==1)
		{
			echo " <span class=\"category-count\">(".(isset($arr_listings_count[$key])?$arr_listings_count[$key]:"0").")</span>";
		}
		echo "</span>";
	
		$b_first_sub_category = true;
		$i_sub_counter=0;
		$i_category_counter++;
	}
	else
	{
		
		if($i_sub_counter<7)
		{
			if($i_sub_counter!=0) echo ", ";	

			echo "<span class=\"sub-text\">".trim($value)."</span>";
			
			if(false&&$website->GetParam("SHOW_LISTINGS_NUMBER")==1)
			{
				echo " <span class=\"category-count\">(".(isset($arr_listings_count[$key])?$arr_listings_count[$key]:"0").")</span>";
			}
			
		}
		if($i_sub_counter==7) echo " ...";
		$b_first_sub_category = false;
		$i_sub_counter++;
	}
	
}

echo "</div><div class=\"clear\"></div>";
?>
<br/>
<br/>