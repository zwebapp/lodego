<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
if(!defined('IN_SCRIPT')) die("");

$website->Title($M_SUBMIT_COUPON);
$website->MetaDescription("");
$website->MetaKeywords("");


if(!isset($_REQUEST["package"]))
{
?>
<div class="container">
    <div class="row">
        <h3><?php echo $M_SELECT_PLAN;?></h3>
        <hr>
		<?php
		$listing_packages = $database->DataTable("packages","WHERE active=1 ORDER BY link_category,price");
		$first_paid_package=true;
		while($listing_package = mysql_fetch_array($listing_packages))
		{
		?>
		<a href="index.php?mod=new_listing&package=<?php echo $listing_package["id"];?>">
        <div class="col-sm-4">
          <div class="panel pricing-panel panel-<?php if($first_paid_package&&$listing_package["price"]!=0) echo "success";else echo "default";?> text-center">
            <div class="panel-heading">
              <h3><?php echo stripslashes($listing_package["name"]);?></h3>
            </div>
            <div class="panel-body">
              <h3>
				<?php 
				if($listing_package["price"]==0)
				{
					echo $M_FREE;
				}
				else
				{
				
					echo $website->GetParam("WEBSITE_CURRENCY").number_format($listing_package["price"],2,'.',',');
				}
				?>
			  
			  </h3>
            </div>
            <ul class="list-group">
				<li class="list-group-item">
				<?php 
				if($listing_package["featured"]==1)
				{
					echo $M_FEATURED_LISTING;
				}
				else
				{
					echo $M_REGULAR_LISTING;
				}
				?>
				
				</li>
			
           
              
              <li class="list-group-item">
				<?php
				if(trim($listing_package["description"])!="")
				{
				?>
					<?php echo stripslashes($listing_package["description"]);?>
					
				<?php
				}
				?>
			  
			  </li>
              <li class="list-group-item"><span class="btn btn-<?php if($first_paid_package&&$listing_package["price"]!=0) echo "success";else echo "default";?>"><?php echo $M_SELECT;?></span></li>
            </ul>
          </div>          
        </div>
		<?php
			if($listing_package["price"]>0)
			{
				$first_paid_package=false;
			}
		}
		?>
        
 
      </div>
    </div> 
	
	<br/>
	<i>
	<a href="<?php if($website->GetParam("SEO_URLS")==1) echo "mod-".($MULTI_LANGUAGE_SITE?($website->lang)."-":"")."registration.html";else echo "index.php?mod=registration".($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");?>">
		<?php echo $M_SIGNUP_EXPLANATION;?>
	</a>
	</i>
	<br/>
	<br/>
	<br/>
<?php

}
else
{


	$show_form = true;
	$process_error="";

	if(isset($_POST["ProceedSend"]))
	{
		//print_r($_POST);die("1");
		if
		(
			$website->GetParam("USE_CAPTCHA_IMAGES")==1
			&& ( (strtoupper($_POST['code']) != $_SESSION['code'])|| trim($_POST['code']) == "" )
		)
		{
			$process_error=$M_WRONG_CODE;
		}
		else
		if(trim($_POST["title"])=="")
		{
			$process_error=$M_PLEASE_ENTER_TITLE;
		}
		else
		
		if(!isset($_POST["category_1_0"])||trim($_POST["category_1_0"])=="")
		{
			$process_error=$M_PLEASE_SELECT_CATEGORY;
		}
		
		else
		{
			
			$website->ms_i($_POST["package"]);
			
			$selected_package=$database->DataArray("packages","id=".$_REQUEST["package"]);
		
			///images processing
			$str_images_list = "";
				
			include("include/images_processing.php");
			///end images processing
		
			
			$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

			$modify_code = '';
			
			for ($i = 0; $i < 24; $i++) 
			{
				$modify_code .= $characters[rand(0, strlen($characters) - 1)];
			}
			
			
			
			$arrPValues = array();
			
			$selected_loc_items = explode(".",$_POST["location"]);
			
			
			
			
			$category_fields_table= $database->DataTable("fields","");
			
			while($category_fields = mysql_fetch_array($category_fields_table))
			{
				if(isset($category_fields["id"]))
				{
					$arr_cat_fields = unserialize(stripslashes($category_fields["fields"]));
					$current_cat = str_replace(".","-",$category_fields["cat_id"]);
					
					if(is_array($arr_cat_fields))
					{
						$iFCounter = 0;
					
						foreach($arr_cat_fields as $arr_cat_field)
						{	
						
							if(isset($_POST["pfield-".$current_cat."-".$iFCounter]))
							{
								$arrPValues[$arr_cat_field[0]]=trim(strip_tags($_POST["pfield-".$current_cat."-".$iFCounter]));
								$iFCounter++;
							}
						}	
					}
				}
			}
			
			function get_cat_value($field_name)
			{
				for($i=4;$i>=0;$i--)
				{
					if(isset($_REQUEST[$field_name."_".$i]))
					{
						return $_REQUEST[$field_name."_".$i];
						
					}
				}
				return "";
			}
			
			$link_category=get_cat_value("category_1");
		
			$i_submit_id = $database->SQLInsert
			(
				"listings",
				array
				(
					"offer_type",
					"coupon",
					"offer_link",
					"offer_expires",
					"package",
					"status",
					"title",
					"url",
					"description",
					
					"link_category",
					
					"name",
					"phone",
					"email",
					"date",
					"featured",
					"expires",
					"ip",
					"location",
					
					"images",
					"code",
					"fields",
					"address",
					"latitude",
					"longitude",
					"email_verified"
				),
				array
				(
					$_POST["offer_type"],
					$_POST["coupon"],
					$_POST["offer_link"],
					$_POST["offer_expires"],
					$_POST["package"],
					$website->GetParam("AUTO_APPROVE"),
					$_POST["title"],
					str_ireplace("http://","",stripslashes(strip_tags($_POST["url"]))),
					$_POST["description"],
					
					$_POST["link_category"],
					$_POST["name"],
					$_POST["phone"],
					$_POST["email"],
					time(),
					$selected_package["featured"],
					($selected_package["days"]==0?(time()+$website->GetParam("ADS_EXPIRE")*24*3600):(time()+$selected_package["days"]*24*3600)),
					$_SERVER["REMOTE_ADDR"],
					$_POST["location"],
					
					$str_images_list,
					$modify_code,
					serialize($arrPValues),
					$_POST["address"],
					$_POST["latitude"],
					$_POST["longitude"],
					($website->GetParam("VERIFY_EMAIL")==1?"0":"1")
				)
			);
		
		
			$SYSTEM_EMAIL_FROM=$website->GetParam("SYSTEM_EMAIL_FROM");
			$SYSTEM_EMAIL_ADDRESS=$website->GetParam("SYSTEM_EMAIL_ADDRESS");
		
			$headers  = "From: \"".$SYSTEM_EMAIL_FROM."\"<".$SYSTEM_EMAIL_ADDRESS.">\n";
			
				
			//send email alerts
			
			//end email alerts
		
			$show_form=false;
			?>
			
			<?php
			if($selected_package["price"]==0)
			{
		
				echo nl2br(stripslashes($website->GetParam("SUCCESS_MESSAGE_FREE")));
			
			}
			else
			{
			?>
			
				<?php
				echo nl2br(stripslashes($website->GetParam("SUCCESS_MESSAGE_PAID")));
				?>
				<br/><br/>
				<?php echo $M_PLEASE_SELECT_PAYMENT;?>
				
				<?php
				if(trim($website->GetParam("PAYPAL_ID")) !="")
				{
				?>	<br/><br/>
					<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
					<input type="hidden" name="cmd" value="_xclick">
					<input type="hidden" name="business" value="<?php echo $website->GetParam("PAYPAL_ID");?>">
					<input type="hidden" name="currency_code" value="<?php echo $website->GetParam("CURRENCY_CODE");?>">
					<input type="hidden" name="item_name" value="Payment for ad id#<?php echo $i_submit_id;?> on <?php echo $DOMAIN_NAME;?>">
					<input type="hidden" name="item_number" value="<?php echo $i_submit_id;?>">
					<input type="hidden" name="amount" value="<?php echo number_format($selected_package["price"], 2, '.', '');?>">
					<input type="image"  src="images/paypal.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
					</form>
				<?php
				}
				?>
				
				<?php
				if(trim($website->GetParam("2CHECKOUT_ID")) !="")
				{
				?>	<br/><br/>
				
					<form target="_blank" action="https://www.2checkout.com/cgi-bin/sbuyers/cartpurchase.2c" method="post">
					<input type="hidden" name="sid" value="<?php echo trim($website->GetParam("2CHECKOUT_ID"));?>"> 
					<input type="hidden" name="cart_order_id" value="<?php echo $i_submit_id;?>"> 
					<input type="hidden" name="total" value="<?php echo number_format($selected_package["price"], 2, '.', '');?>">
					<input type="hidden" name="skip_landing" value="1"> 
					<input type="image" src="images/2checkout.gif" alt="" border="0">
					</form>
					
				<?php
				}
				?>
				
			<?php
			
			}
			
			?>
			
			<?php
			if($website->GetParam("VERIFY_EMAIL")==1)
			{
				
				$email_subject = stripslashes($website->GetParam("VERIFY_EMAIL_SUBJECT"));
				$email_text = str_replace("{VERIFY_LINK}","http://".$DOMAIN_NAME."/index.php?mod=verify_email&code=".md5($DOMAIN_NAME.$modify_code)."&id=".$i_submit_id,stripslashes($website->GetParam("VERIFY_EMAIL_MESSAGE")));
			
				mail
				(
					$_POST["email"],
					$email_subject,
					$email_text, 
					$headers
				);
			?>
				<br/><br/>
				An email to verify your email address has been
				sent to: <b><?php echo $_POST["email"];?></b>
				<br/><br/>
				Please click on the link in it, in order to verify your email
				address.
				
			<?php
			}
			?>
			
			<br/><br/>
			
			<?php echo $M_CODE_TO_MODIFY;?>
			<b><?php echo $modify_code;?></b>
			
			
			<?php
			
		}
		
		
		

	}


	if($show_form)
	{

	?>
	  <h3>
        
	<?php
	if($process_error=="")
	{
		echo $M_SUBMIT_LISTING;
	}
	else
	{
		echo $process_error;
	}
	?>
	</h3>
	<hr>

	<br/>
	<script>
	function gSubmitForm(x)
	{
		<?php
		if(!isset($_REQUEST["location"]))
		{
		?>
		document.getElementById("location").value=selected_location;
		<?php
		}
		?>
		
		<?php
		if(!isset($_REQUEST["link_category"]))
		{
		?>
		document.getElementById("link_category").value=category_selected_location;
		<?php
		}
		?>
		
		
		return true;
	}
	function OfferType(x)
		{
			if(x==1)
			{
				document.getElementById("type_coupon").style.display="block";
				document.getElementById("type_printable").style.display="none";
			}
			else
			if(x==2)
			{
				document.getElementById("type_coupon").style.display="none";
				document.getElementById("type_printable").style.display="block";
			}
			else
			{
				document.getElementById("type_coupon").style.display="none";
				document.getElementById("type_printable").style.display="none";
			}
		}
	</script>
	<form id="main" onsubmit="return gSubmitForm(this)" action="index.php" method="post"  enctype="multipart/form-data">
	<?php
	
	$website->ms_i($_REQUEST["package"]);
			
	$selected_package=$database->DataArray("packages","id=".$_REQUEST["package"]);
	?>
	<input type="hidden" name="package" value="<?php echo $selected_package["id"];?>"/>
	<?php	
		
	if(isset($_REQUEST["lang"]))
	{
	?>
	<input type="hidden" name="lang" value="<?php echo $_REQUEST["lang"];?>"/>
	<?php
	}

	if(isset($_REQUEST["mod"]))
	{
	?>
	<input type="hidden" name="mod" value="<?php echo $_REQUEST["mod"];?>"/>
	<?php
	}
	else
	{
	?>
	<input type="hidden" name="page" value="<?php echo $_REQUEST["page"];?>"/>
	<?php
	}
	?>
	<input type="hidden" name="ProceedSend" value="1"/>

	<div style="display:none"><select></select></div>

		<fieldset>
			<legend>
			
			<?php 
				
				echo $M_ENTER_AD_DETAILS;
				
			?>
			
			</legend>
			<ol>
			<li>
			<div class="div_label">
					<?php echo $M_OFFER_TYPE;?>(*)
				</div>
				
				<div class="div_field">
				
					<input type="radio" checked name="offer_type" onmousedown="javascript:OfferType(1)" value="1"/>
					<?php echo $M_COUPON_CODE;?>
					<div class="clear"></div>
					<input type="radio" name="offer_type" onmousedown="javascript:OfferType(2)" value="2"/>
					<?php echo $M_PRINTABLE_COUPON;?>
					<div class="clear"></div>
					<input type="radio" name="offer_type" onmousedown="javascript:OfferType(3)" value="3"/>
					<?php echo $M_SALE_TIP;?>
					<div class="clear"></div>
					
					
				</div>
			<div class="clear"></div>
			</li>
			
			

			<div id="type_coupon">
				<li>
				<div class="div_label"><?php echo $M_COUPON_CODE;?>:</div>
				
				<div class="div_field">
					<input class="large-field" id="coupon_code" name="coupon_code"  <?php if(isset($_REQUEST["coupon_code"])) echo "value=\"".stripslashes($_REQUEST["coupon_code"])."\"";?> type="text" placeholder="<?php echo $M_EX_COUPON;?>"/>
				</div>
				<div class="clear"></div>
				</li>
			</div>	
			<div class="clear"></div>
			
			<div id="type_printable" class="no-display">
				<li>
				<div class="div_label"><?php echo $M_OFFER_LINK;?>:</div>
				
				<div class="div_field">
					<input class="large-field"  id="offer_link" name="offer_link"  <?php if(isset($_REQUEST["offer_link"])) echo "value=\"".stripslashes($_REQUEST["offer_link"])."\"";?> type="text" placeholder="<?php echo $M_EX_OFFER;?>"/>
				</div>
				<div class="clear"></div>
				</li>
			</div>			
		
			<div class="clear"></div>
			
			<li>
		
				<?php
				$list_cat_ids ="";
				?>
				
					<div class="div_label">
						<?php echo $M_CATEGORY;?>(*):
					</div>
					
					<div class="div_field">
						
						
						<select name="link_category_menu" id="link_category_menu" required>
						
						<?php
						
						
						$categories_content = file_get_contents('categories/categories_en.php');
						
						$arrCategories = explode("\n", trim($categories_content));

						$categories=array();
						
						foreach($arrCategories as $str_category)
						{
							list($key,$value)=explode(". ",$str_category);
							$categories[trim($key)]=trim($value);
							
							$i_level = substr_count($key, ".");
							
							if($i_level!=0) continue;
							
							if($list_cat_ids!="") $list_cat_ids.=",";
							$list_cat_ids .=trim($key);
							
							echo "<option ".(isset($_REQUEST["link_category"])&&$_REQUEST["link_category"]==trim($key)?"selected":"")." value=\"".trim($key)."\">".trim($value)."</option>";
						}
						
						
						?>
						
						
						</select>
						
						<script>
						var category_suggest_location_url="include/suggest_category.php";
						</script>
						<script type="text/javascript" src="include/multi_category_select.js"></script>
						<input type="hidden" name="link_category" id="link_category" value="">
						<script type="text/javascript">
						var m_all="<?php echo $M_PLEASE_SELECT;?>";
						category_transform_select("link_category_menu","390px","220px");
						</script>
					
						
					</div>	
				
					<div class="clear"></div>
			
			<script>
			var main_cat_ids=new Array(<?php echo $list_cat_ids;?>);
			</script>
			<div class="clear"></div>
			</li>
				<li>
					<label for="title"><?php echo $M_TITLE;?>(*)</label>
					<input id="title" name="title"  <?php if(isset($_REQUEST["title"])) echo "value=\"".stripslashes($_REQUEST["title"])."\"";?> type="text" placeholder="<?php echo $M_PLEASE_ENTER_TITLE;?>" required autofocus/>
				</li>
				
				<li>
					<label for="description"><?php echo $M_DESCRIPTION;?>(*)
					<br>
					
					</label>
					<textarea id="description" required name="description" rows="10"><?php if(isset($_REQUEST["description"])) echo stripslashes($_REQUEST["description"]);?></textarea>
					
				</li>
				
				<li>
				<label for="expires">
				<?php echo $M_OFFER_EXPIRES;?>(*)
				<br>
				
				</label>
				 <link href="css/datepicker.css" rel="stylesheet">
				 
				<script src="js/bootstrap-datepicker.js"></script>
	
				<input type="text" name="offer_expires" value="<?php echo date("m/d/Y",time()+14*86400);?>" id="dp-expires" >
				<script>
				$(function(){
					
					$('#dp-expires').datepicker({
						format: 'mm/dd/yyyy'
					});
					
					});
				</script>
			</li>
			
		</fieldset>
		
	<fieldset>
			<legend><?php echo $M_ADDITIONAL_INFO;?></legend>
			<ol>
			
			
				
				<li>
					<div class="div_label">
						<?php echo $M_IMAGES;?>
						
					</div>
					
					<div class="div_field">
					
						<input type="file" name="images[]" id="images"/>
					</div>
					<div class="clear"></div>
				</li>
				
				
				<li>
					<label for="url"><?php echo $M_WEBSITE;?></label>
					<input id="url" name="url"  <?php if(isset($_REQUEST["url"])) echo "value=\"".$_REQUEST["url"]."\"";?> type="text" placeholder="www.domain.com"/>
				</li>
				<li>
					<label for="phone"><?php echo $M_PHONE;?></label>
					<input id="phone" <?php if(isset($_REQUEST["phone"])) echo "value=\"".$_REQUEST["phone"]."\"";?> name="phone" placeholder="" type="text"/>
				</li>
				
				<?php
				
				$category_fields_table = $database->DataTable("fields","");
				
				while($category_fields = mysql_fetch_array($category_fields_table))
				{
					$arr_cat_fields = unserialize(stripslashes($category_fields["fields"]));
					
					if(!is_array($arr_cat_fields)) continue;
					
					$iFCounter = 0;
						
					foreach($arr_cat_fields as $arr_cat_field)
					{
					
						if(!is_array($arr_cat_field) || sizeof($arr_cat_field)!=2) continue;
						
							
						$field_name = $arr_cat_field[0];
						$field_values = $arr_cat_field[1];
						
						echo "\n<li>";
						echo "\n<label for=\"pfield-".$category_fields["cat_id"]."-".$iFCounter."\">".stripslashes($field_name)."</label>";
						
						if(trim($field_values) != "")
						{
							echo  "\n<select  name=\"pfield-".str_replace(".","-",$category_fields["cat_id"])."-".$iFCounter."\">";
								
							$arrFieldValues = explode("\n", trim($field_values));
																
							if(sizeof($arrFieldValues) > 0)
							{
								foreach($arrFieldValues as $strFieldValue)
								{
									$strFieldValue = trim($strFieldValue);
									if(strstr($strFieldValue,"{"))
									{
										$strVName = substr($strFieldValue,1,strlen($strFieldValue)-2);
										echo  "<option ".(trim($$strVName)==$arrPropFields[$arrPropertyField[0]]?"selected":"").">".trim($$strVName)."</option>";
									}
									else
									{
										echo  "<option ".(isset($_REQUEST["pfield-".str_replace(".","-",$category_fields["cat_id"])."-".$iFCounter])&&trim($strFieldValue)==$_REQUEST["pfield-".str_replace(".","-",$category_fields["cat_id"])."-".$iFCounter]?"selected":"").">".trim($strFieldValue)."</option>";
									}		
								
								}
							}
								
								echo  "</select>";
						}
						else
						{
							echo  "\n<input value=\"".(isset($_REQUEST["pfield-".str_replace(".","-",$category_fields["cat_id"])."-".$iFCounter])!=""?$_REQUEST["pfield-".str_replace(".","-",$category_fields["cat_id"])."-".$iFCounter]:"")."\" type=\"text\" name=\"pfield-".str_replace(".","-",$category_fields["cat_id"])."-".$iFCounter."\"/>";
						}

						echo "\n</li>";
						
						$iFCounter++;
					}
					
				}
				
				?>
			
				
				<?php
				if(isset($_POST["location"]))
				{
				?>
					<input type="hidden" name="location" value="<?php echo $_POST["location"];?>"/>
				<?php
				}
				else
				{
				?>
				<li>
				
					<div class="div_label">
						<?php echo $M_LOCATION;?>
						
					</div>
					
					<div class="div_field">
						<select name="location_menu" id="location_menu">
						<?php
						$locations_content = file_get_contents('locations/locations.php');
						$arrLocations = explode("\n", trim($locations_content));
				
						foreach($arrLocations as $str_location)
						{
							$loc_items = explode(". ",$str_location);
							
							if(sizeof($loc_items)==2)
							{
								list($key,$value)=$loc_items;
								
								$i_level = substr_count($key, ".");
								if($i_level!=0) continue;
								echo "<option ".(isset($_REQUEST["location"])&&$_REQUEST["location"]==trim($key)?"selected":"")." value=\"".trim($key)."\">".trim($value)."</option>";
							}
						}
						?>
						
						</select>
					
					<script>
					var suggest_location_url="include/suggest_location.php";
					</script>
					<script type="text/javascript" src="include/multi_location_select.js"></script>
					<input type="hidden" name="location" id="location" value="">
					
					<script type="text/javascript">
					transform_select("location_menu","200px","200px");
					
					</script>
					
					</div>
					<div class="clear"></div>
					
					
				</li>
				<?php
				}
				?>
				<li id="add-map">
					<div class="div_label">
						<?php echo $M_ADDRESS;?>
					
					</div>
					
					<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
					<script>
					  var geocoder;
					  var map;
					  function initialize() 
					  {
						geocoder = new google.maps.Geocoder();
						var latlng = new google.maps.LatLng(0, 0);
						var mapOptions = {
						  zoom: 1,
						  center: latlng,
						  mapTypeId: google.maps.MapTypeId.ROADMAP
						}
						map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
						
					}

					  function PreviewMap() 
					  {
					  
						document.getElementById("map-canvas").style.display="block";
					  
						var address = "";
						
						
						if(document.getElementById("selected_dropdown").innerText)
						{
							address +=
							document.getElementById("selected_dropdown").innerText.replace(" :",", ")
							+", ";
						}
						
						
						address += document.getElementById('address').value;
						geocoder.geocode( { 'address': address}, function(results, status) {
						  if (status == google.maps.GeocoderStatus.OK) {
							map.setCenter(results[0].geometry.location);
							
							map.setZoom(13);
							
							var marker = new google.maps.Marker({
								map: map,
								position: results[0].geometry.location
							});
							
							document.getElementById("latitude").value=
							results[0].geometry.location.lat();
							
							document.getElementById("longitude").value=
							results[0].geometry.location.lng();
							
							
						  } else {
							alert('Google Maps can\'t find this address: ' + status);
						  }
						});
						google.maps.event.trigger(map, 'resize');
						map.checkResize();
					  }
					  
					  window.onload=initialize;
					</script>
					<script>
					function AddMap()
					{
						if(document.getElementById("add-map").style.display=="none")
						{
							document.getElementById("add-map").style.display="block"
						}
						else
						{
							document.getElementById("add-map").style.display="none"
						}
					}
					
					function UserKeyUp()
					{
						if(document.getElementById("address").value=="")
						{
							document.getElementById("preview-map").style.display="none";
						}
						else
						{
							document.getElementById("preview-map").style.display="block";
						}
					}
					</script>
					
					<div class="div_field">
						<input name="address" value="<?php if(isset($_REQUEST["address"])) echo $_REQUEST["address"];?>" onkeyup="javascript:UserKeyUp()" id="address" type="text" placeholder="<?php echo $M_PLEASE_ENTER_ADDRESS;?>"/>
					</div>
					<br/>
					<div style="display:none">
						<?php echo $M_OR_ENTER;?> 
						&nbsp;
						<?php echo $M_LATITUDE;?>:
						<input type="text" name="latitude" id="latitude"  style="width:60px" value="<?php if(isset($_REQUEST["latitude"])) echo $_REQUEST["latitude"];?>">
						<span style="font-size:10px">e.g. 40.758224</span>
						&nbsp;&nbsp;
						<?php echo $M_LONGITUDE;?>:
						<input type="text" name="longitude" id="longitude" style="width:60px" value="<?php if(isset($_REQUEST["longitude"])) echo $_REQUEST["longitude"];?>"> 
						<span style="font-size:10px">e.g. -73.917404</span>
					</div>	
					<div class="clear"></div>
					
					<span id="preview-map" style="display:none"><a href="javascript:PreviewMap()"><?php echo $M_ADD_MAP;?></a></span>
					<div class="clear"></div>
				
					<div id="map-canvas" style="width: 500px; height: 300px;display:none"></div>
		
				</li>
				
				
				
				<li>
					<label for="email"><?php echo $M_YOUR_EMAIL;?>(*)</label>
					<input id="email" <?php if(isset($_REQUEST["email"])) echo "value=\"".$_REQUEST["email"]."\"";?> name="email" placeholder="example@domain.com" type="email" required/>
					<br/>
					<span class="sub-text"><?php echo $M_EMAIL_PUBLISHED;?></span>
				</li>
				
				<li>
					<label for="name"><?php echo $M_YOUR_NAME;?>(*)</label>
					<input id="name" <?php if(isset($_REQUEST["name"])) echo "value=\"".$_REQUEST["name"]."\"";?> name="name" placeholder="" type="text" required/>
				</li>
				<?php
				if($website->GetParam("USE_CAPTCHA_IMAGES")==1)
				{
				?>
				<li>
					<label for="code">
					<img src="include/sec_image.php" width="100" height="30"/>
					</label>
					<input id="code" name="code" placeholder="<?php echo $M_PLEASE_ENTER_CODE;?>" type="text" required/>
				</li>
				<?php
				}
				?>	
			</ol>
		</fieldset>
	
		
		
		<script type="text/javascript">
		var m_all="<?php echo $M_PLEASE_SELECT;?>";
		</script>					
		
		
		(*) <?php echo $M_REQUIRED_FIELDS;?>
		<fieldset>
			<button type="submit"><?php echo $M_SUBMIT;?></button>
		</fieldset>
	</form>
	<?php
	}
	?>
	<!--[if !IE]>
	<script>
	document.getElementById('title').setCustomValidity('<?php echo $M_PLEASE_ENTER_TITLE;?>');
	document.getElementById('url').setCustomValidity('<?php echo $M_PLEASE_ENTER_URL;?>');
	document.getElementById('category_1').setCustomValidity('<?php echo $M_PLEASE_SELECT_CATEGORY;?>');
	document.getElementById('name').setCustomValidity('<?php echo $M_PLEASE_ENTER_NAME;?>');
	document.getElementById('email').setCustomValidity('<?php echo $M_PLEASE_ENTER_EMAIL;?>');
	document.getElementById('code').setCustomValidity('<?php echo $M_PLEASE_ENTER_CODE;?>');
	</script>
	<![endif]-->
<?php
}
?>
<br/><br/>