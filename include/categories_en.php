1. Buy or Sell
1.1. Animals
1.2. Art & Collectibles
1.3. Books & Magazines
1.4. Business & Industry
1.5. Clothing & Accessories
1.6. Computers
1.7. CDs & Records
1.8. DVD
1.9. Electronics
1.10. Jewelry & Watches
1.11. Games
1.12. Health & Beauty
1.13. Home & Garden
1.14. Musical Instruments
1.15. Phones
1.16. Photography & Audio
1.17. Sporting Goods & Bicycles
1.18. Tickets
1.19. Video Games
1.20. Others
2. Courses & Classes
2.1. Computers & Multimedia
2.2. Driving
2.3. Language Courses
2.4. Music, Theatre & Dance
2.5. Private Lessons
2.6. Other courses
3. Cars
3.1. Boats
3.2. Campers & Caravans
3.3. Car Accessories
3.4. Motorcycles & Scooters
3.5. Trucks & Commercial Vehicles
3.6. Other vehicles
4. Properties
4.1. Exchange
4.2. Houses & Apartments for Sale
4.3. Houses & Apartments for Rent
4.4. Land
4.5. Offices & Commercial
4.6. Parking
4.7. Roommate
4.8. Vacation Rentals
4.9. Other
5. Services
5.1. Babysitter & Nanny
5.2. Casting & Auditions
5.3. Computing
5.4. Moving & Storage
5.5. Events Planning
5.6. Health & Beauty
5.7. Horoscope
5.8. Household & Domestic Help
5.9. Repair
5.10. Translators
5.11. Other Services
6. Jobs
6.1. Accounting & Finance
6.2. Administrative & Secreatary
6.3. Babysitters
6.4. Business & Industry
6.5. Commercial
6.6. Customer Service
6.7. Education & Training
6.8. Entertainment
6.9. Engineering & Architecture
6.10. Human Resources
6.11. Internet & Multimedia
6.12. Lawyers
6.13. Marketing & Advertising
6.14. Medicine & Nursing
6.15. Production - Operations
6.16. Promoters
6.17. Real Estate
6.18. Restoration & Waiters
6.19. Sellers
6.20. Software & Hardware
6.21. Voluntary
6.22. Other jobs
7. Community
7.1. Activities
7.2. Ride share & Carpool
7.4. Musicians & Artists
7.5. Lost and Found
7.6. Volunteers
7.7. Other
8. Events
8.1. Cultural
8.2. Concerts & Parties
8.3. Religious
8.4. Sports
8.5. Other Events
9. Business Opportunities
9.1. Affiliate programs
9.2. Businesses for Sale
9.3. Distributors
9.4. Investment & Partnership
9.5. Work at Home
9.6. Other