<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
class Page
{
	
	var $templateHTML="";
	var $pageHTML="";
	public $page;
	
	var $arrElements=array("title","content","keywords","description");
	var $arrElementsHTML=array();
	var $templateID=0;
	var $arrPage;
	
	function Page($page_name)
	{
		$this->page = $page_name;
	}
	
	function LoadPageData()
	{
		global $lang,$database,$website;
		
		
		if($this->page == "")
		{
			$this->page = $website->DefaultPage;
			
		}
	
		list($lang,$link)=explode("_",urldecode($this->page),2);
		
		if(trim($lang)!="" && strlen($lang)==2)
		{
			$website->SetLanguage($lang);
		}
		
		$this->arrPage=$database->DataArray("pages","link_".$lang."='".$link."'");
		
		if(!isset($this->arrPage["name_".$lang]))
		{
			
		}
			
		$this->arrElementsHTML["title"]=stripslashes($this->arrPage["name_".$lang]);
		$this->arrElementsHTML["keywords"]=stripslashes($this->arrPage["keywords_".$lang]);
		$this->arrElementsHTML["description"]=stripslashes($this->arrPage["description_".$lang]);
		
		$this->arrElementsHTML["content"]=stripslashes($this->arrPage["html_".$lang]);
		$this->templateID = $this->arrPage["template_id"];
		
		
		
	}
	
	function Process()
	{
		
		global $MULTI_LANGUAGE_SITE,$website,$database,$lang,$_POST,$DOMAIN_NAME;
					
		$this->pageHTML=$website->TemplateHTML;
		
		
		$frmArray=$database->DataArray("forms","page='".urldecode($this->page)."' OR page='".urldecode($lang."_".$this->page)."'");

		if(isset($frmArray["code"])&&trim($frmArray["code"]) != "")
		{
			$frmArray["code"]=stripslashes($frmArray["code"]);
			$frmArray["code"] = str_replace("style=\"border: 1px dashed rgb(170, 170, 170);\"","",$frmArray["code"]);
			$frmArray["code"] = str_replace("dashed","none",$frmArray["code"]);
		
			if(isset($_POST["ProceedCustomForm"]))
			{
				if($website->GetParam("USE_CAPTCHA_IMAGES")==1 && ($_POST['captcha_code'] != $_SESSION['code']|| trim($_POST['captcha_code']) == ""))
				{
					$strFormCode= "<br><b>The code of the image you entered is incorrect!</b>";
				}
				else		
				{
				
				$strEmail ="";
				$arrValues=array();

				foreach($_POST as $key=>$value )
				{
					if(substr($key,0,9)=="namefield")
					{
						$strValueField=str_replace ("name","",$key);

						if($_POST[$strValueField] != "")
						{
							$arrValues[$value]=$_POST[$strValueField];
							
							$strEmail .= $value.": ".$_POST[$strValueField]."\n";
						}
						else
						{
							$arrValues[$value]="";
						}

					}

				}

				$form_id=$_POST["form_id"];
				$website->ms_i($form_id);
				
				$database->SQLInsert
				(
					"forms_data",
					array("form_id","data","date","ip"),
					array($form_id,serialize($arrValues),date("F j, Y, g:i a"),$_SERVER['REMOTE_ADDR'])
				);

				if(trim($frmArray["email"]) != "")
				{
					$headers  = "From: \"".$website->GetParam("SYSTEM_EMAIL_FROM")."\"<".$website->GetParam("SYSTEM_EMAIL_ADDRESS").">\n";
				
					mail($frmArray["email"],"New Contact Message",$strEmail, $headers);
				}
				
				
				
					$strFormCode=$frmArray["message"];
				}
			}
			else
			{
				$arrFormItems = explode("~~~~~", $frmArray["code"]);
				$hasValidation = false;
				if(sizeof($arrFormItems) == 2)
				{
					$hasValidation = true;
				}
				
				$strFormCode = "";
				
				if($hasValidation)
				{
					$strFormCode .= "
					
					<script>
					
					function ValidateForm(x)
					{
						".stripslashes($arrFormItems[0])."
						return true;
					}
					
					</script>
					
					";
				}
				
				$strFormCode.="\n<form action=\"index.php\" method=\"post\" ".($hasValidation?"onsubmit='return ValidateForm(this)'":"").">
				
				";
				$strFormCode.="<input name=\"form_id\" type=\"hidden\"  value=\"".$frmArray["id"]."\"/>";
				
				$strFormCode.="<input type=\"hidden\" name=\"page\" value=\"".$this->page."\"/>";
				$strFormCode.="<input type=\"hidden\" name=\"ProceedCustomForm\" value=\"1\"/>";
				
				if($hasValidation)
				{
					$strFormCode.=stripslashes($arrFormItems[1]);
				}
				else
				{
					$strFormCode.=stripslashes($frmArray["code"]);
				}
				
				
				if(true)
				{
				
					$strFormCode.='<br>
					<img src="'.(isset($is_mobile)&&$is_mobile?"../":"").'include/sec_image.php" />
					Code:
					<input type="text" name="captcha_code" value="" size="8"/>	
					';
				}
				
				$strFormCode.="<br/><br/>
				<input type=\"submit\" value=\"".$frmArray["submit"]."\"/>";
				$strFormCode.="</form>";
			}
			
			$this->pageHTML=
				str_replace
				(
					"<wsa content/>",
					"<wsa content/>".$strFormCode,
					$this->pageHTML
				);
		}
		
		
		
		
		
		
		foreach($this->arrElements as $element)
		{
		
			if(isset($this->arrElementsHTML[$element]))
			{
				$this->pageHTML=str_replace
				(
					"<wsa ".$element."/>",
					$this->arrElementsHTML[$element],
					$this->pageHTML
				);
			}
		}		
		
		
		$this->pageHTML=str_replace("../image.php?id=","image.php?id=",$this->pageHTML);
		
		$lang_menu_pos = strpos($this->pageHTML, "<wsa languages_menu/>");
		
		if($MULTI_LANGUAGE_SITE && $lang_menu_pos !== false)
		{
			$this->pageHTML=
			str_replace
			(
				"<wsa languages_menu/>",
				$website->LanguagesMenu($this->arrPage["id"]),
				$this->pageHTML
			);
		}
			
		
		$website->TemplateHTML=$this->pageHTML;
			
		
	}
	
	
	
}
?>
