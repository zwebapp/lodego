<?php
class SiteManager
{
	public $lang="en";
	public $arrPages = array();
	public $domain = "";
	private $db;
	
	
	function SiteManager()
	{
		global $DOMAIN_NAME,$_REQUEST,$is_mobile;
		$this->domain = $DOMAIN_NAME;
		
		if(isset($is_mobile))
		{
		
		}
		else
		if(isset($_REQUEST["lang"]))
		{
			$this->lang= substr(preg_replace("/[^a-z]/i", "", $_REQUEST["lang"]), 0, 2); 
		}
		else
		if(isset($_REQUEST["page"]))
		{
			list($lang,$link)=explode("_",urldecode($_REQUEST["page"]),2);
		
			if(trim($lang)!="" && strlen($lang)==2)
			{
				$this->lang= substr(preg_replace("/[^a-z]/i", "", $lang), 0, 2); 
		
			}
		}
		
	}
	
	/// The website title and meta description and keywords,
	/// which can be used for SEO purposes
	public $Title = true;
	public $Description = true;
	public $Keywords = true;
	
	/// The current language version on the website
	public $Language = true;
	
	/// The html code of the website template
	public $TemplateHTML = "";
	
	/// The site paramets
	public $params = array();
	
	public $IsExtension = false;
	public $ExtensionFile = false;
	
	public $DefaultPage = "";
	public $DefaultPageId = 0;
	
	public $isAdminPanel = false;
		
	public $MenuHTML = "";
	
	function SetLanguage($lang)
	{
		$this->lang= substr(preg_replace("/[^a-z]/i", "", $lang), 0, 2); 
	}
	
	function SetDatabase(Database $db)
	{
		$this->db = $db;
	
	}
	
	function LoadSettings()
	{
		global $DBprefix,$_REQUEST;
		
		if
		(
			isset($_REQUEST["mod"]) &&
			$_REQUEST["mod"] != "" &&
			file_exists("extensions/".$_REQUEST["mod"].".php")
		)
		{
			$this->IsExtension = true;
			$this->ExtensionFile = $_REQUEST["mod"];
		}
		
		$sql_query = "select id,value from ".$DBprefix."settings";
		
		$results = $this->db->Query($sql_query);
	
		while($row = mysql_fetch_array($results))
		{
			$this->params[$row["id"]]=$row["value"];
		}
		
		$this->DefaultPageId = $this->params[1];
	
		date_default_timezone_set("Europe/London");
	
	}
	
	function SetPage()
	{
	
	}
	
	function LoadTemplate($template_id)
	{
	
		if(file_exists($template_id.".htm"))
		{
			$templateArray=array();
			$templateArray["html"] = file_get_contents($template_id.".htm");
		}		
		else
		if($template_id != 0)
		{
			$templateArray = $this->db->DataArray("templates","id=".$template_id);
		}
		else
		{
			if(file_exists("template.htm"))
			{
				$templateArray=array();
				$templateArray["html"] = file_get_contents('template.htm');
			}
			else
			{
				$templateArray= $this->db->DataArray("templates","id=".$this->params[10]);
			}
		}
		
		$this->TemplateHTML = stripslashes($templateArray["html"]);
		$this->TemplateHTML = str_replace(" />","/>",$this->TemplateHTML);
		
		$pattern = "/{(\w+)}/i";
		preg_match_all($pattern, $this->TemplateHTML, $items_found);
		foreach($items_found[1] as $item_found)
		{
			global $$item_found;
			if(isset($$item_found))
			{
				$this->TemplateHTML=str_replace("{".$item_found."}",$$item_found,$this->TemplateHTML);
			}
		}
		
		if(isset($_REQUEST["mod"]))
		{
			$this->TemplateHTML=str_replace('<wsa browse_locations/>','<br/><br/>',$this->TemplateHTML);
		}
	}
	
	function Render()
	{
		if($this->isAdminPanel==false && $this->params[60] == "NO")
		{
			$this->TemplateHTML=str_replace
			(
				"</head>",
				"<style>
				td, div, span{font-family:".$this->params[61].";font-size:".$this->params[62]."px;color:".$this->params[63]."}
				a:link{color:".$this->params[64]."}
				a:visited{color:".$this->params[65]."}
				a:hover{color:".$this->params[66]."}
				h1,h2,h3,h4,h5,h6{color:".$this->params[67]."}
				</style></head>",
				$this->TemplateHTML
			);
		}
		echo $this->TemplateHTML;
	}
	
	function LanguagesMenu($page_id)
	{
		global $lang,$database,$website;
		$strResult="";

			
		$tableLanguages=$database->DataTable("languages","WHERE active=1");
		
		
			$bFirst=true;
		
					
			while($arrLanguages=mysql_fetch_array($tableLanguages))
			{
	
				
					$strResult.="
							
					<a href=\"index.php?lang=".strtolower($arrLanguages["code"]).($page_id!=""?"&page_id=".$page_id:"")."\"><img alt=\"".stripslashes($arrLanguages["name"])."\"  title=\"".stripslashes($arrLanguages["name"])."\" src=\"images/flags/".strtoupper($arrLanguages["code"]).".gif\" width=\"21\" height=\"14\"/></a>
					 		
					";	
					
				
			
			}
		
		return $strResult;
	}
	
	function GetSubArray($parent,$arr)
	{
		$result = array();

		for($i=0;$i<sizeof($arr);$i++)
		{
			if($arr[$i][1] == $parent)
			{
				array_push($result, $arr[$i]);
			}
		}

		return $result;

	}


	function GenerateMenu()
	{
		global $page,$database,$website;
		$strResult="";
		
		$site_pages=$database->DataTable("pages","WHERE id>0 AND active_".$this->lang."=1 order by id");

		while ($row = mysql_fetch_array($site_pages))
		{
			
			array_push($this->arrPages, array($row['id'], $row['parent_id'], $row["link_".$this->lang], $row["custom_link_".$this->lang]));
		}
		
		$strLinkTemplate = stripslashes($this->params[333]);
			
		foreach($this->arrPages as $arrPage)
		{
			if($arrPage[1]!=0) continue;
			
			$arrSubPages = $this->GetSubArray($arrPage[0],$this->arrPages);
		
			if($this->DefaultPageId == $arrPage[0]) 
			{
				$this->DefaultPage = $this->lang."_".stripslashes($arrPage[2]);
			}
			
			$strResult .= "\n";
			
			$strSubResult = "";
			
			if(sizeof($arrSubPages) > 0)
			{
			
				$strSubResult .= "\n <ul  class=\"text-left dropdown-menu\">\n";
				
				foreach($arrSubPages as $arrSubPage)
				{
					$strSubResult .= "  <li><a href=\"".$this->GenerateLink($this->params[1111],$this->params[1112],$this->lang,stripslashes($arrSubPage[2]))."\">".stripslashes($arrSubPage[2])."</a></li>\n";
				}
				
				$strSubResult .= " </ul>\n";
				
				$strResult .= str_replace("[LINK_TEXT]",stripslashes($arrPage[2]),str_replace("[LINK_HREF]",$this->GenerateLink($this->params[1111],$this->params[1112],$this->lang,stripslashes($arrPage[2])),
				str_replace("</li>",$strSubResult."</li>",str_replace("<li>","<li  class=\"dropdown\">",$strLinkTemplate))
				));
			}
			else
			{
				$strResult .= str_replace("[LINK_TEXT]",stripslashes($arrPage[2]),str_replace("[LINK_HREF]",$this->GenerateLink($this->params[1111],$this->params[1112],$this->lang,stripslashes($arrPage[2])),$strLinkTemplate));
			}
			
			
		}
		$this->MenuHTML = $strResult;
	}

	function GenerateLink($urlFormat,$urlLanguage,$lang,$page)
	{
		global $DOMAIN_NAME;
		if($this->GetParam("SEO_URLS")==1)
		{
			if($lang."_".stripslashes($page) == "en_Home")
			{
				return "http://".$DOMAIN_NAME;
			}
			else
			{
				return (urlencode($lang."_".stripslashes($page))).".html";	
			}
		}
		else
		{
			return ("index.php?page=".urlencode($lang."_".stripslashes($page)));	
		}
		
		
		
	}
	

	function ms_w($input)
	{
		if(!preg_match("/^[a-zA-Z0-9_]+$/i", $input)) die("");
	}
	
	function ms_ew($input)
	{
		if(!preg_match("/^[a-zA-Z0-9_\-. @]+$/i", $input)) die("");
	} 
	
	function ms_i($input)
	{
		if(!is_numeric($input)) die("");
	} 
	
	function ms_ia($input)
	{
		foreach($input as $inp) if(!is_numeric($inp)) die("");
	}
	
	function ForceLogin()
	{
		die("<script>document.location.href='login.php';</script>");
	}
	
	function aParameter($param_id)
	{
		return $this->params[$param_id];
	}
	
	function Statistics()
	{
		global $database,$_REQUEST,$_SERVER;
		
		$database->SQLInsert
		(
			"statistics",
			array("date","timestamp","host","referer","page"),
			array(date("F j, Y"),time(),$_SERVER["REMOTE_ADDR"],(isset($_SERVER["HTTP_REFERER"])?$_SERVER["HTTP_REFERER"]:""),(isset($_REQUEST["page"])?$_REQUEST["page"]:"home"))
		);
	}

	
	function ProcessTags()
	{
		global $MULTI_LANGUAGE_SITE,$DBprefix,$DOMAIN_NAME;
		
		if(file_exists("include/texts_".$this->lang.".php"))
		{
			include("include/texts_".$this->lang.".php");
		}
		$this->TemplateHTML = str_replace("<wsa menu/>",$this->MenuHTML,$this->TemplateHTML);
				
		$arrTags = unserialize($this->params[100]);
		
		array_push($arrTags, array("browse_locations","browse_locations_tag.php"));
		
		if(isset($_REQUEST["mod"])||isset($_REQUEST["page"]))
		{
			$this->TemplateHTML = str_replace('<a href="http://www.netartmedia.net','<a rel="nofollow" href="http://www.netartmedia.net',$this->TemplateHTML);
		}
		
		if(is_array($arrTags))
		{
			foreach($arrTags as $arrTag)
			{
				$tag_pos = strpos($this->TemplateHTML,"<wsa ".$arrTag[0]."/>");
			
				if($tag_pos !== false)
				{
					if(trim($arrTag[1]) != "none" && trim($arrTag[0]) != "" && trim($arrTag[1]) != "")
					{
						$HTML="";
						ob_start();
						include("extensions/".$arrTag[1]);
						
						if($HTML=="")
						{
							$HTML = ob_get_contents();
						}
						ob_end_clean();
						$this->TemplateHTML = str_replace("<wsa ".$arrTag[0]."/>",$HTML,$this->TemplateHTML);
					}
				}
			}
		}

		
		$pattern = "/{(\w+)}/i";
		preg_match_all($pattern, $this->TemplateHTML, $items_found);
		
		
		foreach($items_found[1] as $item_found)
		{
			if(strstr($item_found,"DB")) continue;
			if(isset($$item_found))
			{
				$this->TemplateHTML=str_replace("{".$item_found."}",$$item_found,$this->TemplateHTML);
			}
		}
		
		
		
		
		
		
		
		
		
			$tableBannerAreas = $this->db->DataTable("ext_banner_areas","");

			while($arrBannerArea = mysql_fetch_array($tableBannerAreas))
			{
				$areaHTML = "";
				
				
				$tableBanners = 
				$this->db->Query
				(
				"SELECT 
				".$DBprefix."ext_banners.agent,
				".$DBprefix."ext_banners.link,
				".$DBprefix."ext_banners.link_type,
				".$DBprefix."ext_banners.image_id
				FROM
				".$DBprefix."ext_banners
				 WHERE 
				
				 banner_type=".$arrBannerArea["id"]."
				 AND
				 ".$DBprefix."ext_banners.active=1
				  AND
				 ".$DBprefix."ext_banners.expires>".time()."
				 ORDER BY id DESC
				 "
			);
	
			$iCounter = 0;
			
			if(mysql_num_rows($tableBanners))
			{
				
				
				while($arrBanner = mysql_fetch_array($tableBanners))
				{
					if($iCounter>=($arrBannerArea["rows"]*$arrBannerArea["cols"]))
					{
						break;
					}
					
					if($arrBanner["link_type"] == "2")
					{
						$areaHTML .= "<a href=\"".$arrBanner["link"]."\" target=\"_blank\">";
					}
					else
					{
						if($this->GetParam("SEO_URLS")==1)
						{
							$strLink = $arrBanner["agent"]."-ads.html";
						}
						else
						{
							$strLink = "index.php?mod=search&user_ads=1&user=".$arrBanner["agent"];
						}
						
						$areaHTML .= "<a href=\"".$strLink."\">";
					}
					
					$areaHTML .= "<img class=\"front-site-banner\" alt=\"\" width=\"".$arrBannerArea["width"]."\" src=\"".($arrBannerArea["height"]<=125&&$arrBannerArea["width"]<=200?"thumbnails":"uploaded_images")."/".$arrBanner["image_id"].".jpg\"/>";		
					
					$areaHTML .= "</a>";
					
					if(($iCounter%$arrBannerArea["cols"]+1)==0)
					{
					//	$areaHTML .= "<div class=\"clear\"></div>";
					}
					
							
					$iCounter++;
				}
				
				if($iCounter%$arrBannerArea["cols"]!=0)
				{
					for($i=$iCounter;$i<($iCounter%$arrBannerArea["cols"]);$i++)
					{
							if($i%$arrBannerArea["cols"]==0)
							{
							//	$areaHTML .= "<tr>";
							}
							
							//$areaHTML .= "<td>&nbsp;</td>";
							
							if(($i%$arrBannerArea["cols"]+1)==0)
							{
							//	$areaHTML .= "</tr>";
							}
					}
				
				}
				
				//$areaHTML .= "</table>";	
				
				$this->TemplateHTML = str_replace("<wsa banners_".$arrBannerArea["id"]."/>",$areaHTML,$this->TemplateHTML);
			}	
		}

		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	function format_str($strTitle)
	{
		$strSEPage = ""; 
		$strTitle=strtolower(trim($strTitle));
		$arrSigns = array("~", "!","\t", "@","1","2","3","4","5","6","7","8","9","0", "#", "$", "%", "^", "&", "*", "(", ")", "+", "-", ",",".","/", "?", ":","<",">","[","]","{","}","|"); 
		
		$strTitle = str_replace($arrSigns, "", $strTitle); 
		
		$pattern = '/[^\w ]+/';
		$replacement = '';
		$strTitle = preg_replace($pattern, $replacement, $strTitle);

		$arrWords = explode(" ",$strTitle);
		$iWCounter = 1; 
		
		foreach($arrWords as $strWord) 
		{ 
			if($strWord == "") { continue; }  
			
			if($iWCounter == 4) { break; }  
			if($iWCounter != 1) { $strSEPage .= "-"; }
			$strSEPage .= $strWord;  
			
			$iWCounter++; 
		} 
		
		return $strSEPage;
		
	}
	
	function page_link($page_name,$page_id,$lang)
	{
		$SEO_URLS=true;
		$MULTI_LANGUAGE=true;
		
		if($SEO_URLS)
		{
			return format_str($page_name)."-".$page_id.($MULTI_LANGUAGE?"-".strtolower($lang):"").".html";
		}
		else
		{
			return "index.php?page=".$page_id.($MULTI_LANGUAGE?"&lang=".strtolower($lang):"");
		
		}
	}
	
	function text_words($string, $wordsreturned)
	{
		$string=trim($string);
		$string=str_replace("\n","",$string);
		$string=str_replace("\t"," ",$string);
		
		$string=str_replace("\r","",$string);
		$string=str_replace("  "," ",$string);
		 $retval = $string;    
		$array = explode(" ", $string);
	  
		if (count($array)<=$wordsreturned)
		{
			$retval = $string;
		}
		else
		{
			array_splice($array, $wordsreturned);
			$retval = implode(" ", $array)." ...";
		}
		return str_replace("&","&amp;",$retval);
	}
	
	function format_keywords($keywords_text)
	{
		$keywords_text = str_replace("-"," ",$keywords_text);
		$keywords_text = strtolower(str_replace(" ",",",str_replace(", ...","",$this->text_words(stripslashes(strip_tags($keywords_text)),30))));
		$keywords_text = str_replace(",,",",",$keywords_text);
		return $keywords_text;
	}
	
	function Title($website_title)
	{
		$this->TemplateHTML = 
		str_replace
		(
			"<wsa title/>",
			strip_tags(stripslashes($website_title)),
			$this->TemplateHTML
		);
	}
	
	function MetaDescription($meta_description)
	{
		$this->TemplateHTML = 
		str_replace
		(
			"<wsa description/>",
			strip_tags(stripslashes($meta_description)),
			$this->TemplateHTML
		);
	}
	
	function MetaKeywords($meta_keywords)
	{
		$this->TemplateHTML = 
		str_replace
		(
			"<wsa keywords/>",
			strip_tags(stripslashes($meta_keywords)),
			$this->TemplateHTML
		);
	}
	
	function GetParam($param_name)
	{
		switch($param_name)
		{
			case "ADS_EXPIRE":
				return $this->params[98];
				break;
				
			case "SEO_URLS":
				return $this->params[99];
				break;
				
			case "AUTO_APPROVE":
				return $this->params[101];
				break;
				
			case "SYSTEM_EMAIL_ADDRESS":
				return $this->params[102];
				break;
				
			case "SYSTEM_EMAIL_FROM":
				return $this->params[103];
				break;
				
			case "SEND_APPROVE_EMAIL":
				return $this->params[104];
				break;
			
			case "APPROVE_EMAIL_SUBJECT":
				return $this->params[105];
				break;
				
			case "APPROVE_EMAIL_TEXT":
				return $this->params[106];
				break;
			
			case "RESULTS_PER_PAGE":
				return $this->params[107];
				break;
				
			case "NUMBER_OF_CATEGORIES_PER_ROW":
				return $this->params[108];
				break;
				
			case "NUMBER_OF_FEATURED_LISTINGS":
				return $this->params[109];
				break;
				
			case "SUCCESS_MESSAGE_FREE":
				return $this->params[110];
				break;
				
			case "SUCCESS_MESSAGE_PAID":
				return $this->params[111];
				break;
				
			case "WEBSITE_CURRENCY":
				return $this->params[112];
				break;
				
			case "CURRENCY_CODE":
				return $this->params[113];
				break;
				
			case "USE_CAPTCHA_IMAGES":
				return $this->params[114];
				break;
				
			case "TIMEZONE":
				return $this->params[115];
				break;
				
			case "PAYPAL_ID":
				return $this->params[116];
				break;

			case "2CHECKOUT_ID":
				return $this->params[117];
				break;	
				
			case "CHEQUES_ADDRESS":
				return $this->params[118];
				break;
				
			case "BANK_ACCOUNT":
				return $this->params[119];
				break;
				
			case "AMAZON_ID":
				return $this->params[120];
				break;
				
			case "AMAZON_ACCESS_KEY":
				return $this->params[121];
				break;
				
			case "AMAZON_SECRET_KEY":
				return $this->params[122];
				break;
				
			case "PAYFAST_ID":
				return $this->params[123];
				break;
				
			case "INTERKASSA_ID":
				return $this->params[124];
				break;
				
			case "GOOGLE_CHECKOUT_ID":
				return $this->params[125];
				break;
				
			case "GOOGLE_CHECKOUT_KEY":
				return $this->params[126];
				break;
				
			case "MONEYBOOKERS_ID":
				return $this->params[127];
				break;
				
			case "PAYMATE_ID":
				return $this->params[128];
				break;
				
			case "SEO_APPEND_TITLE":
				return $this->params[129];
				break;
				
			case "SEO_APPEND_DESCRIPTION":
				return $this->params[130];
				break;
				
			case "SEO_APPEND_KEYWORDS":
				return $this->params[131];
				break;
				
			case "DATE_FORMAT":
				return $this->params[132];
				break;
				
			case "SHOW_LISTINGS_NUMBER":
				return $this->params[133];
				break;
				
			case "VERIFY_EMAIL":
				return $this->params[134];
				break;
				
			case "VERIFY_EMAIL_SUBJECT":
				return $this->params[135];
				break;
				
			case "VERIFY_EMAIL_MESSAGE":
				return $this->params[136];
				break;
		}
	}
	
	
	
	
	function show_full_location($location) 
	{ 
   
	  $str_result=""; 
	   
	  include("locations/locations_array.php"); 
	   
	  $location=str_replace("~",".",$location); 
	  $arr_digits= explode(".",$location); 
	   
	  while(sizeof($arr_digits) >= 1) 
	  { 
	   
	   if(sizeof($arr_digits)==1) 
	   { 
		if(isset($l[$arr_digits[0]])) $str_result = $l[$arr_digits[0]].$str_result; 
	   } 
	   elseif(sizeof($arr_digits)==2) 
	   { 
		if(isset($l1[$arr_digits[0]][$arr_digits[1]] )) $str_result = ", ".$l1[$arr_digits[0]][$arr_digits[1]].$str_result; 
	   } 
	   elseif(sizeof($arr_digits)==3) 
	   { 
		if(isset($l2[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]]))  $str_result = ", ". $l2[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]].$str_result; 
	   } 
	   elseif(sizeof($arr_digits)==4) 
	   { 
		if(isset($l3[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]][$arr_digits[3]]))  $str_result = ", ". $l3[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]][$arr_digits[3]].$str_result; 
	   } 
		
	   array_pop($arr_digits); 
	   
	  } 
	   
	  $str_result_items = explode(",",$str_result); 
	   
	  $str_result_items = array_reverse($str_result_items); 
	  return implode(", ",$str_result_items); 
	   
	} 



	function show_location($location)
	{
		global $l,$l1,$l2,$l3,$l4;
		
		if(!isset($l))
		{
			if(file_exists("locations/locations_array.php")) include_once("locations/locations_array.php");
			elseif(file_exists("../locations/locations_array.php")) include_once("../locations/locations_array.php");
			else return "";
		}

		$arr_digits= explode(".",$location);
		
		if(sizeof($arr_digits)==1)
		{
			if(isset($l[$arr_digits[0]])) return $l[$arr_digits[0]];else return "";
		}
		elseif(sizeof($arr_digits)==2)
		{
			if(isset($l1[$arr_digits[0]][$arr_digits[1]] )) return $l1[$arr_digits[0]][$arr_digits[1]];else return "";
		}
		elseif(sizeof($arr_digits)==3)
		{
			if(isset($l2[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]])) return $l2[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]];else return "";
		}
		elseif(sizeof($arr_digits)==4)
		{
			if(isset($l3[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]][$arr_digits[3]])) return $l3[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]][$arr_digits[3]];else return "";
		}
		else
		{
			return "";
		}
	}
	
	
	
	function show_pic($pic_id,$display_mode="small",$alt_text="",$img_class="", $path = "")
	{
		if($display_mode=="small"||$display_mode=="small-x")
		{
			$images=explode(",",$pic_id);
			if($pic_id==""||!file_exists($path.'thumbnails/'.$images[0].'.jpg'))
			{
				$result = '<img src="'.$path.'images/no_pic.gif" width="'.($display_mode=="small-x"?"150":"100").'" '.($img_class!=""?'class="'.$img_class.'"':'').' alt="no picture available"/>';
		
			}
			else
			{
				$result = '<img src="'.$path.'thumbnails/'.$images[0].'.jpg" width="'.($display_mode=="small-x"?"150":"100").'" alt="'.$alt_text.'" '.($img_class!=""?'class="'.$img_class.'"':'').'/>';
			}
		}
		else
		{
			if($pic_id=="")
			{
				$result = '<img src="'.$path.'images/no_pic.gif" width="300" height="250" alt="no picture available"/>';
		
			}
			else
			{
				$images=explode(",",$pic_id);
				$result = '<img src="'.$path.'uploaded_images/'.$images[0].'.jpg" width="300" alt="'.$alt_text.'"/>';
		
			}
		}
		
		return $result;
	}
 
	function show_category($category)
	{
		global $l,$l1,$l2,$l3,$l4;
		
		if(!isset($l))
		{
			if(file_exists("categories/categories_array_".$this->lang.".php")) include_once("categories/categories_array_".$this->lang.".php");
			elseif(file_exists("../categories/categories_array_".$this->lang.".php")) include_once("../categories/categories_array_".$this->lang.".php");
			else return "";
		}

		$arr_digits= explode(".",$category);
		
		if(sizeof($arr_digits)==1)
		{
			if(isset($l[$arr_digits[0]])) return $l[$arr_digits[0]];else return "";
		}
		elseif(sizeof($arr_digits)==2)
		{
			if(isset($l1[$arr_digits[0]][$arr_digits[1]] )) return $l1[$arr_digits[0]][$arr_digits[1]];else return "";
		}
		elseif(sizeof($arr_digits)==3)
		{
			if(isset($l2[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]])) return $l2[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]];else return "";
		}
		elseif(sizeof($arr_digits)==4)
		{
			if(isset($l3[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]][$arr_digits[3]])) return $l3[$arr_digits[0]][$arr_digits[1]][$arr_digits[2]][$arr_digits[3]];else return "";
		}
		else
		{
			return "";
		}
	}
	
	function current_url() 
	{
		$pageURL = 'http://';
		
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		
		return $pageURL;
	}
	
	function show_stars($vote,$class="")
	{
		
		$result = "<div ".($class!=""?"class=\"".$class."\"":"").">";
		
		for($x=0;$x<floor($vote);$x++)
		{
			$result .= "<img src=\"images/full-star.gif\" width=\"13\" height=\"12\" alt=\"\" class=\"lfloat\">";
		}
		
		for($c=0;$c<ceil(fmod($vote, 1) );$c++)
		{
			$result .= "<img src=\"images/half-star.gif\" width=\"13\" height=\"12\" alt=\"\" class=\"lfloat\">";
		}
		
		for($v=($c+$x);$v<5;$v++)
		{
			$result .= "<img src=\"images/empty-star.gif\" width=\"13\" height=\"12\" alt=\"\" class=\"lfloat\">";
		}

		$result .= "</div>";
		
		return $result;
	}
	
	function UpdateListingRating($id)
	{
		global $database,$DBprefix;
		$this->ms_i($id);
		$database->Query("
		UPDATE ".$DBprefix."listings
		SET rating=(
		 SELECT AVG(vote)
		 FROM ".$DBprefix."comments
		 WHERE listing_id=".$id."
		)
		WHERE id=".$id);
	}
	
	function sanitize($input)
	{
		$strip_chars = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                 "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                 ",", "<", ".", ">", "/", "?");
		$output = trim(str_replace($strip_chars, " ", strip_tags($input)));
		$output = preg_replace('/\s+/', ' ',$output);
		$output = preg_replace('/\-+/', '-',$output);
		return $output;
	}
}	
?>