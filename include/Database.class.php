<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
class Database
{
	public $allowed_html_fields=array("html","code");

    public function Connect($mysql_server, $mysql_user, $mysql_password)
    {
         $this->connection = mysql_connect($mysql_server, $mysql_user, $mysql_password);
    }

	public function SelectDB($database_name)
    {
        mysql_select_db($database_name, $this->connection);
    }
	
    public function __destruct()
    {
		
        mysql_close($this->connection);
    }

    public function Query($query)
    {
        return mysql_query($query, $this->connection);
    }
	
	public function SQLQuery($query)
    {
        return mysql_query($query, $this->connection);
    }
	
	public function SetParameter($param_id, $param_value)
    {
		global $DBprefix;
	
		mysql_query("UPDATE ".$DBprefix."settings SET value='".mysql_real_escape_string($param_value)."' WHERE id=".$param_id, $this->connection);
    }
	
	public function DataArray($strTable,$sqlClause)
	{
		if(preg_match("/select|union|concat|delete|drop|\/\*/i", $sqlClause)) die("");
		
		global $DBprefix;
		
		$sql_query = "SELECT * FROM ".$DBprefix.$strTable." WHERE ".$sqlClause;

		$data_table = $this->Query($sql_query);
		
		if(!$data_table || mysql_num_rows($data_table) == 0)
		{
			return null;
		}
		else
		{
			return mysql_fetch_array($data_table);
		}

	}
	
	public function DataArray_Query($sql_query)
	{
		
		global $DBprefix;
		
		
		$data_table = $this->Query($sql_query);
		
		if(!$data_table || mysql_num_rows($data_table) == 0)
		{
			return null;
		}
		else
		{
			return mysql_fetch_array($data_table);
		}

	}
	
	public function DataTable($strTable,$sqlClause)
	{
		global $DBprefix;
		
		$sql_query = "SELECT * FROM ".$DBprefix.$strTable." ".$sqlClause;

		
		$data_table = $this->Query($sql_query);
		
		return $data_table;
	}
	
	
	public function SQLCount_Query($strQuery)
	{
		$iResult=0;

		$result_set = mysql_query($strQuery, $this->connection);
		
		$iResult=mysql_num_rows($result_set);
		
		return $iResult;
	}
	
	function GetFieldsInTable($strTable)
	{
		global $DBprefix;
		$mysql_fields = array();
		$pieces = explode(",", $strTable);
		$oResult=mysql_query("SHOW COLUMNS FROM ".$DBprefix.$pieces[0], $this->connection); 
		
		while ($row = mysql_fetch_assoc($oResult)) 
		{
		   array_push($mysql_fields,$row["Field"]);
		}
				
		return $mysql_fields;
	}
	
	function SQLInsert($strTable,$arrNames,$arrValues)
	{
		global $DBprefix;
		$strNames="";
		$strList="";

		$num = count($arrNames);

		for ($i = 0; $i < $num; $i++) 
		{
			$strNames.=$arrNames[$i].",";
		}

		$num = count ($arrValues);

		for ($i = 0; $i < $num; $i++) 
		{
			if(strpos($arrNames[$i], "html_")!== false)
			{
			
			}
			else
			if(!in_array($arrNames[$i], $this->allowed_html_fields))
			{
				$arrValues[$i]=strip_tags($arrValues[$i]);
			}
			$strList.="'".mysql_real_escape_string($arrValues[$i])."',";
		}

		$strList=substr($strList,0,(strlen($strList)-1));
		$strNames=substr($strNames,0,(strlen($strNames)-1));

		$strQuery="INSERT INTO ".$DBprefix.$strTable." 
		(".$strNames.") 
		VALUES 
		(".$strList.")";
		
		
		$this->Query($strQuery);

		$iResult=mysql_insert_id($this->connection);
		
		return $iResult;
	}

	public function SQLCount($table,$where_query="", $count_column = "id")
	{
		global $DBprefix;
		$result = $this->Query
		(
			"SELECT COUNT(".$count_column.")
			FROM ".$DBprefix.$table."
			".($where_query!=""?$where_query:"")
		);
		return mysql_result($result, 0);
	}
 
	
	function SQLDelete($strTable,$Key,$arrIDs)
	{

		global $DBprefix;

		$strList="";


		$num = count ($arrIDs);

		for ($i = 0; $i < $num; $i++) 
		{
			$strList.=$arrIDs[$i].",";
		}

		$strList=substr($strList,0,(strlen($strList)-1));


		$strQuery="DELETE FROM ".$DBprefix.$strTable." WHERE ".$Key." IN ($strList)";

		$this->Query($strQuery);

	}
	
	
	function SQLDeletePlus($strTable,$Key,$arrIDs,$auth_column,$auth_value)
	{

		global $DBprefix;

		$strList="";


		$num = count ($arrIDs);

		for ($i = 0; $i < $num; $i++) 
		{
			$strList.=$arrIDs[$i].",";
		}

		$strList=substr($strList,0,(strlen($strList)-1));


		$strQuery="DELETE FROM ".$DBprefix.$strTable." WHERE ".$Key." IN ($strList) AND ".$auth_column."='".$auth_value."'";

		$this->Query($strQuery);

	}
	
	function SQLUpdate($strTable,$arrNames,$arrValues,$whereClause)
	{
		global $DBprefix;

		$strUpdateList="";

		$num = count($arrNames);

		for ($i = 0; $i < $num; $i++) 
		{
			if(strpos($arrNames[$i], "html_")!== false)
			{
				
			}
			else
			if(!in_array($arrNames[$i], $this->allowed_html_fields))
			{
				$arrValues[$i]=strip_tags($arrValues[$i]);
			}
			
			$strUpdateList.=$arrNames[$i]."='".mysql_real_escape_string($arrValues[$i])."',";
		}

		$strUpdateList=substr($strUpdateList,0,(strlen($strUpdateList)-1));

		$strQuery="UPDATE ".$DBprefix.$strTable."
		SET ".$strUpdateList."
		WHERE ".$whereClause;

		$iResult=$this->Query($strQuery);

		return $iResult;
	}
	
	function SQLUpdate_SingleValue
	(
		$strTable,
		$PrimaryKey,
		$PrimaryKeyValue,
		$FieldName,
		$FieldValue
	){


		global $DBprefix;

		if(!in_array($FieldName, $this->allowed_html_fields))
		{
			$FieldValue=strip_tags($FieldValue);
		}
		
		$strQuery="UPDATE ".$DBprefix.$strTable."
		SET ".$FieldName."='".mysql_real_escape_string($FieldValue)."'
		WHERE ".$PrimaryKey."=".$PrimaryKeyValue;

		$iResult=$this->Query($strQuery);

		return $iResult;
	}

	function SQLUpdateField_MultipleArray($strTable,$strName,$strValue,$strIdName,$arrIds)
	{

		if(sizeof($arrIds)==0)
		{
			return;
		}
		
		$strIds="";

		for($i=0;$i<sizeof($arrIds);$i++)
		{

			if($i==(sizeof($arrIds)-1))
			{
				$strIds.="".$arrIds[$i];
			}
			else{
				$strIds.="".$arrIds[$i].",";
			}

		}

		global $DBprefix;

		$strQuery="UPDATE ".$DBprefix.$strTable."
		SET ".$strName."='".mysql_real_escape_string($strValue)."'
		WHERE ".$strIdName." IN (".$strIds.")";
		
		$this->Query($strQuery);
	}

	function getSingleValue
			(
				$strTable,
				$PrimaryKey,
				$PrimaryKeyValue,
				$FieldName
			)
	{

		$strResult="";
		global $DBprefix;

		
		$strQuery="SELECT $FieldName FROM $DBprefix".$strTable."
						  WHERE $PrimaryKey=$PrimaryKeyValue";

		$oResult=$this->Query($strQuery);

		$aResult=mysql_fetch_array($oResult);
		
		$strResult=$aResult[$FieldName];
		
		return $strResult;
	}
	
}
?>