<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
define("IN_SCRIPT","1");
session_start();
require("config.php");
if(!$DEBUG_MODE) error_reporting(0);
require("include/SiteManager.class.php");
include("include/Database.class.php");

/// Initialization of the site manager and database objects
$website = new SiteManager();
$database = new Database();

/// Connect to the website database
$database->Connect($DBHost, $DBUser,$DBPass );
$database->SelectDB($DBName);
$website->SetDatabase($database);

/// Loading the website default settings
$website->LoadSettings();
$website->GenerateMenu();

if($website->IsExtension)
{
	include("include/Extension.class.php");
	$currentExtension = new Extension($website->ExtensionFile);
}
else
{
	include("include/Page.class.php");
	$currentPage = new Page(isset($_REQUEST["page"])?$_REQUEST["page"]:"");

	$currentPage->LoadPageData();
	
	if(substr(trim($currentPage->arrElementsHTML["content"]),0,13) == "wsa:extension")
	{
		$mod_info = explode(":",trim($currentPage->arrElementsHTML["content"]));
		$website->IsExtension=true;
		include("include/Extension.class.php");
		$currentExtension = new Extension($mod_info[2]);
		$currentExtension->SetMasterPage($currentPage);
	}
		
}

$website->LoadTemplate($website->IsExtension?0:$currentPage->templateID);
$website->ProcessTags();

if($website->IsExtension)
{
	$currentExtension->Process();
}
else
{
	$currentPage->Process();
}

/// Rendering the final html of the website
$website->Render();

/// Inserrting the statistics information in the database
$website->Statistics();


?>