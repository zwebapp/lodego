<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
$ProductName="Deals Portal v2.0";

$oLinkTexts=array($M_HOME,$M_LINKS_DIRECTORY,$M_SETTINGS,$H_WEBSITE,$M_EXTENSIONS,$M_TEMPLATES,$M_STATISTICS,$M_ADMIN_USERS);
$oLinkActions=array("home","links_directory","settings","site_management","extensions","templates","statistics","security");

$home_oLinkTexts=array($M_WELCOME,$M_CONNECTIONS,$M_CHANGE_PWD);
$home_oLinkActions=array("welcome","connections","password");

$links_directory_oLinkTexts=array($M_DASHBOARD,$M_APPROVE,$M_CURRENT_LISTINGS,$M_USERS,$M_PACKAGES,$M_BANNERS);
$links_directory_oLinkActions=array("home","approve","links","users","packages","banners");

$settings_oLinkTexts=array($M_CONFIGURATION_OPTIONS,$M_CATEGORIES,$M_LOCATIONS,$M_FIELDS,$M_LANGUAGE_VERSIONS);
$settings_oLinkActions=array("settings","categories","locations","fields","languages");


$site_management_oLinkTexts=array($H_MANAGEMENT,$H_NAVIGATION_MENU,$M_MANAGE_THE_FORMS,$M_POSTED_DATA,$M_EXPORT);
$site_management_oLinkActions=array("pages_pro","menu","manage","posted_data","export");


$statistics_oLinkTexts=array($H_REPORTS,$H_REFERALS);
$statistics_oLinkActions=array("reports","referals");

$extensions_oLinkTexts=array($M_FILES,$M_TAGS,
$M_NEWS." [".$M_ADD_ON."]",
$M_FAQ_MANAGER." [".$M_ADD_ON."]",
$M_NEWSLETTER." [".$M_ADD_ON."]"
);
$extensions_oLinkActions=array("extensions","tags",
"news","faq_manager",
"newsletter"
);

$templates_oLinkTexts=array($MODIFY,$M_SELECT_TEMPLATE,$M_ADD_TEMPLATE);
$templates_oLinkActions=array("modify","select","add");

$products_manager_oLinkTexts=array($DESCRIPTION,$M_CATEGORIES,$M_PRODUCTS,$M_SETTINGS,$SEARCH,$M_MENU_SETTINGS);
$products_manager_oLinkActions=array("description","categories","products","settings","search","menu_settings");

$news_oLinkTexts=array($DESCRIPTION,$M_NEWS);
$news_oLinkActions=array("description","news");

$faq_manager_oLinkTexts=array($DESCRIPTION,$M_QUESTIONS);
$faq_manager_oLinkActions=array("description","faq");

$newsletter_oLinkTexts=array($DESCRIPTION,$M_CATEGORIES,$M_NEWSLETTER,$M_LIST,$ENVOYER,$M_LOG,$M_SETTINGS);
$newsletter_oLinkActions=array("description","categories","newsletter2","newsletter","send","log","settings");


$security_oLinkTexts=array($M_USER_GROUPS,$M_NEW_USER,$M_USERS_LIST,$M_ACCESS_RIGHTS);
$security_oLinkActions=array("types","new_user","admin","permissions");


$exit_oLinkTexts=array($M_THANK_YOU);
$exit_oLinkActions=array("exit");
?>