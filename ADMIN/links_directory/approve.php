<?php
if(!isset($iKEY)||$iKEY!="AZ8007") die("ACCESS DENIED");

if(isset($_REQUEST["package"]))
{
	$website->ms_i($_REQUEST["package"]);
}
?>
<script>
function DeleteTemplate(x)
{
	document.location.href="index.php?category=links_directory&action=approve&del="+x;

}


function CallBack()
{
	document.getElementById("main-content").innerHTML =
	top.frames['ajax-ifr'].document.body.innerHTML;
	HideLoadingIcon();
}


$(function(){
	var offsetX = 20;
	var offsetY = -200;
	$('a.hover').hover(function(e){	
		var href = $(this).attr('href');
		$('<img id="largeImage" src="' + href + '" alt="image" />')
			.css({'top':e.pageY + offsetY,'left':e.pageX + offsetX})
			.appendTo('body');
	}, function(){
		$('#largeImage').remove();
	});
	$('a.hover').mousemove(function(e){
		$('#largeImage').css({'top':e.pageY + offsetY,'left':e.pageX + offsetX});
	});
	$('a.hover').click(function(e){
		e.preventDefault();
	});
});

</script>

<?php
if(isset($_REQUEST["del"]))
{
	$website->ms_i($_REQUEST["del"]);
	$database->SQLDelete("listings","id",array($_REQUEST["del"]));
}

if(isset($_REQUEST["Delete"])&&isset($_REQUEST["CheckList"]))
{
	$database->SQLUpdateField_MultipleArray("listings","status","1","id",$_REQUEST["CheckList"]);
	
	foreach($_REQUEST["CheckList"] as $listing_id)
	{
		$website->ms_i($listing_id);
		
		$current_listing = $database->DataArray("listings","id=".$listing_id);
		
		$SEND_APPROVE_EMAIL=$website->GetParam("SEND_APPROVE_EMAIL");
		$SYSTEM_EMAIL_FROM=$website->GetParam("SYSTEM_EMAIL_FROM");
		$SYSTEM_EMAIL_ADDRESS=$website->GetParam("SYSTEM_EMAIL_ADDRESS");
		$APPROVE_EMAIL_SUBJECT=stripslashes($website->GetParam("APPROVE_EMAIL_SUBJECT"));
		$APPROVE_EMAIL_TEXT=stripslashes($website->GetParam("APPROVE_EMAIL_TEXT"));
		
		if($SEND_APPROVE_EMAIL)
		{
			$headers  = "From: \"".$SYSTEM_EMAIL_FROM."\"<".$SYSTEM_EMAIL_ADDRESS.">\n";
			
			$email_subject = str_replace("{NAME}",stripslashes($current_listing["name"]),$APPROVE_EMAIL_SUBJECT);
			
			if($website->GetParam("SEO_URLS")==1)
			{
				$strLink = "http://".$DOMAIN_NAME."/ad-".$website->format_str(strip_tags(stripslashes($current_listing["title"])))."-".$current_listing["id"].".html";
			}
			else
			{
				$strLink = "index.php?mod=details&id=".$current_listing["id"];
			}
			
			$email_text = str_replace("{LINK}",$strLink,$APPROVE_EMAIL_TEXT);
			$email_text = str_replace("{MODIFY_LINK}","http://".$DOMAIN_NAME."/index.php?mod=modify&ad=".$current_listing["code"],$email_text);
		
			
		
			mail
			(
				$current_listing["email"],
				$email_subject,
				$email_text, 
				$headers
			);
		}
					
	}
}
?>


<div class="fright">

		
	<?php
	
	
		 echo LinkTile
 (
	"links_directory",
	"home",
	$M_DASHBOARD,
	$M_GO_BACK_DASH,
	"blue"
 );
		 
		?>
		
	
</div>
<div class="clear"></div>
<br/>
<span class="medium-font">
<?php echo $M_LIST_CURRENT_ADS;?>
</span>
<br/>
<?php
$packages = array();
$arr_packages = $database->DataTable("packages","");

while($arr_package = mysql_fetch_array($arr_packages))
{
	$packages[$arr_package["id"]]=stripslashes($arr_package["name"]);
}

$_REQUEST["packages"]=$packages;

if(file_exists('../categories/categories_'.strtolower($lang).'.php'))
{
	$categories_content = file_get_contents('../categories/categories_'.strtolower($lang).'.php');
}
else
{
	$categories_content = file_get_contents('../categories/categories_en.php');
}

$arrCategories = explode("\n", trim($categories_content));

$categories=array();

foreach($arrCategories as $str_category)
{
	list($key,$value)=explode(". ",$str_category);
	$categories[trim($key)]=trim($value);
}
	
$_REQUEST["categories"]=$categories;

if(isset($_REQUEST["package"]))
{
	$hidden_fields = array();
	$hidden_fields["package"] = $_REQUEST["package"];
	$_REQUEST["hidden_fields"]=$hidden_fields;
}

RenderTable
(
	"listings",
	array("EditNote","show_images","site_info","user_info","package_info","DeleteTemplate"),
	array($MODIFY,$M_IMAGES,$DESCRIPTION,$UTILISATEUR,"",$EFFACER),
	750,
	"WHERE status=0 ".(isset($_REQUEST["package"])?"AND package=".$_REQUEST["package"]:""),
	$M_APPROVE,
	"id",
	"index.php",
	true,
	20,
	false,
	-1,
	"ORDER BY ID desc"
);
?>

