<?php
if(!isset($iKEY)||$iKEY!="AZ8007") die("ACCESS DENIED");
?>


<div class="fright">

	<?php
	echo LinkTile
		 (
			"links_directory",
			"approve",
			$M_GO_BACK,
			"",
			
			"red"
		 );
		 
	echo LinkTile
	 (
		"links_directory",
		"home",
		$M_DASHBOARD,
		$M_GO_BACK_DASH,
		"blue"
	 );
		?>
</div>

<script>

$(function(){
	var offsetX = 20;
	var offsetY = -200;
	$('a.hover').hover(function(e){	
		var href = $(this).attr('href');
		$('<img id="largeImage" src="' + href + '" alt="image" />')
			.css({'top':e.pageY + offsetY,'left':e.pageX + offsetX})
			.appendTo('body');
	}, function(){
		$('#largeImage').remove();
	});
	$('a.hover').mousemove(function(e){
		$('#largeImage').css({'top':e.pageY + offsetY,'left':e.pageX + offsetX});
	});
	$('a.hover').click(function(e){
		e.preventDefault();
	});
});


</script>
<div class="clear"></div>

<?php
$id=$_REQUEST["id"];
$website->ms_i($id);
$_REQUEST["select-width"]="200";


AddEditForm
(
	array
	(
	$M_CATEGORY.":",
	$M_IMAGES.":",
	$M_TITLE.":",
	$DESCRIPTION.":",
	$NOM.":",
	$EMAIL.":",
	$M_APPROVED.":",
	$M_FEATURED.":",
	$CODE.":"
	),
	array
	(
	"link_category",
	"images",
	"title",
	"description",
	"name",
	"email",
	"status",
	"featured",
	"code"
	),
	array("images","code"),
	array(
	"combobox_link_categories",
	"images",
	"textbox_50","textarea_50_5",
	"textbox_50","textbox_50",
	"combobox_".$M_YES."^1_".$M_NO."^0",
	"combobox_".$M_NO."^0_".$M_YES."^1",
	"textbox_50"),
	"listings",
	"id",
	$id,
	$M_NEW_VALUES_SAVED
);
	
?>
<br>

