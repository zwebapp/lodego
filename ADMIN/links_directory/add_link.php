<?php
if(!isset($iKEY)||$iKEY!="AZ8007"){
	die("ACCESS DENIED");
}

?>

<div class="fright">
<?php
echo LinkTile
	 (
		"links_directory",
		"links",
		$M_GO_BACK,
		"",
		"red"
	 );

?>
	
</div>

<script>

function ValidateForm(x)
{
	if(x.title.value=="")
	{
		x.title.style.background="#edeff3";
		
		document.getElementById("page-header").innerHTML=
		"The title can not be empty!";
		return false;
	}
	
	if(x.url.value=="")
	{
		x.url.style.background="#edeff3";
		
		document.getElementById("page-header").innerHTML=
		"The url can not be empty!";
		return false;
	}
	
	return true;
	
}
function CallBack()
{
	lock_check = false;
	document.getElementById("page-header").innerHTML
	= "The new link was added succesfully!";

	loadPage("#links_directory-links");
}
</script>
<div class="clear"></div>


<span class="medium-font" id="page-header">
<?php echo $M_ADD_NEW_LINK;?>
</span>


<br>
<div class="clear"></div>
<br>
<?php

if(isset($_REQUEST["url"]))
{
	$_REQUEST["url"]=str_ireplace("http://","",$_REQUEST["url"]);
}

$added_link = AddNewForm
(
	array($M_PACKAGE.":",
	$M_TITLE.":","URL".":",$DESCRIPTION.":",
	$M_CATEGORY,$NOM.":",$EMAIL.":"),
	array("package",
	"title","url","description",
	"link_category","name","email"),
	array("combobox_table~packages~id~name",
	"textbox_50","textbox_50","textarea_50_5",
	"combobox_link_categories","textbox_50","textbox_50"),
	$M_ADD,
	"listings",
	"The link was added successfully!",
	true,
	array(),
	""
);


if($added_link != null)
{
	
	$selected_package=$database->DataArray("packages","id=".$_REQUEST["package"]);
	
	$database->SQLUpdate
	(
		"listings",
		array
		(
			"status",
			"date",
			"featured",
			"dofollow",
			"ip"
		),
		array
		(
			"1",
			time(),
			$selected_package["featured"],
			$selected_package["follow"],
			$_SERVER["REMOTE_ADDR"]
		),
		"id=".$added_link
	);
	
}

?>	

