<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<div class="fright">

	<?php
			echo LinkTile
				 (
					"faq_manager",
					"faq",
					$M_QUESTIONS,
					"",
					"green",
					"small",
					true
				 );
		?>
</div>
<div class="clear"></div>

<table summary="" border="0" width=750>
	<tr>
		<td class=basictext>
		
		<br>
		<b>FAQ Manager Brief Overview</b>
		<br><br>
		
		The FAQ Manager module provides functionality to display
		frequently asked questions with their answers on a selected
		page of the website.
		<br><br>
		In order to use it you need to set <font face=Courier>faq.php</font>
		(FAQ) as extension of a selected page.
		<br><br>
		
		</td>
	</tr>
</table>