<?php
if(isset($_REQUEST["bn"]))
{
	if($_REQUEST["bn"]=="s")
	{
		$o = str_replace("b-","",str_replace("box-","",$_REQUEST["o"]));
		$n = str_replace("b-","",str_replace("box-","",$_REQUEST["n"]));
		$temp_value = $AdminUser["box_".$o];
		$database->SQLUpdate("admin_users",array("box_".$o),array($AdminUser["box_".$n]),"username='".$AdminUser["username"]."'");
		$database->SQLUpdate("admin_users",array("box_".$n),array($temp_value),"username='".$AdminUser["username"]."'");
		$AdminUser=$database->DataArray("admin_users","username='".$AdminUser["username"]."'");
	}
	else
	if($_REQUEST["bn"]=="a")
	{
		$o = str_replace("b-","",str_replace("box-","",$_REQUEST["o"]));
		$n = strip_tags(stripslashes($_REQUEST["n"]));
		$current_value = $AdminUser["box_".$o];
		$p_items = explode("#",$current_value);
		$passed_value =  explode("-",$n);	
		
		if(trim($passed_value[0])!=""&&trim($passed_value[1])!="")
		{
			$new_value = $passed_value[0]."#".$passed_value[1]."#".$p_items[2];
			$database->SQLUpdate("admin_users",array("box_".$o),array($new_value),"username='".$AdminUser["username"]."'");
			$AdminUser=$database->DataArray("admin_users","username='".$AdminUser["username"]."'");
		}
	}
	
}
?>
<script type="text/javascript">
 
$(init);

String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g, '');};

function CallBack()
{
	document.getElementById("main-content").innerHTML =
	top.frames['ajax-ifr'].document.body.innerHTML.trim();
	
	$(init);
}
</script>
<div class="row">
	<div class="col-md-4 hidden-sm hidden-xs welcome-left-block"">
	<!--welcome text-->
		<br/>
		<span class="large-font">
			<?php echo $M_WELCOME;?>, <b><?php echo $AuthUserName;?></b>!
		</span>
		<br/><br/>	
		There are <a style="text-decoration:underline;color:white" href="index.php?category=links_directory&action=approve"><?php echo $database->SQLCount("listings","WHERE status=0");?></a> new listings waiting to be approved.
		<br/><br/>
		
		<?php
		if($this->CheckPermissions("statistics","reports"))
		{
			$last_login = $database->DataArray_Query("SELECT max(date) max_date FROM ".$DBprefix."login_log WHERE username='".$AuthUserName."' AND action='login'");
			$last_date=date("F j, Y",$last_login["max_date"]);
			$login_stat=str_replace("{show_date}","<a class=\"white-link\" href=\"".CreateLink("home","connections")."\">".$last_date."</a>",$M_LOGIN_STAT_MESSAGE);
			
			$strServerName=$_SERVER["SERVER_NAME"];

			$count_1 = $database->DataArray_Query("SELECT count(id) u_visits FROM ".$DBprefix."statistics WHERE referer NOT LIKE '%".$strServerName."%' AND timestamp>".$last_login["max_date"]);
			$count_3 = $database->DataArray_Query("SELECT count(id) r_visits FROM ".$DBprefix."statistics WHERE referer<>'' AND referer NOT LIKE '%".$strServerName."%' AND timestamp>".$last_login["max_date"]);
			
			$login_stat=str_replace("{unique_visits}","<a class=\"white-link\" href=\"".CreateLink("statistics","reports")."\">".$count_1["u_visits"]."</a>",$login_stat);
			$login_stat=str_replace("{referral_visits}","<a class=\"white-link\" href=\"".CreateLink("statistics","referals")."\">".$count_3["r_visits"]."</a>",$login_stat);
			
			
			echo $login_stat;
		?>
		<br/><br/>
		<?php
		}
		?>
		
	
	<!--end welcome text-->
	</div>
	<div id="home-links-area" class="col-md-8">
		
		<div class="row" style="padding:10px">
		<?php
		
		
		
		
		$arr_cache_texts=array();
		$arr_box_sizes = array("","3","3","3","3","3","3","3","3");
			
		
		if($AdminUser["box_1"]==""&&$AdminUser["box_2"]==""&&$AdminUser["box_3"]==""&&$AdminUser["box_4"]==""&&$AdminUser["box_5"]==""&&$AdminUser["box_6"]==""&&$AdminUser["box_7"]==""&&$AdminUser["box_8"]=="")
		{
			$arr_box_names = array();
			$arr_set_perms = array();
			for($i=1;$i<=8;$i++)
			{
				if(isset($currentUser->arrPermissions[$i]))
				{
				
					$p_items = explode("@",$currentUser->arrPermissions[$i]);
					
					if(sizeof($p_items) != 4) continue;
					if($p_items[3]=="welcome") continue;
					array_push($arr_box_names,"box_".$i);
					array_push($arr_set_perms,$p_items[2]."#".$p_items[3]."#".$i);
				}
			}
			
			
			$database->SQLUpdate("admin_users",$arr_box_names,$arr_set_perms,"id=".$AdminUser["id"]);
			$AdminUser=$database->DataArray("admin_users","id=".$AdminUser["id"]);
		}
		
		for($i=1;$i<=6;$i++)
		{
			$p_items = explode("#",$AdminUser["box_".$i]);
			if(sizeof($p_items)==1) continue;
			if($this->CheckPermissions($p_items[0],$p_items[1]))
			{
				
				$str_arr_texts = $p_items[0]."_oLinkTexts";
				$str_arr_actions = $p_items[0]."_oLinkActions";
				
				if(!isset($$str_arr_texts)||!isset($$str_arr_actions)) continue;
				
				$arr_texts = $$str_arr_texts;
				$arr_actions = $$str_arr_actions;
				$key = array_search($p_items[1], $arr_actions); 
				
				?>
				<div class="col-md-4 col-sm-6 col-xs-12 t-padding" id="box-<?php echo $i;?>">
					<div class="tile-p" id="b-<?php echo $i;?>">
					<?php
					$show_text = $arr_texts[$key];
					$b_pos = strpos($show_text, ' ');
					if($b_pos===false)
					{
						$t_key = array_search($p_items[0], $oLinkActions); 
						if(isset($oLinkTexts[$t_key]))
						{
							$show_text = $oLinkTexts[$t_key]." / ".$show_text;
						}
					}
					
					if(false&&$show_text==$M_REAL_ESTATE." / ".$M_LISTINGS)
					{
						$show_text=$database->SQLCount("listings","")." ".$M_LISTINGS." / ".$M_MANAGE;
					}
					
					echo LinkTile
					 (
						$p_items[0],
						$p_items[1],
						$show_text,
						"",
						"box-".(isset($p_items[2])?$p_items[2]:$i),
						"home"
					 );
					?>
					</div>
				</div>
				
				<?php
				
			}
		}
		
		
		
		?>
			
		</div>
	
	</div>
</div>
<br/>
<br/>
<?php
if($this->CheckPermissions("ads","list"))
{
?>
<div class="row">
	<span class="medium-font"><?php echo $M_LATEST_ADS;?></span>
	<br/>	
	<hr/>	

	<!--recent listings-->
	
		<?php
		
		$MULTI_LANGUAGE_SITE = false;
			
		$csTable=$database->DataTable("listings","ORDER BY id DESC LIMIT 0,6 ");

		if(mysql_num_rows($csTable) == 0)
		{
			echo "<i class=\"sub-text\">".$ANY_ADS."</i>";
		}
							
		$i_listings_counter = 0;


		while($listing = mysql_fetch_array($csTable))
		{
			$i_listings_counter++;
							
			$adFeatured = false;
			
			if($listing["featured"]==1)
			{
				$adFeatured = true;
			}
			
			$strLink = "index.php?category=links_directory&folder=links&page=edit&id=".$listing["id"];
	
			?>
		
			<div class="col-md-2 col-sm-3 col-xs-6">
				<a href="<?php echo $strLink;?>">
					
				<div style="height:120px;overflow:hidden">
				<?php
				echo $website->show_pic($listing["images"],"small-x","","","../");
				?>
				</div>
				
				<div class="clearfix"></div>
			
					<span class="<?php if($adFeatured) echo "featured_";?>listing_title"><?php echo stripslashes(strip_tags($listing["title"]));?></span></a>
					
					<br/>
				<?php
					if(trim($listing["description"]) != "")
					{
					?>
						<div class="listing_description">
						<?php echo $website->text_words(stripslashes(strip_tags($listing["description"])),10);?>
						</div>
					<?php
					}
					?>	
				
			
			</div>
			
			<?php
			
		}

		?>
		<div class="clearfix"></div>
		
		<br/>
		<a style="text-decoration:underline" href="index.php?category=links_directory&action=home">Approve and Manage the Ads</a>
	

</div>
<?php
}


?>