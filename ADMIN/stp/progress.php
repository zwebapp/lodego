</div>
<div class="clear"></div>
<br>

<?php
$x_step = $Step+1;

if(isset($_REQUEST["ProceedMySQLServer"]))
{
	$x_step++;
}

if(isset($fileCreatedSuccess)&&$fileCreatedSuccess) $x_step++;
?>

<div class="progress">
	<div class="progress-bar" role="progressbar" style="width: <?php echo $x_step*25;?>%;"><span class="sr-only"></span>
		step <?php echo $x_step;?> of 4
	</div>
</div>