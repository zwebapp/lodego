<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
if(!isset($iKEY)||$iKEY!="AZ8007") die("ACCESS DENIED");
?>
<script>
function DeleteTemplate(x)
{
	if(confirm("Are you sure that you want to delete this template?\n\nBe aware that once deleted the template can not be restored!"))
	{
		document.location.href="index.php?category=templates&action=modify&Delete="+x;
	}
}


function CallBack()
{
	document.getElementById("main-content").innerHTML =
	top.frames['ajax-ifr'].document.body.innerHTML;
	HideLoadingIcon();
}

</script>
<?php
if(isset($_REQUEST["Delete"]))
{
	$database->SQLDelete("templates","id",array($_REQUEST["Delete"]));
}
?>


<div class="fright">

		
	<?php
	
	echo LinkTile
		 (
			"templates",
			"add",
			$M_ADD,
			$M_ADD_TEMPLATE,
			
			"lila"
		 );
		 
		 
		echo LinkTile
			 (
				"templates",
				"select",
				$M_SELECT ,
				$M_SELECT_TEMPLATE,
				
				"green"
			 );
		?>
		
	
</div>
<div class="clear"></div>
<br/>
<span class="medium-font">
<?php echo $MODIFY_TEMPLATE;?>
</span>
<br/>
<?php
	
$arrHighlightIds=array($website->aParameter(10));
$strHighlightIdName="id";

RenderTable
(
	"templates",
	array("ModifyTemplate","DeleteTemplate","name","description"),
	array($MODIFY,$EFFACER,$NOM,$DESCRIPTION),
	750,
	"",
	"",
	"id",
	"index.php",
	true,
	20,
	false,
	-1,
	"ORDER BY ID desc"
);
?>

