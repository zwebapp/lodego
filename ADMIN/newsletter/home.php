<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<span class="medium-font">Newsletter Module</span>
<br><br>

<div id="information-panel">

<br>
The Newsletter module lets you collect user data and emails and then send personalized
newsletter messages to them. Various options are available when sending the messages,
they may be seen from the next pages.
<br><br>
The module can be used with a custom tag when just the email addresses of
the users subscribing to the newsletter will be collected (in this case 
you need to add a new custom tag and set newsletter.php be executed
for it) or by setting up newsletter2.php as extension of a selected page
(in this case more data like the user first and last names, its company and others
will be also collected and stored in the database)
<br><br>

</div>

<div id="control-panel">
	
	<?php
	
	echo LinkTile
	(
		"newsletter",
		"categories",
		$M_CATEGORIES,
		"",
		"lila"
	 );
	 
	 
	echo LinkTile
	(
		"newsletter",
		"newsletter2",
		$M_NEWSLETTER,
		"",
		"green"
	 );
	 
	 echo LinkTile
	(
		"newsletter",
		"newsletter",
		$M_LIST,
		"",
		"red"
	 );
	 
	 echo LinkTile
	(
		"newsletter",
		"send",
		$ENVOYER,
		"",
		"blue"
	 );
	 
	 echo LinkTile
	(
		"newsletter",
		"settings",
		$M_SETTINGS,
		"",
		"yellow"
	 );
	?>	 
</div>
