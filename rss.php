<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
require("config.php");
if(!$DEBUG_MODE) error_reporting(0);
require("include/SiteManager.class.php");
include("include/Database.class.php");

$website = new SiteManager();
$database = new Database();

/// Connect to the website database
$database->Connect($DBHost, $DBUser,$DBPass );
$database->SelectDB($DBName);
$website->SetDatabase($database);

//".($website->GetParam("ADS_EXPIRE")!=-1?" date>".(time()-$website->GetParam("ADS_EXPIRE")*86400)." AND ":"")."
	
$page_size = 10;
$page_number = 1;

if(isset($_REQUEST["page_size"]))
{
	$page_size = $_REQUEST["page_size"];
	$website->ms_i($page_size);
}	

if(isset($_REQUEST["page_number"]))
{
	$page_number = $_REQUEST["page_number"];
	$website->ms_i($page_number);
}	
	
	
if(isset($_REQUEST["type"])&&$_REQUEST["type"]=="featured")	
{
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".$DBprefix."listings.status = 1
		AND
		".$DBprefix."listings.featured = 1
		ORDER BY 
		".$DBprefix."listings.date DESC
		LIMIT ".($page_number-1).",".$page_size."
	");
}
else
if(isset($_REQUEST["type"])&&$_REQUEST["type"]=="search")	
{
	$search_by = trim($_REQUEST["param"]);
	
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".$DBprefix."listings.status = 1
		AND
		(
			title like '%".mysql_real_escape_string($search_by)."%' 
			OR 
			description LIKE '%".mysql_real_escape_string($search_by)."%'
		)
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date DESC
		LIMIT ".($page_number-1).",".$page_size."
	");
}
else
if(isset($_REQUEST["type"])&&$_REQUEST["type"]=="category")	
{
	$category=trim($_REQUEST["param"]);
	
	$website->ms_i(str_replace("-","",$category));
	
	$category=str_replace("-",".",$category);
		
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".$DBprefix."listings.status = 1
		AND
		(
		link_category='".$category."' 
		OR 
		link_category LIKE '".$category.".%'
		)
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date
		LIMIT ".($page_number-1).",".$page_size."
	");
}
else
if(isset($_REQUEST["type"])&&$_REQUEST["type"]=="listing_day")	
{
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".$DBprefix."listings.status = 1
		AND
		".$DBprefix."listings.featured = 1
		ORDER BY RAND()
		LIMIT 0,1
	");
}
else
{
	$SearchTable = $database->Query
	("
		SELECT * FROM ".$DBprefix."listings
		WHERE 
		".$DBprefix."listings.status = 1
		
		ORDER BY 
		".$DBprefix."listings.featured DESC,
		".$DBprefix."listings.date DESC
		LIMIT ".($page_number-1).",".$page_size."
	");
}


echo "<?xml version=\"1.0\" ?>";
echo "<rss version=\"2.0\">";
echo "<channel>\n";
			
echo "<title>".$DOMAIN_NAME."</title>\n";
echo "<link>http://www.".$DOMAIN_NAME."</link>\n";
echo "<description> </description>\n";

while($listing = mysql_fetch_array($SearchTable))
{
	$strLink = "http://".$DOMAIN_NAME."/ad-".$website->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
		
	echo "<item>\n";
	echo "<title>".stripslashes(strip_tags($listing["title"]))."</title>\n";
	echo "<description>".$website->text_words(stripslashes(strip_tags($listing["description"])),30)."</description>\n";
	echo "<link>".$strLink."</link>\n";
	echo "<guid>".$strLink."</guid>\n";
	
	echo "</item>\n";
}

echo "</channel>\n";	
echo "</rss>\n";

?>

