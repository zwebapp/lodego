<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
if(!isset($iKEY)||$iKEY!="AZ8007"){
	die("ACCESS DENIED");
}
?>



<?php
include("include/languages_menu_processing.php");
?>


<?php
$HideFormFlag=false;
$oArr=$database->DataArray("pages"," id=$id AND user='".$AuthUserName."' ");

if(!isset($oArr["id"]))
{
	die("");
}

if(!isset($LANG)||$LANG=="")
{
	$LANG="EN";
}
$lang = strtolower($LANG);

if(isset($Proceed)){

if($is_default=="yes")
{
	SetParameter(
		"1",
		$id
	);
}

if(isset($extension) && $extension =="NONE")
{
	if(substr($oArr["html_".$lang] ,0,14) == "wsa:extension:")
	{
		$database->SQLUpdate_SingleValue(
				"pages",
				"id",
				$id,
				"html_".strtolower($lang),
				""
			);
		}
}
else
if(isset($extension) && $extension !="NONE")
{
	$database->SQLUpdate_SingleValue(
				"pages",
				"id",
				$id,
				"html_".strtolower($LANG),
				($extension==""?"":"wsa:extension:".$extension)
			);
}

	$arrNames=array("active_".strtolower($LANG),"name_".strtolower($LANG),"description_".strtolower($LANG),"keywords_".strtolower($LANG),"link_".strtolower($LANG));
	$arrValues=array($active,$pName,$pDescription,$pKeywords,$pLink);
	$database->SQLUpdate("pages",$arrNames,$arrValues," id=$id");
	$HideFormFlag=true;
	
}
?>



<?php
include("include/languages_menu.php");
?>




<?php
if($HideFormFlag){
?>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" width=750>
	<tr>
		<td class=basictext>
		<b>
<?php echo $strChangeSuccess; ?>
		</b>
		<br><br>

		</td>
	</tr>
</table>
<?php
}else{
?>
<form action="index.php" method="post">
<input type=hidden name=id value="<?php echo $id;?>">
<input type=hidden name=folder value=pages>
<input type=hidden name=page value=edit>
<input type=hidden name=category value=site_management>
<!--
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" width=750>
	<tr>
		<td class=basictext>
		<b><?php echo $strEditNewPage;?></b>
		</td>
	</tr>
</table>
-->

<table border="0" width="750" cellspacing="6">
	<tr>
		<td class=basictext><b><? echo $str_PageLinkPage;?></b></td>
		<td class=basictext>
		<input type="text" name="pLink" size="64" maxlength="256" value="<?php echo stripslashes($oArr['link_'.$lang]);?>">
		</td>
	</tr>

	<tr>
		<td class=basictext width=150><b><? echo $str_PageNamePage;?></b></td>
		<td class=basictext>
		<input type="text" name="pName" size="64" maxlength="256"  value="<?php echo stripslashes($oArr['name_'.$lang]);?>">
  		</td>
	</tr>

	<tr>
		<td class=basictext width=150><b><? echo $ACTIVE;?>:</b></td>
		<td class=basictext>
		
		<select name=active>
		<option value=1 <?php if($oArr['active_'.$lang]==1) echo "selected";?>><?php echo strtoupper($YES);?></option>
		<option value=0 <?php if($oArr['active_'.$lang]==0) echo "selected";?>><?php echo strtoupper($NO);?></option>
		</select>		

  		</td>
	</tr>


	<tr>
		<td class=basictext width=150><b><? echo $DEFAULT;?>:</b></td>
		<td class=basictext>
		<?php
		$strCurrentDefault = Parameter(1);
		?>
		<input <?php if($strCurrentDefault==$id) echo "checked";?> type=radio name=is_default value=yes> <?php echo strtoupper($YES);?>		

		<input <?php if($strCurrentDefault!=$id) echo "checked";?> type=radio name=is_default value=no> <?php echo strtoupper($NO);?>
  		</td>
	</tr>


	<tr>
		<td class=basictext width=150><b><? echo $CUSTOM_EXTENSION;?>:</b></td>
		<td class=basictext>
		
		<select name=extension>
		<option>NONE</option>
		<?php
		$handle=opendir('../extensions');
		while ($file = readdir($handle))
		{
		    if ($file != "." && $file != "..")
			{
				if("wsa:extension:".str_replace(".php","",$file) == $oArr["html_".$lang] )
				{
					echo "<option selected>".str_replace(".php","",$file)."</option>";
				}
				else
				{
					echo "<option>".str_replace(".php","",$file)."</option>";
				}
				
		   }
		}
		?>
		</select>
		
		
  		</td>
	</tr>

	<tr>
		<td class=basictext valign=top><b><? echo $str_PageDescriptionPage;?></b></td>
		<td class=basictext>
		<textarea name="pDescription" cols="50" rows="5"><?php echo stripslashes($oArr['description_'.$lang]);?></textarea>
  
		</td>
	</tr>
	<tr>
		<td class=basictext valign=top><b><? echo $str_PageKeywordsPage;?></b></td>
		<td class=basictext>
		<textarea name="pKeywords" cols="50" rows="10"><?php echo stripslashes($oArr['keywords_'.$lang]);?></textarea>
  
		</td>
	</tr>

</table>

<table summary="" border="0" width=750>
	<tr>
		<td class=basictext>

<br><br>
<input type=hidden name=Proceed value="">
<input type=submit class=adminButton value="<?php echo $SAUVEGARDER;?>">
		</td>
	</tr>
</table>
</form>
<?php
}
?>

<br>
<?php
generateBackLink("pages");
?>
