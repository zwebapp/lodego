<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
include_once("Utils.php");
include_once("security.php");

if(!isset($page)){
	$page="0";
}

if(stristr($_SERVER['HTTP_USER_AGENT'],"MSIE"))
{
	$N="IE";
}
else
{
	$N="FF";
}

$LANG="en";

include("../include/texts_en.php");

$HTMLCODE="";

$oPage=mysql_fetch_array(DataTable("user_pages","where  user='".$AuthUserName."' AND  id=$page"));
$HTMLCODE = $oPage['html_'.$LANG];	
$HTMLCODE = ereg_replace("<wsa ([a-z_]+)/>","[WSA TAG: \\1]",$HTMLCODE);

?>
<html>
<head>
<script>
function pageSaved()
{
	alert("The new content has been saved successfully!");
}

</script>

<script language="javascript" type="text/javascript" src="../ADMIN/jscripts/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,searchreplace,print,contextmenu,paste,directionality",
		theme_advanced_buttons1_add_before : "save,newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_path_location : "none",
		
	    plugin_insertdate_dateFormat : "%Y-%m-%d",
	    plugin_insertdate_timeFormat : "%H:%M:%S",
		extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
		file_browser_callback : "fileBrowserCallBack"
	});

	function fileBrowserCallBack(field_name, url, type) 
	{

	}
</script>


</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
<iframe id="opIframe" name="opIframe" src="blank.html" style="position:absolute;top:0px;left:0px;visibility:hidden" width="1" height="1"> </iframe>
<form method="post"  action="savePage.php" target="opIframe">

<input type=hidden  name="page" value="<?php echo($page);?>">

<input type=hidden  name="LANG" value="<?php echo($LANG);?>">


<textarea id="strCode" name="strCode" rows="15" cols="80" style="height:100%;width: 100%;background:white">

<?php echo(str_replace("=\"images/","=\"../images/", stripslashes($HTMLCODE) ));?>
	
</textarea>
</form>

</body>
</html>
