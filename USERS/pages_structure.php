<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
$ProductName="PHP Classified Ads v3.0";

$oLinkTexts=array($M_HOME,$M_PROFILE,$M_MY_ADS,$M_LOGOUT);
$oLinkActions=array("home","profile","ads","exit");

$profile_oLinkTexts=array($M_EDIT,$M_CONTACT);
$profile_oLinkActions=array("edit","contact");

$messages_oLinkTexts=array($RECEIVED_MESSAGES);
$messages_oLinkActions=array("received");

$home_oLinkTexts=array($M_DASHBOARD,$M_PASSWORD,$RECEIVED_MESSAGES);
$home_oLinkActions=array("welcome","password","received");

$ads_oLinkTexts=array($M_MY_ADS,$M_EXPIRED_ADS,$M_BANNERS);
$ads_oLinkActions=array("list","expired_ads","banners");

$exit_oLinkTexts=array($M_THANK_YOU);
$exit_oLinkActions=array("exit");
?>