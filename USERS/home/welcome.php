<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<br/><br/>
	<div class="text-left col-md-6">
	
		<h3>
		<img src="images/icons/graph.png"/>
		<?php echo $M_RIGHT_NOW;?></h3>
		<hr/>
		<b><?php echo $database->SQLCount("listings","WHERE username='".$AuthUserName."' AND expires>=".time()." AND status=1");?></b>
		<a class="dashboard-link" href="index.php?category=ads&action=list"><?php echo $M_ACTIVE_LISTINGS;?></a>
		<br>
		<b><?php echo $database->SQLCount("listings","WHERE username='".$AuthUserName."' AND expires>=".time()." AND status=0");?></b>
		<a class="dashboard-link" href="index.php?category=ads&action=waiting"><?php echo $M_ADS_WAITING;?></a>
		<br>
		<b><?php echo $database->SQLCount("listings","WHERE username='".$AuthUserName."' AND expires<".time());?></b>
		<a class="dashboard-link" href="index.php?category=ads&action=expired_ads"><?php echo $M_EXPIRED_LISTINGS;?></a>
		<br>
		<b><?php echo $database->SQLCount("ext_banners","WHERE agent='".$AuthUserName."' ");?></b>
		<a class="dashboard-link" href="index.php?category=ads&action=banners"><?php echo $M_BANNERS;?></a>
		
		<br><br><br>
		<h3>
		<img src="images/icons/check.png"/>
		<?php echo $M_YOUR_LATEST_LISTINGS;?>
		</h3>
		
		<hr/>
		
		<!--recent listings-->
	
		<?php
		
		
			
		$csTable=$database->DataTable("listings","WHERE username='".$AuthUserName."'  ORDER BY id DESC LIMIT 0,3 ");

		if(mysql_num_rows($csTable) == 0)
		{
			echo "<i class=\"sub-text\">".$ANY_ADS."</i>";
		}
							
		$i_listings_counter = 0;


		while($listing = mysql_fetch_array($csTable))
		{
			$i_listings_counter++;
							
			$adFeatured = false;
			
			if($listing["featured"]==1)
			{
				$adFeatured = true;
			}
			
			
			if($website->GetParam("SEO_URLS")==1)
			{
				$strLink = "../".($MULTI_LANGUAGE_SITE?$M_SEO_AD:"ad")."-".$website->format_str(strip_tags(stripslashes($listing["title"])))."-".$listing["id"].".html";
			}
			else
			{
				$strLink = "../index.php?mod=details&id=".$listing["id"].($MULTI_LANGUAGE_SITE?"&lang=".$website->lang:"");
			}
			?>
		
			<div class="listing">
		
				<?php
				if(trim($listing["images"])!="")
				{
				?>
				<div class="result-image">
					<?php
					echo $website->show_pic($listing["images"],"small","","","../");
					?>
				</div>
			  
				<div class="result-text">
				<?php
				}
				?>
					<a target="_blank" href="<?php echo $strLink;?>">
					<span class="<?php if($adFeatured) echo "featured_";?>listing_title"><?php echo stripslashes(strip_tags($listing["title"]));?></span></a>
					
					
					
					<?php
					if(trim($listing["fields"]) != "")
					{
					?>
						<div class="listing_description">
						<?php 
						$listing_fields = unserialize($listing["fields"]);
				
						if(is_array($listing_fields))
						{
							foreach($listing_fields as $key=>$value)
							{
							?>
								<span class="img-right-margin"><?php echo $key;?>: <?php echo $value;?></span>
							<?php
							}
						}
						
						?>
						</div>
					<?php
					}
					?>
					
					
					<?php
					if(trim($listing["description"]) != "")
					{
					?>
						<div class="listing_description">
						<?php echo $website->text_words(stripslashes(strip_tags($listing["description"])),30);?>
						</div>
					<?php
					}
					?>
					<div class="clear"></div>
					<span class="listing_posted_date"><?php echo $M_POSTED_ON;?> <?php echo date($website->GetParam("DATE_FORMAT"),strip_tags($listing["date"]));?></span>
					
				<?php
				if(trim($listing["images"])!="")
				{
				?>
				</div>
				<?php
				}
				?>
				<div class="clear"></div>
				
				
			</div>
			<div class="clear"></div>
			<hr/>	
			
			<?php
			
		}

		?>
		<div class="clearfix"></div>
		
		<br/>

		
	</div>


	<div class="text-left col-md-6">
	
		<h3>
		<img src="images/icons/ad.png"/> 
		<?php echo $M_SUBMIT_LISTING;?>
		</h3>
		
		<hr/>
		
		<br/>
		<script>
		function gSubmitForm(x)
		{
			
			document.getElementById("link_category").value=category_selected_location;
			
			return true;
		}
		
		function OfferType(x)
		{
			if(x==1)
			{
				document.getElementById("type_coupon").style.display="block";
				document.getElementById("type_printable").style.display="none";
			}
			else
			if(x==2)
			{
				document.getElementById("type_coupon").style.display="none";
				document.getElementById("type_printable").style.display="block";
			}
			else
			{
				document.getElementById("type_coupon").style.display="none";
				document.getElementById("type_printable").style.display="none";
			}
		}
		</script>
		<form onsubmit="return gSubmitForm(this)" id="user-form" action="index.php" method="post">
		<input type="hidden" name="category" value="ads"/>
		<input type="hidden" name="action" value="post"/>
		
		
	
				<div class="div_label">
					<?php echo $M_OFFER_TYPE;?>(*)
				</div>
				
				<div class="div_field">
				
					<input type="radio" checked name="offer_type" onmousedown="javascript:OfferType(1)" value="1"/>
					<?php echo $M_COUPON_CODE;?>
					<div class="clear"></div>
					<input type="radio" name="offer_type" onmousedown="javascript:OfferType(2)" value="2"/>
					<?php echo $M_PRINTABLE_COUPON;?>
					<div class="clear"></div>
					<input type="radio" name="offer_type" onmousedown="javascript:OfferType(3)" value="3"/>
					<?php echo $M_SALE_TIP;?>
					<div class="clear"></div>
					
					
				</div>
			<div class="clear"></div>
			<br/>

			<div id="type_coupon">
				<div class="div_label"><?php echo $M_COUPON_CODE;?>:</div>
				
				<div class="div_field">
					<input class="large-field" id="coupon_code" name="coupon_code"  <?php if(isset($_REQUEST["coupon_code"])) echo "value=\"".stripslashes($_REQUEST["coupon_code"])."\"";?> type="text" placeholder="<?php echo $M_EX_COUPON;?>"/>
				</div>
			</div>	
			<div class="clear"></div>
			
			<div id="type_printable" class="no-display">
				<div class="div_label"><?php echo $M_OFFER_LINK;?>:</div>
				
				<div class="div_field">
					<input class="large-field"  id="offer_link" name="offer_link"  <?php if(isset($_REQUEST["offer_link"])) echo "value=\"".stripslashes($_REQUEST["offer_link"])."\"";?> type="text" placeholder="<?php echo $M_EX_OFFER;?>"/>
				</div>
			</div>			
		
			<div class="clear"></div>
			<br/>
		

				<?php
				$list_cat_ids ="";
				?>
				
					<div class="div_label">
						<?php echo $M_CATEGORY;?>:
					</div>
					
					<div class="div_field">
						
						
						<select name="link_category_menu" id="link_category_menu" required>
						
						<?php
						
						if(file_exists('../categories/categories_'.strtolower($website->lang).'.php'))
						{
							$categories_content = file_get_contents('../categories/categories_'.strtolower($website->lang).'.php');
						}
						else
						{
							$categories_content = file_get_contents('../categories/categories_en.php');
						}

						$arrCategories = explode("\n", trim($categories_content));

						$categories=array();
						
						foreach($arrCategories as $str_category)
						{
							list($key,$value)=explode(". ",$str_category);
							$categories[trim($key)]=trim($value);
							
							$i_level = substr_count($key, ".");
							
							if($i_level!=0) continue;
							
							if($list_cat_ids!="") $list_cat_ids.=",";
							$list_cat_ids .=trim($key);
							
							echo "<option ".(isset($_REQUEST["link_category"])&&$_REQUEST["link_category"]==trim($key)?"selected":"")." value=\"".trim($key)."\">".trim($value)."</option>";
						}
						
						
						?>
						
						
						</select>
						
						<script>
						var category_suggest_location_url="../include/suggest_category.php";
						</script>
						<script type="text/javascript" src="../include/multi_category_select.js"></script>
						<input type="hidden" name="link_category" id="link_category" value="">
						<script type="text/javascript">
						var m_all="<?php echo $M_PLEASE_SELECT;?>";
						category_transform_select("link_category_menu","390px","220px");
						</script>
					
						
					</div>	
				
					<div class="clear"></div>
			
			<script>
			var main_cat_ids=new Array(<?php echo $list_cat_ids;?>);
			</script>
			<div class="clear"></div>
			
			<br/>
			<div class="div_label">
				<?php echo $M_TITLE;?>:
			</div>
			
			<div class="div_field">
				<input class="large-field" id="title" name="title"  <?php if(isset($_REQUEST["title"])) echo "value=\"".stripslashes($_REQUEST["title"])."\"";?> type="text" placeholder="<?php echo $M_PLEASE_ENTER_TITLE;?>" required autofocus/>
			</div>
		<div class="clear"></div>	
		<br/>
		<input class="btn btn-success" type="submit" value="<?php echo $M_CONTINUE;?>"/>
	
		
		
		</form>
		<div class="clear"></div>
		<br/><br/>
		<h3>
		<img src="images/icons/message.png"/> 
		<?php echo $RECEIVED_MESSAGES;?>
		</h3>
	
		<hr/>
		
		
		<?php
		$count_messages = $database->SQLCount("ext_messages"," WHERE user_to='".$AuthUserName."' ");
		if($count_messages == 0)
		{
		?>
			<i class="sub-text"><?php echo $ANY_MESSAGES;?></i>
		<?php
		}
		else
		{
		?>
			
			<img src="images/new-message.png" width="45" height="45"/>
			<a href="index.php?category=home&action=received"><?php echo $M_YOU_CURRENTLY_HAVE;?> 
			<b><?php echo $count_messages;?></b>
			<?php echo $M_NEW_MESSAGE;?>
			</a>
		<?php
		}
		?>
		
		
		
		
		
	</div>
<div class="clear"></div>
<br/><br/> 