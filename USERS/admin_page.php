<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
$is_mobile=false;
include("../config.php");
include("../include/SiteManager.class.php");
include("../include/Database.class.php");
$website = new SiteManager();
$website->isAdminPanel = true;
$database = new Database();
$database->Connect($DBHost, $DBUser,$DBPass );
$database->SelectDB($DBName);
$website->SetDatabase($database);
include("security.php");
$website->LoadSettings();

include("include/AdminUser.class.php");
if(!isset($AuthUserName) || !isset($AuthGroup)) $website->ForceLogin();
$currentUser = new AdminUser($AuthUserName, $AuthGroup);
$currentUser->LoadPermissions();
$lang = $currentUser->GetLanguage();

include("../include/texts_".$lang.".php");
include("../ADMIN/texts_".$lang.".php");
include("include/init_vars.php");
		
include("include/page_functions.php");
include("include/AdminPage.class.php");
$currentPage = new AdminPage();
$output_html = $currentPage->GetHTML();

if(isset($_REQUEST["ajax_load"]))
{
	echo $output_html;
	echo "<script>parent.CallBack();</script>";
}
else
if(isset($_REQUEST["ajax_call"]))
{
	echo "<script>parent.CallBack();</script>";
}
else
{
	echo $output_html;
}

?>