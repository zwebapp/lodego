<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>

<span class="medium-font"><?php echo $POST_NEW_PROPERTY_SALE;?></span>

<br>


<?php

if($FREE_WEBSITE)
{
	$package = 0;
	$AddStep = 2;
}

if(!isset($_POST["AddStep"]))
{
	$AddStep = 1;
}

if(isset($_POST["ProceedSelectPackage"]))
{
	if(!isset($_POST["package"])||$_POST["package"]=="")
	{
		echo "
			<script>
				alert('".$SELECT_PACKAGE."');
			</script>
		";
	}
	else
	{
		$AddStep = 2;
	}
}

?>
<br>

<?php
if(isset($_POST["AddStep"]))
{
	$AddStep = $_POST["AddStep"];
}

if($AddStep == 1)
{
?>
	<span class="medium-font"><?php echo $SELECT_PACKAGE_OWN;?></span>
	
		<br><br>


<form action="index.php" method="post">
<input type="hidden" name="ProceedSelectPackage" value="1"/>
<input type="hidden" name="category" value="<?php echo $category; ?>"/>
<input type="hidden" name="action" value="<?php echo $action; ?>"/>
<?php

$hasPackages = false;

$packages = $database->DataTable("ext_packages_dealer","WHERE ads>0 AND employer='$AuthUserName'");

$hasValidPackages = false;

while($oPackage = mysql_fetch_array($packages))
{

		if($oPackage["active"] == "0")
		{
			echo "[<font color=red>".$PENDING_VALIDATION."</font>]";
		}
		else
		{
			$hasValidPackages = true;
			
			echo 
			"
				<input type=radio name=package value=\"".$oPackage["id"]."\">
			";
		}
		
		echo "	".$REMAINING_ADS.": <b>".$oPackage["ads"]."</b>
		&nbsp;&nbsp;";
		
		echo "	".$M_POSTED_EXPIRE.": <b>".($oPackage["expire_days"]==-1?"never":$oPackage["expire_days"]." days")."</b>
		&nbsp;&nbsp;";
		echo "	".$M_VIDEO_SUPPORT.": <b>".($oPackage["allow_video"]=="1"?$YES:$NO)."</b>
		&nbsp;&nbsp;";
	
		echo "[".$PRICE_WHOLE_PACKAGE.": <b>".$oPackage["price"]."</b> ".$M_CREDITS."]
		<br><br>
	";

	$hasPackages = true;
}
?>
<br>

<?php
if($hasPackages && $hasValidPackages)
{
?>
<input type="submit" value=" <?php echo $M_SELECT;?> " class=adminButton>
<?php
}
else
{
?>

<b><font color=red><?php echo $ANY_VALID_PACKAGES;?></font></b>
<br><br><br>
<?php echo $M_PLEASE;?> <a href="index.php?category=ads&page=packages"><?php echo $PURCHASE_ONE;?></a>, <?php echo $IN_ORDER_TO_BE_ABLE;?>

<?php
}
?>

</form>


<?php
}
?>




<?php
if($AddStep == 2)
{

?>



<?php

if(!isset($_POST["Step"])||$_POST["Step"]=="")
{
	$Step=1;
}
else
if($_POST["Step"]==1)
{
	$Step=2;
}
else
if($_POST["Step"]==2)
{

		if($MAX_NUMBER_IMAGES == 0)
		{
			$Step = 4;
		}
		else
		{
			$Step = 3;
		}

}
else
if($_POST["Step"]==3)
{

	if(isset($_POST["ProceedAddPicture"]))
	{
		$Step=3;
	}
	else
	{

		$Step=4;
	}
}


?>

<?php
if($Step==1)
{
?>

<script>
		function SubmitForm1(x)
		{
		
			if(x.price.value=="")
			{
				alert("<?php echo $ENTER_PROPERTY_PRICE;?>");
				x.price.focus()
				return false;
			}	
			
			if(x.price.value.match(/^\d+$/))
			{
				
			}	
			else
			{
				alert("<?php echo $PROPERTY_PRICE_DIGITS;?>");
				x.price.focus()
				return false;
			}
			
			
			return true;
		}
</script> 

<i><?php echo $M_SELL_OR_LET;?></i>
<hr/>


<form action="index.php" style="margin-bottom:0px;margin-top:0px" method=post name="form1" id="form1" onsubmit="return SubmitForm1(this)">	

<input type="hidden" name="AddStep" value="2">
<input type="hidden" name="package" value="<?php echo $_POST["package"];?>">
<input type="hidden" name="action" value="<?php echo $action;?>">
<input type="hidden" name="category" value="<?php echo $category;?>">
<input type="hidden" name="lang" value="<?php echo strtolower($lang);?>">
<input type="hidden" name="Step" value="1">
<br>
<?php echo $M_CHOOSE_TYPE;?>:		
<input type="radio" name="ad_type" value="sale" checked > <b><?php echo $M_SALE2;?></b>
<input type="radio" name="ad_type" value="rent"> <b><?php echo $M_RENT2;?></b>

&nbsp;&nbsp;&nbsp;

<?php echo $PROPERTY_TYPE;?>:
<select size="1" name="property_type"  tabindex="2" class=iselect>
<script>

var types = Array(3);

<?php

$strType2 = "";

echo "types[1] = \"";
$tableTypes = $database->DataTable("property_types","ORDER BY name");
while($arrPType = mysql_fetch_array($tableTypes))
{
	$strType2 .= $arrPType["id"].",";
	
	echo str_show($arrPType["name"]).",";
}

echo "\";";

echo "types[2] = \"";
echo $strType2;
echo "\";";

?>
</script>
<script>
var reTypes = types[1].split(',');
var reTypes2 = types[2].split(',');
for(i=0;i<reTypes.length;i++)
{
				if(reTypes[i] != "")
				{
					document.write("<option value=\""+reTypes2[i]+"\">"+reTypes[i]+"</option>");
				}
}
</script>
</select>
&nbsp;&nbsp;&nbsp;
<?php echo $M_PRICE;?>:
<?php echo $CURRENCY_SYMBOL;?><input type=text name=price size=6 class=iselect2_small>

<?php
if($SITE_FORMAT=="US"||$ASK_FOR_ZIP)
{
?>
&nbsp;&nbsp;&nbsp;
<?php echo $M_ZIP;?>: <input type=text name="property_zip" size=6 class=iselect2_small>
<?php
}
?>

<br><br><br>

<input type=submit value=" <?php echo $M_CONTINUE;?> " class="ibutton">
<br>


</form>

<?php
}
?>


<?php
if($Step==2)
{
?>


	<script>
		function SubmitForm2(x){
		
			
									
			if(x.post_location.value==""||x.post_location.value=="-1")
			{
				alert("<?php echo $M_PLEASE_SELECT_PROPERTY_LOCATION;?>");
				
				return false;
			}	
			
			return true;
		
		}
		
	</script>
	
	<form action="index.php" method="post"  id="sell_form" name="sell_form" onsubmit="return SubmitForm2(this)">	
	<input type="hidden" name="ad_type" value="<?php echo get_param("ad_type");?>">
	
	<input type="hidden" name="property_type" value="<?php echo get_param("property_type");?>">
	<input type="hidden" name="price" value="<?php echo get_param("price");?>">
	<input type="hidden" name="property_zip" value="<?php echo get_param("property_zip");?>">
	<input type="hidden" name="AddStep" value="2">
	
	<input type="hidden" name="package" value="<?php echo $package;?>">
	<input type="hidden" name="action" value="<?php echo $action;?>">
	<input type="hidden" name="category" value="<?php echo $category;?>">
	
	<input type="hidden" name="lang" value="<?php echo $lang;?>">
	<input type="hidden" name="Step" value="<?php echo $Step;?>">
	

<i><?php echo $PROPERTY_INFO;?></i>
<hr color="#f3f3f3" size="1" width="100%">


 	
<table width="100%"   cellpadding="5">
	<tr>
		<td><?php echo $M_HEADLINE;?>:</td>
		<td colspan=3>
				<input type=text name="headline"  class=iselect2_big style="width:100%" value="<?php echo get_param("headline");?>">
		</td>
	</tr>
	<tr>
		<td width="25%"> </td>
		<td colspan=3>
				<i style="font-size:10px;position:relative;top:-10px"><?php echo $M_EMPTY_HEADLINE_EXPLANATION;?></i>
		</td>
	</tr>
	<tr>
		<td width="25%">
				<?php
					echo $M_LOCATION;
				?>: 
		</td>
		<td width="25%">
				
			<?php  	
			
			$arrLines = explode("\n",implode('', file('../locations/locations.php')));
			$form_name="sell_form";		
			$mod_form = "re_sell";
			$form_select_width="120";
			$step = 1;
			include("../include/location_form.php");
			
			?>
			
			
		</td>
		<td width="25%">
			<?php echo $M_SIZE_MSG;?>:
		</td>
		<td width="25%">
			<input type="text" name="size" value="<?php echo get_param("size");?>" style="width:120px">
		</td>
	</tr>
<?php

$property_type=$_POST["property_type"];

$website->ms_i($property_type);

$arrSelectedProperty = $database->DataArray("property_types","id=".$property_type);

if(is_array(unserialize(stripslashes($arrSelectedProperty["property_fields"]))))
{
	$arrPropertyFields = unserialize(stripslashes($arrSelectedProperty["property_fields"]));
}
else
{
	$arrPropertyFields = array();
}	


$iFCounter = 0;

foreach($arrPropertyFields as $arrPropertyField)
{
	if($iFCounter%2==0)
	{
		echo "<tr>";
	}

	echo "<td>".str_show($arrPropertyField[0],true).":</td>";	
	
	echo "<td>";
	
	if(trim($arrPropertyField[2]) != "")
	{
	
			$arrFieldValues = explode("\n", trim($arrPropertyField[2]));
			
			
			echo "<select  name=\"pfield".$iFCounter."\" style=\"width:".$arrPropertyField[1]."px\">";
					
						
			if(sizeof($arrFieldValues) > 0)
			{
				foreach($arrFieldValues as $strFieldValue)
				{
				
						echo "<option ".(str_show($strFieldValue, true)!=""&&str_show($strFieldValue, true)==get_param("pfield".$iFCounter)?"selected":"").">".str_show($strFieldValue, true)."</option>";
								
				}
			}
			
			echo "</select>";
	}
	else
	{
			echo "<input type=text value=\"".get_param("pfield".$iFCounter)."\" name=\"pfield".$iFCounter."\" style=\"width:".$arrPropertyField[1]."px\">";
	}
	
	echo "</td>";
	
	if(($iFCounter+1)%2==0)
	{
		echo "</tr>";
	}

	$iFCounter++;		
}
			

?>	
	
</table>


<br> 
<i><?php echo $PROPERTY_FEATURES;?></i>
<hr color="#f3f3f3" size="1" width="100%">
<table border="0" class="sell_table" cellpadding="3" cellspacing="0">

   <tr>
   		<td>
				<table width="100%"   cellpadding="5">
						
<?php


if(file_exists('../include/property_features_'.strtolower($LANGUAGE).'.php'))
{
	$strFeatures = implode('',file('../include/property_features_'.strtolower($LANGUAGE).'.php'));
}
else
{
	$strFeatures = implode('',file('../include/property_features_en.php'));
}

$arrPropertyFeatures = explode("\n", trim($strFeatures) );
$iFeaturesCounter = 0;

foreach($arrPropertyFeatures as $arrPropertyFeature)
{

	if($iFeaturesCounter%3 == 0)
	{
		echo '<TR>';
	}
	 echo '<TD width="33%"><INPUT type=checkbox value="'.$arrPropertyFeature.'" name="re_opt'.($iFeaturesCounter+1).'" '.(get_param("re_opt".($iFeaturesCounter+1))!=""?"checked":"").' >'.$arrPropertyFeature.'</TD>';
	 
	 if(($iFeaturesCounter+1)%3 == 0)
	{
		echo '</TR>';
	}
	
	$iFeaturesCounter++;
}

?>

				 </table>
	
			</td>
		</tr>
	</table>

<br>
<i><?php echo $PROPERTY_DESCRIPTION;?></i>
<hr color="#f3f3f3" size="1" width="100%">

<table border="0" class="sell_table" cellpadding="3" cellspacing="0">
   <tr>
   		<td>
		
				<table width="100%"   cellpadding="5">
				 	<tr>
				 		<td>
						
						<textarea cols="64" rows="5" name="description"><?php echo get_param("description");?></textarea>
						<br>
				 		
						</td>
				 	</tr>
				 </table>
				
		</td>
	</tr>
</table>
<?php
if($ENABLE_GOOGLE_MAPS)
{
?>


<br> 
<i><?php echo $M_GOOGLE_ADS_COORDINATES;?></i>
<hr color="#f3f3f3" size="1" width="100%">




<table border="0" class=sell_table cellpadding="3" cellspacing="0">
   <tr>
   		<td>
		
			<table width="100%"   cellpadding="5">
				<tr>
					<td valign="top"><?php echo $M_LATITUDE;?>:</td>
					<td align="center">
						<input type=text name="latitude"   style="width:120px" value="<?php echo get_param("latitude");?>">
						<br>
						<span style="font-size:10px">e.g. 40.758224</span>
					</td>
					
					<td valign="top"><?php echo $M_LONGITUDE;?>:</td>
					<td align="center">
						<input type=text name="longitude"   style="width:120px" value="<?php echo get_param("longitude");?>"> 
						<br>
						<span style="font-size:10px">e.g. -73.917404</span>
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
<br>
<?php
}
?>

<br>
<table border="0" class=sell_table cellpadding="3" cellspacing="0">
   <tr>
   		<td>
		
						<input type="submit" value=" <?php echo $M_CONTINUE;?> " class="ibutton">
				
		</td>
	</tr>
</table>


</form>	

<?php
}
?>






<?php
if($Step==3)
{

if(isset($ProceedAddPicture))
{
	if(file_exists("include/img_pr.php"))
	{
		include("include/img_pr.php");
	}
	
		if($video_file_id!="")
	{
	
			$database->SQLUpdate_SingleValue
			(
				"re_ads",
				"id",
				$re_id,
				"video_id",
				$video_file_id
			);
		
	}
	
	if($image_id!="")
	{
	
		$database->SQLUpdate_SingleValue
			(
				"re_ads",
				"id",
				$re_id,
				"image_id",
				$image_id
			);
		
	}
	
		if($more_images !="")
		{
	
		  	$database->SQLUpdate_SingleValue
		  	(
				"re_ads",
				"id",
				$re_id,
				"more_images",
				$more_images
			);
			
		}
}
else
{

		$strOptionList = "";
		
		$bFirst=true;
		
		foreach ($_POST as $key=>$value ) 
		{ 
				if(substr($key,0,6)=="re_opt")
				{
					if(!$bFirst){
						$strOptionList .=", ";
					}
					
					$strOptionList .= trim($value);
				
					$bFirst = false;
				}
		}
		
		ms_i($property_type);		
		$arrSelectedProperty = $database->DataArray("property_types","id=".$property_type);
		
		if(is_array(unserialize(stripslashes($arrSelectedProperty["property_fields"]))))
		{
				$arrPropertyFields = unserialize(stripslashes($arrSelectedProperty["property_fields"]));
		}
		else
		{
				$arrPropertyFields = array();
		}	


			$arrPValues = array();
			
			$iFCounter = 0;
			
			foreach($arrPropertyFields as $arrPropertyField)
			{		
				$v_name = "pfield".$iFCounter;
				$arrPValues[$arrPropertyField[0]]=$$v_name;
				$iFCounter++;
			}
			ms_i($package);
			$arrAgentPackage = $database->DataArray("ext_packages_dealer","id=$package");
			/*
			$count_previous=
				$database->SQLCount_Query
				(
					"SELECT id FROM ".$DBprefix."re_ads 
					WHERE username='".$AuthUserName."'
					AND ad_type='".get_param("ad_type")."'
					AND property_type='".get_param("property_type")."'
					AND price='".get_param("price")."'
					AND size='".get_param("size"."'
					AND location='".get_param("post_location")."'
					AND expires>".time()."
					"
				);
			if($count_previous==0)
			*/
			$iLast=$database->SQLInsert
			(
				"re_ads",
				array("publish","aff","username","date",
				"property_zip","ad_type","property_type","price",
				"headline",
				"features","description","property_fields",
				"size",
				"location",
				"longitude",
				"latitude",
				"expires"
				),
				array(($AUTO_VALIDATE_AGENT_ADS?"1":"0"),"", $AuthUserName,time(),
				get_param("property_zip"),get_param("ad_type"),get_param("property_type"),get_param("price"),
				get_param("headline"),
				$strOptionList,$description,serialize($arrPValues),
				get_param("size"),
				get_param("post_location"),
				get_param("longitude"),
				get_param("latitude"),
				($FREE_WEBSITE?$FEATURED_ADS_EXPIRE*86400:0)+time()+($arrAgentPackage["expire_days"]==-1?(36500*86400):($arrAgentPackage["expire_days"]*86400)),
				)
			);
			
			if(!$FREE_WEBSITE)
			{
				$database->SQLUpdate_SingleValue
				(
								"ext_packages_dealer",
								"id",
								$package,
								"ads",
								(intval($arrAgentPackage["ads"])-1)
				);
			}
				
					
				
				$re_id=$database->SQLMaxPlus("re_ads","id","username='".$AuthUserName."'");
			
				
				if($AUTO_VALIDATE_AGENT_ADS&&$SEND_EMAIL_NOTIFICATIONS)
				{
				
					$iPublishID = $re_id;
					ms_i($iPublishID);
					$arrAd = $database->DataArray("re_ads","id=".$iPublishID);
					
					$strWhereQuery = " active=1";
					
					$strWhereQuery .= " AND (price_from=0 OR price_from<".$arrAd["price"].") ";
					$strWhereQuery .= " AND (price_to=0 OR price_to>".$arrAd["price"].") ";
					$strWhereQuery .= " AND (location='' OR location LIKE '".$arrAd["location"]."%') ";
					$strWhereQuery .= " AND type='".$arrAd["ad_type"]."'";
					$strWhereQuery .= " AND property_type='".$arrAd["property_type"]."'";
					
					$tableNotifications = $database->DataTable("notifications","WHERE".$strWhereQuery);
					
					
					while($arrNotification=mysql_fetch_array($tableNotifications))
					{
	
						global $DOMAIN_NAME,$NAME_FROM,$EMAIL_FROM,$SEND_EMAIL_NOTIFICATIONS, $NEW_PROPERTY_EMAIL_ALERT_SUBJECT, $NEW_PROPERTY_EMAIL_ALERT_MESSAGE;


						if($SEND_EMAIL_NOTIFICATIONS)
						{
							$NEW_PROPERTY_EMAIL_ALERT_MESSAGE = str_replace("[LINK]","http://www.".$DOMAIN_NAME."/index.php?mod=re_search&ad=".$arrAd["id"],$NEW_PROPERTY_EMAIL_ALERT_MESSAGE);
							
							mail
							(
								$arrNotification["email"],$NEW_PROPERTY_EMAIL_ALERT_SUBJECT,$NEW_PROPERTY_EMAIL_ALERT_MESSAGE,
								"From: ".$NAME_FROM." <".$EMAIL_FROM.">\nReply-To: ".$EMAIL_FROM.""
							);	
						}
					}
				}
		
	//end 2
}
//3 start
?>
	<form action="index.php" method="post" id="form3" name="form3"  ENCTYPE="multipart/form-data">	
	<input type="hidden" name="ProceedAddPicture" value="1">
	<input type="hidden" name="AddStep" value="2">
	<input type="hidden" name="package" value="<?php echo $package;?>">
	<input type="hidden" name="action" value="<?php echo $action;?>">
	<input type="hidden" name="category" value="<?php echo $category;?>">
	<input type="hidden" name="property_zip" value="<?php echo get_param("property_zip");?>">
	<input type="hidden" name="lang" value="<?php echo $lang;?>">
	<input type="hidden" name="property_type" value="<?php echo $property_type;?>">
	<input type="hidden" name="Step" value="<?php echo $Step;?>">
	
	<input type="hidden" name="re_id" value="<?php echo (isset($re_id)?$re_id:$iLast);?>">
	
	
 <i><?php echo $PROPERTY_PICTURES;?></i>
<hr color="#f3f3f3" size="1" width="100%">

<table border="0" class="sell_table" cellpadding="3" cellspacing="0">

   <tr>
   		<td>
<table width="100%"   cellpadding="5">

 	<tr>
 		<td>
		
		
		
		<?php
		if(isset($ProceedAddPicture))
		{
		?>
				
					<b><?php echo $MAIN_PICTURE;?>:  </b>
					<br><br>
							
					<?php
					if(!isset($image_id) || $image_id < 2)
					{
					?>
						<img src="../images/no_pic.gif" border="0" width="200" height="150" alt="">
					<?php
					}
					else
					{
					?>
						<img src="../image.php?id=<?php echo $image_id;?>" border="0" width="200" height="150" alt="">
					<?php
					}
					?>
			
			
					<?php
					if($more_images != "")
					{
									
						$arrImages = explode(",", $more_images);
					
						$iImageCounter =1;	
						foreach($arrImages as $strImageId)
						{
							if(trim($strImageId) != "")
							{
					?>
									<br><br>
									<?php echo $M_PICTURE;?> <?php echo $iImageCounter;?>:
									<br><br>
									
										<?php
										if(!isset($strImageId) || $strImageId < 2)
										{
										?>
											<img src="../images/no_pic.gif" border="0" width="200" height="150" alt="">
										<?php
										}
										else
										{
										?>
											<?php
											if($USE_GD)
											{
											?>
												<img src="../thumbnail.php?id=<?php echo $strImageId;?>&w=200&h=150" border="0" width="200" height="150" alt="">
											
											<?php
											}
											else
											{
											?>
												<img src="../image.php?id=<?php echo $strImageId;?>" border="0" width="200" height="150" alt="">
											<?php
											}
											?>
											
											
										<?php
										}
										?>
									
									
								<?php
							}
					
							$iImageCounter++;
						}
						
					}
					?>	
			
		<br>
		<?php
		}
		else
		{
		?>
		
		
	
			<img src="../images/no_pic.gif" border="0" width="200" height="150" alt="">
	
		<br><br>
		<table border="0" cellspacing="8">
		  	<tr>
		  		<td><?php echo $MAIN_PICTURE;?>: </td>
		  		<td><input type="file" id="userfile" name="userfile"></td>
		  	</tr>
  			<tr>
				<td colspan=2>
				<?php echo $HOW_MANY_ADDITIONAL;?>:
				
				<select name="images_number"   id="images_number" onChange="ShowFields(this);">
				<?php
				for($i=0;$i<=($MAX_NUMBER_IMAGES-1);$i++)
				{
					echo "<option value=\"".$i."\">".$i."</option>";
				}
				?>
				</select>
		</td>
	</tr>
  </table>
  	
	<script>
	function ShowFields(x)
	{
		var iFields = parseInt(x.options[x.selectedIndex].value);
		
		var strCode = "";
		
		strCode +="<table border=\"0\" cellspacing=\"8\">";
		
		for(i=0;i<iFields;i++)
		{
			strCode +="<tr>";
  				strCode +="<td><?php echo $M_IMAGE;?> "+(i+1)+":</td>";
  				strCode +="<td> <input type=file id=userfile"+(i+1)+" name=userfile"+(i+1)+"></td>";
  			strCode +="</tr>";
		}
		
		strCode +="</table>";
		
		document.getElementById("UploadFields").innerHTML = strCode;
		
	}
	</script>
	
	<div id="UploadFields">
	
	</div>
		
	<?php
	
	}
	
	ms_i($package);
	$arrAgentPackage = $database->DataArray("ext_packages_dealer","id=$package");

	
	?>
				

		</td>
		
		
		
		
 	</tr>
 </table>
 
	</td>
	</tr>
	</table>
 
 
	<?php 
	
	
if($ALLOW_VIDEO&& ($arrAgentPackage["allow_video"]==1) )
{

 	if($arrAgentPackage["allow_video"]==1)
	{
	?>	 
		<i><?php echo $M_PROPERTY_VIDEO;?></i>
		<hr color="#f3f3f3" size="1" width="100%">
		
	<?php
	}
	?>
	<br>
	<table border="0" cellpadding="5">
	<tr>
		<td>
		
		<?php 
		if(!isset($ProceedAddPicture)) 
		{
		?>
	<table summary="" border="0" cellspacing="8" >
	<tr>
		<td>
		
		<?php echo $M_VIDEO_FILE;?>:
		</td>
		
		<td>
		
		
		
			<input type="file" name="video_file" id="video_file" value="">
		
		</td>
	</tr>
</table>

<?php
		}
		else
		{
		
			if($video_file_name != "")
			{
		?>
					
					<table border="0" height="30" width="300" cellpadding="0" cellspacing="0">
				     	<tr>
				     		<td bgcolor="black" width="40" >
							
								<a href="../video.php?id=<?php echo $vir;?>" target="_blank"><img src="../images/play.gif" width="28" height="25" alt="" border="0"></a>
							
							</td>
				     		<td bgcolor="black">
							
								<a href="../video.php?id=<?php echo $vir;?>" target="_blank"><font color="white"><?php
								echo $video_file_name;
								?></font></a>
							
							</td>
				     	</tr>
				     </table>
					
					
		<?php
			}
		
		}
		?>
		

		</td>
	</tr>
</table>
<?php
}
?>
</form>

<script>
function Form4Submit()
{
	var hasValue = false;
	
	if(document.getElementById("userfile").value!="") hasValue = true;
	
	var iFields = parseInt(document.getElementById("images_number").options[document.getElementById("images_number").selectedIndex].value);
	
	for(i=0;i<iFields;i++)
	{
		if
		(document.getElementById("userfile"+i)
		&&
		document.getElementById("userfile"+i).value!=""
		) { hasValue = true; break;}
	}
	
	<?php
	if($ALLOW_VIDEO)
	{
	?>
	if(document.getElementById("video_file")&&document.getElementById("video_file").value!="") hasValue = true;
	<?php
	}
	?>
	
	if(hasValue)
	document.getElementById("form3").submit();
	else 
	document.getElementById("form4").submit();;
	
	return false;
}
</script>

 <form action="index.php" method="post" id="form4" name="form4" <?php if(!isset($ProceedAddPicture)) echo "onsubmit=\"return Form4Submit()\"";?>>	
 
	<input type="hidden" name="AddStep" value="2">
	<input type="hidden" name="package" value="<?php echo $package;?>">
	<input type="hidden" name="action" value="<?php echo $action;?>">
	<input type="hidden" name="category" value="<?php echo $category;?>">
	<input type="hidden" name="property_zip" value="<?php echo get_param("property_zip");?>">
	<input type="hidden" name="lang" value="<?php echo $lang;?>">
	<input type="hidden" name="Step" value="<?php echo $Step;?>">
	
	<input type="hidden" name="re_id" value="<?php echo $re_id;?>">
	<input type="hidden" name="image_id" value="<?php echo (isset($image_id)?$image_id:"");?>">
	<input type="hidden" name="video_file_id" value="<?php if(isset($video_file_id)) echo $video_file_id;?>">
	<input type="hidden" name="more_images" value="<?php echo get_param("more_images");?>">
	
	<table summary="" border="0" width="95%">
 	<tr>
 		<td>&nbsp;
		
		<div style="text-align:right">
		<input type=submit value=" <?php echo $M_CONTINUE;?> " class=ibutton>
		</div>
		
		</td>
 	</tr>
 </table>
	
	
	  
	</form>	
<?php
//end 3


}
?>


<?php
if($Step==4)
{
?>

<center>
<table summary="" border="0" width=95%>
	<tr>
		<td>
		
		<?php
	
								
				echo "<i style=\"font-size:12px\">".($AUTO_VALIDATE_AGENT_ADS ?$M_ADD_THANK_YOU_POSTED:$M_ADD_THANK_YOU)."</i>";
								
				?>
		
		</td>
	</tr>
</table>
</center>
<?php
}
?>


<!-- addstep -->
<?php
}
?>

