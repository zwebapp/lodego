<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php 	

if(isset($_REQUEST["renew"])&&$_REQUEST["renew"]==1)
{
	$banner = $_REQUEST["banner"];
	
	$website->ms_i($banner);
	
	$arrBanner=$database->DataArray("ext_banners","id=".$banner);
	
	$arrSelectedArea = $database->DataArray("ext_banner_areas","id=".$arrBanner["banner_type"]);
							
	
	$database->SQLUpdate_SingleValue
	(
		"ext_banners",
		"id",
		$banner,
		"expires",
		($arrBanner["expires"]+$arrSelectedArea["days"]*86400)
		
	);

	$database->SQLUpdate_SingleValue
	(
		"ext_banners",
		"id",
		$banner,
		"active",
		"0"
		
	);	
	?>
	<br/>
	<h3><?php echo stripslashes($website->params[137]);?></h3>
	<br/><br/>
	<?php echo $M_PLEASE_SELECT_PAYMENT;?>
			
			<?php
			if(trim($website->GetParam("PAYPAL_ID")) !="")
			{
			?>	<br/><br/>
				<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
				<input type="hidden" name="cmd" value="_xclick">
				<input type="hidden" name="business" value="<?php echo $website->GetParam("PAYPAL_ID");?>">
				<input type="hidden" name="currency_code" value="<?php echo $website->GetParam("CURRENCY_CODE");?>">
				<input type="hidden" name="item_name" value="Payment for banner id#<?php echo $i_banner_id;?> on <?php echo $DOMAIN_NAME;?>">
				<input type="hidden" name="item_number" value="<?php echo $i_banner_id;?>">
				<input type="hidden" name="amount" value="<?php echo number_format($arrSelectedArea["price"], 2, '.', '');?>">
				<input type="image"  src="../images/paypal.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
				</form>
			<?php
			}
			?>
			
			<?php
			if(trim($website->GetParam("2CHECKOUT_ID")) !="")
			{
			?>	<br/><br/>
			
				<form target="_blank" action="https://www.2checkout.com/cgi-bin/sbuyers/cartpurchase.2c" method="post">
				<input type="hidden" name="sid" value="<?php echo trim($website->GetParam("2CHECKOUT_ID"));?>"> 
				<input type="hidden" name="cart_order_id" value="<?php echo $i_banner_id;?>"> 
				<input type="hidden" name="total" value="<?php echo number_format($arrSelectedArea["price"], 2, '.', '');?>">
				<input type="hidden" name="skip_landing" value="1"> 
				<input type="image" src="../images/2checkout.gif" alt="" border="0">
				</form>
				
			<?php
			}
			

}
else
{
if(isset($_POST["Delete"])&&sizeof($_POST["CheckList"])>0)
{

	$arrImgIds = array();
	foreach($_POST["CheckList"] as $strID)
	{
		$website->ms_i($strID);
		$arrB = $database->DataArray("ext_banners","id=".$strID." AND agent='".$AuthUserName."' ");
		
		if(!isset($arrB["id"]))
		{
			die("");
		}
		
		array_push($arrImgIds,$arrB["image_id"]);		
	}
	
	$database->SQLDelete("image","image_id",$arrImgIds);
	$database->SQLDelete("ext_banners","id",$_POST["CheckList"]);	
}
?>
<h3><?php echo $M_MANAGE_YOUR_BANNERS;?></h3>
		
<br>
<br>
		
		<i>
			<?php echo $M_SELECT_BANNER_AREA;?>:
		</i>
		
		
		<br><br><br>
		
		<?php
				
		$tableAreas = $database->DataTable("ext_banner_areas","");
		
		while($arrArea = mysql_fetch_array($tableAreas))
		{
		?>
		
		<table summary="" border="0" width="100%">
  			<tr>
  				<td>
				
						<b>
						[<?php echo $M_AREA;?> #<?php echo $arrArea["id"];?>] 
						<?php
						echo $arrArea["name"];
						?>
						</b>
				
				</td>
  				<td align="right">
				<b>
				<?php
				
				if($database->SQLCount("ext_banners","WHERE banner_type=".$arrArea["id"]) < ($arrArea["rows"]*$arrArea["cols"]))
				{
				?>
				
						<a href="index.php?category=<?php echo $category;?>&action=add_banner&area_id=<?php echo $arrArea["id"];?>">[<?php echo strtoupper($M_SELECT);?>]</a>
				<?php
				}
				else
				{
				?>
				
				<span class="red-font"><?php echo $M_SOLD_OUT;?></span>
				
				<?php
				}
				?>		
						
				</b>
				</td>
  			</tr>
  		</table>
		
		<hr width="100%">
	
		<i>
		<?php
		echo $arrArea["description"];
		?>
		</i>
		<br><br>
		<?php echo $M_TOTAL_BANNERS_AREA;?>: <b><?php echo $arrArea["rows"]*$arrArea["cols"];?> </b>
		<span style="font-size:9px">[<?php echo $arrArea["rows"];?> <?php echo $M_ROWS;?> X <?php echo $arrArea["cols"];?> <?php echo $M_COLUMNS;?>]</span>
		&nbsp;
		<?php echo $M_BANNER_SIZE;?>: <b><?php echo $arrArea["width"];?>px</b> X <b><?php echo $arrArea["height"];?>px</b>
		&nbsp;
		<?php echo $M_PRICE;?>: <b><?php echo $website->GetParam("WEBSITE_CURRENCY").$arrArea["price"];?></b>
		&nbsp;
		<?php echo $M_DAYS2;?>: <b><?php echo $arrArea["days"];?></b>
		
		<br><br><br>
		<?php
		}
		mysql_free_result($tableAreas);
		?>
		
		
		
		
		<br>
		
		<b><?php echo $M_LIST_CURRENT_B;?>:</b>
		
		
		<br>
		
		<?php

		if($database->SQLCount("ext_banners","WHERE agent='".$AuthUserName."' ")==0)
		{
		
			echo "<br>[".$M_CURRENTLY_NO_BANNERS."]";
		
		}
		else
		{
			
			RenderTable
			(
				"ext_banners",
				array("EditNote","RenewBanner","name","active","banner_type","date","image_id"),
				array($MODIFY,$M_RENEW,$NOM,$ACTIVE,$M_AREA2,$DATE_MESSAGE,$M_IMAGE),
				600,
				"WHERE agent='".$AuthUserName."' ",
				$EFFACER,
				"id",
				"index.php"
			);
						
						
		}
}
		?>
	