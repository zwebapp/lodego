<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
if(!isset($iKEY)||$iKEY!="AZ8007") die("ACCESS DENIED");
?>
<script>
function DeleteTemplate(x)
{
	if(confirm("<?php echo $M_SURE_DELETE_PACKAGE;?>"))
	{
		document.location.href="index.php?category=<?php echo $category;?>&action=<?php echo $action;?>&del="+x;
	}
}

function CallBack()
{
	document.getElementById("main-content").innerHTML =
	top.frames['ajax-ifr'].document.body.innerHTML;
	HideLoadingIcon();
}

$(function(){
	var offsetX = 20;
	var offsetY = -200;
	$('a.hover').hover(function(e){	
		var href = $(this).attr('href');
		$('<img id="largeImage" src="' + href + '" alt="image" />')
			.css({'top':e.pageY + offsetY,'left':e.pageX + offsetX})
			.appendTo('body');
	}, function(){
		$('#largeImage').remove();
	});
	$('a.hover').mousemove(function(e){
		$('#largeImage').css({'top':e.pageY + offsetY,'left':e.pageX + offsetX});
	});
	$('a.hover').click(function(e){
		e.preventDefault();
	});
});

</script>

<div class="fright">

	<?php
	echo LinkTile
		 (
			"ads",
			"waiting",
			$M_WAITING,
			"",
			
			"yellow"
		 );
	
	echo LinkTile
		 (
			"ads",
			"expired_ads",
			$M_EXPIRED_LISTINGS,
			"",
			
			"red"
		 );
		?>
</div>
<div class="clear"></div>
<?php
if(isset($_REQUEST["del"]))
{
	$website->ms_i($_REQUEST["del"]);
	
	$del_ad = $database->DataArray("listings","id=".$_REQUEST["del"]." AND username='".$AuthUserName."'");
	
	if(isset($del_ad["id"]))
	{
		$database->SQLDelete("listings","id",array($del_ad["id"]));
		
		$del_images = explode(",",$del_ad["images"]);
		foreach($del_images as $del_image)
		{
			if(file_exists("../uploaded_images/".$del_image.".jpg"))
			{
				unlink("../uploaded_images/".$del_image.".jpg");
			}
			if(file_exists("../thumbnails/".$del_image.".jpg"))
			{
				unlink("../thumbnails/".$del_image.".jpg");
			}
		}
	}
}

?>


<h3>
<?php echo $M_YOUR_ADS;?>
</h3>

<?php
$packages = array();
$arr_packages = $database->DataTable("packages","");

while($arr_package = mysql_fetch_array($arr_packages))
{
	$packages[$arr_package["id"]]=stripslashes($arr_package["name"]);
}

$_REQUEST["packages"]=$packages;

if(file_exists('../categories/categories_'.strtolower($lang).'.php'))
{
	$categories_content = file_get_contents('../categories/categories_'.strtolower($lang).'.php');
}
else
{
	$categories_content = file_get_contents('../categories/categories_en.php');
}

$arrCategories = explode("\n", trim($categories_content));

$categories=array();

foreach($arrCategories as $str_category)
{
	list($key,$value)=explode(". ",$str_category);
	$categories[trim($key)]=trim($value);
}
	
$_REQUEST["categories"]=$categories;

if($database->SQLCount("listings","WHERE username='".$AuthUserName."' AND status=1") == 0)	
{

	echo "<br/><br/><i>".$M_NO_ADS_FOUND."</i>";
}
else
{
	
	RenderTable
	(
		"listings",
		array("EditNote","show_images","date","title","site_info","package_info","DeleteTemplate"),
		array($MODIFY,$M_IMAGES,$DATE_MESSAGE,$M_TITLE,$DESCRIPTION,$M_CATEGORY,$EFFACER),
		750,
		"WHERE username='".$AuthUserName."' AND status=1",
		
		"",
		"id",
		"index.php",
		true,
		20,
		false,
		-1,
		"ORDER BY ID desc"
	);
}
?>

