<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
if(!isset($iKEY)||$iKEY!="AZ8007") die("ACCESS DENIED");
?>

<link href="../css/datepicker.css" rel="stylesheet">
<script src="../js/bootstrap-datepicker.js"></script>
<div class="fright">

	<?php
	echo LinkTile
		 (
			"ads",
			"list",
			$M_GO_BACK,
			"",
			
			"red"
		 );
	
		?>
</div>
<div class="clear"></div>


<?php
$id=$_REQUEST["id"];
$website->ms_i($id);
$_REQUEST["select-width"]="200";


AddEditForm
(
	array
	(
	$M_OFFER_TYPE.":",
	$M_COUPON_CODE.":",
	$M_OFFER_LINK.":",
	$M_CATEGORY.":",
	$M_IMAGES.":",
	$M_TITLE.":",
	$DESCRIPTION.":",
	$M_OFFER_EXPIRES.":",
	$NOM.":",
	$EMAIL.":"
	),
	array
	(
	"offer_type",
	"coupon",
	"offer_link",
	"link_category",
	"images",
	"title",
	"description",
	"offer_expires",
	"name",
	"email"
	
	),
	array("images","code"),
	array(
	"combobox_".$M_COUPON_CODE."^1_".$M_PRINTABLE_COUPON."^2_".$M_SALE_TIP."^3",
	"textbox_50",
	"textbox_50",
	"combobox_link_categories",
	"images",
	"textbox_50","textarea_50_5",
	"textbox_50","textbox_50","textbox_50"),
	"listings",
	"id",
	$id,
	$M_NEW_VALUES_SAVED
);
	
?>
<br>
<script>
$(function(){
	
	$('#offer_expires').datepicker({
		format: 'mm/dd/yyyy'
	});
	
	});
</script>

	