<?php
// Deals Portal
// http://www.netartmedia.net/dealsportal
// Copyright (c) All Rights Reserved NetArt Media
// Find out more about our products and services on:
// http://www.netartmedia.net
?>

<?php

if(!defined('IN_SCRIPT')) die("");
$website->ms_ew($_POST["link_category"]);
if(trim($_POST["link_category"])=="") die("The category is not set.");
$show_form = true;
$process_error="";

if(isset($_POST["ProceedSend"]))
{
	
	if(trim($_POST["title"])=="")
	{
		$process_error=$M_PLEASE_ENTER_TITLE;
	}
	else
	
	if(trim($_POST["link_category"])=="")
	{
		$process_error=$M_PLEASE_SELECT_CATEGORY;
	}
	
	else
	{
		
		$website->ms_i($_POST["package"]);
		
		$selected_package=$database->DataArray("packages","id=".$_REQUEST["package"]);
	
		///images processing
		$str_images_list = "";
		$path="../";	
		include("../include/images_processing.php");
		///end images processing
	
		
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

		$modify_code = '';
		
		for ($i = 0; $i < 24; $i++) 
		{
			$modify_code .= $characters[rand(0, strlen($characters) - 1)];
		}
		
		
		
		$arrPValues = array();
		$selected_cat_items = explode(".",$_POST["link_category"]);
		$selected_cat_id = $selected_cat_items[0]; 
		$category_fields= $database->DataArray("fields","cat_id=".$selected_cat_id);
		
		if(isset($category_fields["id"]))
		{
			$arr_cat_fields = unserialize(stripslashes($category_fields["fields"]));
				
			if(is_array($arr_cat_fields))
			{
				$iFCounter = 0;
					
				foreach($arr_cat_fields as $arr_cat_field)
				{		
					$arrPValues[$arr_cat_field[0]]=trim(strip_tags($_POST["pfield-".$selected_cat_id."-".$iFCounter]));
					$iFCounter++;
				}	
			}
		}
		
		
		$expire_time = time();
		$time_items = split("-",$_POST["expires"]);
		
		if(sizeof($time_items) == 3)
		{
			$expire_time = mktime(0, 0, 0, int($time_items[0]), int($time_items[1]), int($time_items[2]));
		}
		
		$i_submit_id = $database->SQLInsert
		(
			"listings",
			array
			(
				"offer_type",
				"coupon",
				"offer_link",
				"url",
				"business_name",
				"username",
				"package",
				"status",
				"title",
				
				"description",
				"link_category",
				"name",
				"phone",
				"email",
				"date",
				"featured",
				"offer_expires",
				"expires",
				"ip",
				"location",
				"user_type",
				"images",
				"code",
				"fields",
				"address",
				"latitude",
				"longitude",
				"email_verified"
			),
			array
			(
				$_POST["offer_type"],
				$_POST["coupon"],
				$_POST["offer_link"],
				$arrUser["website"],
				$arrUser["business_name"],
				$arrUser["username"],
				$_POST["package"],
				$website->GetParam("AUTO_APPROVE"),
				$_POST["title"],
				$_POST["description"],
				$_POST["link_category"],
				$arrUser["name"],
				$arrUser["user_phone"],
				$arrUser["user_email"],
				time(),
				$selected_package["featured"],
				$_POST["offer_expires"],
				time()+$selected_package["days"]*24*3600,
				$_SERVER["REMOTE_ADDR"],
				$_POST["location"],
				$arrUser["user_type"],
				$str_images_list,
				$modify_code,
				serialize($arrPValues),
				$_POST["address"],
				$_POST["latitude"],
				$_POST["longitude"],
				($website->GetParam("VERIFY_EMAIL")==1?"0":"1")
			)
		);
	
		$show_form=false;
		?>
		
		
		

<div class="fright">

	<?php
	echo LinkTile
		 (
			"ads",
			"waiting",
			$M_WAITING,
			"",
			
			"yellow"
		 );
	
	echo LinkTile
		 (
			"ads",
			"list",
			$M_ACTIVE_LISTINGS,
			"",
			
			"green"
		 );
		?>
</div>
<div class="clear"></div>
<br/>


		<?php
		if($selected_package["price"]==0)
		{
	
			echo "<h4>".nl2br(stripslashes($website->GetParam("SUCCESS_MESSAGE_FREE")))."</h4>";
		
		}
		else
		{
		?>
		
			<?php
			echo "<h4>".nl2br(stripslashes($website->GetParam("SUCCESS_MESSAGE_PAID")))."</h4>";
			?>
			<br/><br/>
			<?php echo $M_PLEASE_SELECT_PAYMENT;?>
			
			<?php
			if(trim($website->GetParam("PAYPAL_ID")) !="")
			{
			?>	<br/><br/>
				<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
				<input type="hidden" name="cmd" value="_xclick">
				<input type="hidden" name="business" value="<?php echo $website->GetParam("PAYPAL_ID");?>">
				<input type="hidden" name="currency_code" value="<?php echo $website->GetParam("CURRENCY_CODE");?>">
				<input type="hidden" name="item_name" value="Payment for ad id#<?php echo $i_submit_id;?> on <?php echo $DOMAIN_NAME;?>">
				<input type="hidden" name="item_number" value="<?php echo $i_submit_id;?>">
				<input type="hidden" name="amount" value="<?php echo number_format($selected_package["price"], 2, '.', '');?>">
				<input type="image"  src="../images/paypal.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
				</form>
			<?php
			}
			?>
			
			<?php
			if(trim($website->GetParam("2CHECKOUT_ID")) !="")
			{
			?>	<br/><br/>
			
				<form target="_blank" action="https://www.2checkout.com/cgi-bin/sbuyers/cartpurchase.2c" method="post">
				<input type="hidden" name="sid" value="<?php echo trim($website->GetParam("2CHECKOUT_ID"));?>"> 
				<input type="hidden" name="cart_order_id" value="<?php echo $i_submit_id;?>"> 
				<input type="hidden" name="total" value="<?php echo number_format($selected_package["price"], 2, '.', '');?>">
				<input type="hidden" name="skip_landing" value="1"> 
				<input type="image" src="../images/2checkout.gif" alt="" border="0">
				</form>
				
			<?php
			}
			?>
			
		<?php
		
		}
		
		?>
		
		
		<br/><br/>
		
				
		<?php
		
	}
	
	
	

}


if($show_form)
{

?>
<h2>
<?php
if($process_error=="")
{
	echo $M_SUBMIT_LISTING;
}
else
{
	echo $process_error;
}
?>
</h2>

<br/>
<div style="width:700px">
<script>
function gSubmitForm(x)
{
	<?php
	if(!isset($_REQUEST["location"]))
	{
	?>
	document.getElementById("location").value=selected_location;
	<?php
	}
	?>
	
	<?php
	if(!isset($_REQUEST["link_category"]))
	{
	?>
	document.getElementById("link_category").value=category_selected_location;
	<?php
	}
	?>
	return true;
}

</script>
<form id="main" onsubmit="return gSubmitForm(this)" action="index.php" method="post"  enctype="multipart/form-data">
<?php
if(isset($_REQUEST["package"]))
{
?>
<input type="hidden" name="package" value="<?php echo $_REQUEST["package"];?>"/>
<?php
}

if(isset($_REQUEST["lang"]))
{
?>
<input type="hidden" name="lang" value="<?php echo $_REQUEST["lang"];?>"/>
<?php
}
?>
<input type="hidden" name="category" value="<?php echo $_REQUEST["category"];?>"/>
<input type="hidden" name="action" value="<?php echo $_REQUEST["action"];?>"/>

<input type="hidden" name="ProceedSend" value="1"/>
<input type="hidden" name="link_category" value="<?php echo $_POST["link_category"];?>"/>
<input type="hidden" name="title" value="<?php echo $_POST["title"];?>"/>
<input type="hidden" name="offer_type" value="<?php if(isset($_REQUEST["offer_type"])) echo $_REQUEST["offer_type"];?>"/>
<input type="hidden" name="coupon" value="<?php if(isset($_REQUEST["coupon_code"])) echo $_REQUEST["coupon_code"];?>"/>
<input type="hidden" name="offer_link" value="<?php if(isset($_REQUEST["offer_link"])) echo $_REQUEST["offer_link"];?>"/>

<fieldset>
		<legend><?php echo $M_WEBSITE_DETAILS;?></legend>
		<ol>
			<li>
				<label for="expires">
				<?php echo $M_OFFER_EXPIRES;?>(*)
				<br>
				
				</label>
				 <link href="../css/datepicker.css" rel="stylesheet">
				 
				<script src="../js/bootstrap-datepicker.js"></script>
	
				<input type="text" name="offer_expires" value="<?php echo date("m/d/Y",time()+14*86400);?>" id="dp-expires" >
				<script>
				$(function(){
					
					$('#dp-expires').datepicker({
						format: 'mm/dd/yyyy'
					});
					
					});
				</script>
			</li>
			<li>
				<label for="description"><?php echo $M_DESCRIPTION;?>(*)
				<br>
				
				</label>
				<textarea id="description" name="description" rows="10"> <?php if(isset($_REQUEST["description"])) echo stripslashes($_REQUEST["description"]);?></textarea>
				<br>
				<span class="sub-text" style="position:relative;left:120px"><?php echo $M_DESCRIBE_DISCOUNT;?></span>
			</li>
			
			<li>
				
				
				<label for="images"><?php echo $M_IMAGES;?></label>
				
				
				<input type="file" name="images[]" id="images" />
				<br/>
				<span class="sub-text"><?php echo $M_SELECT_UP_TO;?></span>
				
			</li>
		</ol>
	</fieldset>
	
	<fieldset>
		<legend><?php echo $M_ADDITIONAL_INFORMATION;?></legend>
		<ol>
			<?php
			
			
			$link_cat = $_POST["link_category"];
			$website->ms_ew($link_cat);
			$where_string = "cat_id='".$link_cat."'";
			$str_cat_items = explode(".",$link_cat);
			
			for($i=(sizeof($str_cat_items)-1);$i>=0;$i--)
			{
				$link_cat = substr($link_cat,0,strlen($link_cat)-strlen($str_cat_items[$i])-1);
				$where_string .= " OR cat_id='".$link_cat."'";
			}
			
			
			$category_fields_table = $database->DataTable("fields","WHERE ".$where_string);
			
			while($category_fields = mysql_fetch_array($category_fields_table))
			{
				$arr_cat_fields = unserialize(stripslashes($category_fields["fields"]));
				
				if(!is_array($arr_cat_fields)) continue;
				
				$iFCounter = 0;
					
				foreach($arr_cat_fields as $arr_cat_field)
				{
				
					if(!is_array($arr_cat_field) || sizeof($arr_cat_field)!=2) continue;
					
						
					$field_name = $arr_cat_field[0];
					$field_values = $arr_cat_field[1];
					
					echo "\n<li>";
					echo "\n<label for=\"pfield-".$category_fields["cat_id"]."-".$iFCounter."\">".stripslashes($field_name)."</label>";
					
					if(trim($field_values) != "")
					{
						echo  "\n<select  name=\"pfield-".$category_fields["cat_id"]."-".$iFCounter."\">";
							
						$arrFieldValues = explode("\n", trim($field_values));
															
						if(sizeof($arrFieldValues) > 0)
						{
							foreach($arrFieldValues as $strFieldValue)
							{
								$strFieldValue = trim($strFieldValue);
								if(strstr($strFieldValue,"{"))
								{
									$strVName = substr($strFieldValue,1,strlen($strFieldValue)-2);
									echo  "<option ".(trim($$strVName)==$arrPropFields[$arrPropertyField[0]]?"selected":"").">".trim($$strVName)."</option>";
								}
								else
								{
									echo  "<option ".(isset($_REQUEST["pfield-".$category_fields["cat_id"]."-".$iFCounter])&&trim($strFieldValue)==$_REQUEST["pfield-".$category_fields["cat_id"]."-".$iFCounter]?"selected":"").">".trim($strFieldValue)."</option>";
								}		
							
							}
						}
							
							echo  "</select>";
					}
					else
					{
						echo  "\n<input value=\"".(isset($_REQUEST["pfield-".$category_fields["cat_id"]."-".$iFCounter])!=""?$_REQUEST["pfield-".$category_fields["cat_id"]."-".$iFCounter]:"")."\" type=\"text\" name=\"pfield-".$category_fields["cat_id"]."-".$iFCounter."\"/>";
					}

					echo "\n</li>";
					
					$iFCounter++;
				}
			
			}
			
			?>
		
		
			<script type="text/javascript">
				var m_all="<?php echo $M_PLEASE_SELECT;?>";
			</script>		
		
			<li >
			
				<div class="div_label">
					<?php echo $M_LOCATION;?>
					<br/>
					<a href="javascript:AddMap()"><?php echo $M_ADD_MAP;?></a>
					
				</div>
				
				<div class="div_field">
					<select name="location_menu" id="location_menu">
					<?php
					$locations_content = file_get_contents('../locations/locations.php');
					$arrLocations = explode("\n", trim($locations_content));
			
					foreach($arrLocations as $str_location)
					{
						$loc_items = explode(". ",$str_location);
						
						if(sizeof($loc_items)==2)
						{
							list($key,$value)=$loc_items;
							
							$i_level = substr_count($key, ".");
							if($i_level!=0) continue;
							echo "<option ".(isset($_REQUEST["location"])&&$_REQUEST["location"]==trim($key)?"selected":"")." value=\"".trim($key)."\">".trim($value)."</option>";
						}
					}
					?>
					
					</select>
				
				<script>
				var suggest_location_url="../include/suggest_location.php";
				</script>
				<script type="text/javascript" src="../include/multi_location_select.js"></script>
				<input type="hidden" name="location" id="location" value="">
				
				<script type="text/javascript">
				transform_select("location_menu","200px","200px");
				
				</script>
				
				
				</div>
				<div class="clear"></div>
				
				<span class="sub-text"><?php echo $M_LOCATION_EXPLANATION;?></span>
				
				
			</li>
			
			<li id="add-map" style="display:none">
				<div class="div_label">
					<?php echo $M_ADDRESS;?>
				
				</div>
				
				<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
				<script>
				  var geocoder;
				  var map;
				  function initialize() 
				  {
					geocoder = new google.maps.Geocoder();
					var latlng = new google.maps.LatLng(0, 0);
					var mapOptions = {
					  zoom: 1,
					  center: latlng,
					  mapTypeId: google.maps.MapTypeId.ROADMAP
					}
					map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
					
				}

				  function PreviewMap() 
				  {
				  
					document.getElementById("map-canvas").style.display="block";
				  
					var address = "";
					
					
					if(document.getElementById("selected_dropdown").innerText)
					{
						address +=
						document.getElementById("selected_dropdown").innerText.replace(" :",", ")
						+", ";
					}
					
					
					address += document.getElementById('address').value;
					geocoder.geocode( { 'address': address}, function(results, status) {
					  if (status == google.maps.GeocoderStatus.OK) {
						map.setCenter(results[0].geometry.location);
						
						map.setZoom(13);
						
						var marker = new google.maps.Marker({
							map: map,
							position: results[0].geometry.location
						});
						
						document.getElementById("latitude").value=
						results[0].geometry.location.lat();
						
						document.getElementById("longitude").value=
						results[0].geometry.location.lng();
						
						
					  } else {
						alert('Google Maps can\'t find this address: ' + status);
					  }
					});
					google.maps.event.trigger(map, 'resize');
					map.checkResize();
				  }
				  
				  window.onload=initialize;
				</script>
				<script>
				function AddMap()
				{
					if(document.getElementById("add-map").style.display=="none")
					{
						document.getElementById("add-map").style.display="block"
					}
					else
					{
						document.getElementById("add-map").style.display="none"
					}
				}
				
				function UserKeyUp()
				{
					if(document.getElementById("address").value=="")
					{
						document.getElementById("preview-map").style.display="none";
					}
					else
					{
						document.getElementById("preview-map").style.display="block";
					}
				}
				</script>
				
				<div class="div_field">
					<input name="address" value="<?php if(isset($_REQUEST["address"])) echo $_REQUEST["address"];?>" onkeyup="javascript:UserKeyUp()" id="address" type="text" placeholder="<?php echo $M_PLEASE_ENTER_ADDRESS;?>"/>
				</div>
				<br/>
				
				<?php echo $M_OR_ENTER;?> 
				&nbsp;
				<?php echo $M_LATITUDE;?>:
				<input type="text" name="latitude" id="latitude"  style="width:60px" value="<?php if(isset($_REQUEST["latitude"])) echo $_REQUEST["latitude"];?>">
				<span style="font-size:10px">e.g. 40.758224</span>
				&nbsp;&nbsp;
				<?php echo $M_LONGITUDE;?>:
				<input type="text" name="longitude" id="longitude" style="width:60px" value="<?php if(isset($_REQUEST["longitude"])) echo $_REQUEST["longitude"];?>"> 
				<span style="font-size:10px">e.g. -73.917404</span>
					
				
				<br/>
				<span id="preview-map" style="display:none"><a href="javascript:PreviewMap()"><?php echo $M_PREVIEW_SAVE;?></a></span>
				<div class="clear"></div>
			
				<div id="map-canvas" style="width: 500px; height: 300px;display:none"></div>
	
			</li>
		</ol>
	</fieldset>
	
	
	<fieldset>
		<legend><?php echo $M_CHOOSE_PACKAGE;?></legend>
		<ol id="ad-packages">
		<?php
		$listing_packages = $database->DataTable("packages","WHERE active=1 ORDER BY link_category,price");
		$b_first_package = true;
		$p_link_category = "-";
		$close_div = false;
		
		while($listing_package = mysql_fetch_array($listing_packages))
		{
			if($p_link_category != $listing_package["link_category"])
			{
				if(!$b_first_package) echo '</div>';
				$p_link_category = $listing_package["link_category"];
				echo '<div '.($p_link_category==""?'':'style="display:none"').' id="packages-'.($p_link_category==""?"0":$p_link_category).'">';
				$close_div = true;
			}
		?>
		
			<li style="line-height:1.0em">
				<br/>
				<input name="package" value="<?php echo $listing_package["id"];?>" <?php if(isset($_REQUEST["package"])&&$_REQUEST["package"]==$listing_package["id"]) echo "checked";?> <?php if($b_first_package) echo "checked";?> class="form-radio" type="radio"/>
				<b><?php echo stripslashes($listing_package["name"]);?></b>
				,&nbsp;   
				<?php echo $M_PRICE;?>: 
				<b>
				<?php 
				if($listing_package["price"]==0)
				{
					echo $M_FREE."!";
				}
				else
				{
					echo $website->GetParam("WEBSITE_CURRENCY").number_format($listing_package["price"],2,'.',',');
				}
				?>
				</b>
			<br/><br/>
			<span class="sub-text">
			<?php echo stripslashes($listing_package["description"]);?>
			</span>
			<br/><br/>
			</li>
		
		<?php
			$b_first_package = false;
		}
		
		if($close_div)
		{
			echo '</div>';
			$close_div = false;
		}
		?>
			
			
		</ol>
	</fieldset>
	
	
	(*) <?php echo $M_REQUIRED_FIELDS;?>
	<fieldset>
		<button type="submit"><?php echo $M_SUBMIT;?></button>
	</fieldset>
</form>
</div>
<?php
}
?>
<!--[if !IE]>
<script>
document.getElementById('title').setCustomValidity('<?php echo $M_PLEASE_ENTER_TITLE;?>');
document.getElementById('url').setCustomValidity('<?php echo $M_PLEASE_ENTER_URL;?>');
document.getElementById('category').setCustomValidity('<?php echo $M_PLEASE_SELECT_CATEGORY;?>');
document.getElementById('name').setCustomValidity('<?php echo $M_PLEASE_ENTER_NAME;?>');
document.getElementById('email').setCustomValidity('<?php echo $M_PLEASE_ENTER_EMAIL;?>');
document.getElementById('code').setCustomValidity('<?php echo $M_PLEASE_ENTER_CODE;?>');
</script>
<![endif]-->

