<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
$id=$_REQUEST["id"];
$website->ms_i($id);

?>
<div class="fright">
<?php
 echo LinkTile
 (
	"ads",
	"list",
	$M_GO_BACK,
	"",
	"red"
 );
 ?>
</div>
<div class="clear"></div>
 
<?php

$arrAd=$database->DataArray("listings","id=".$id);	


if(isset($_POST["ProceedSend"]))
{
	
	$path="../";
	///images processing
	$str_images_list = "";
		
	include("../include/images_processing.php");
	///end images processing
	
	if($arrAd["images"] != "")
	{
		$str_images_list = $arrAd["images"].",".$str_images_list;
	}
	
	if(trim($_POST["dele_images"]) != "")
	{
		$dele_ids = explode(",",trim($_POST["dele_images"]));
		
		foreach($dele_ids as $dele_id)
		{
			if(trim($dele_id)=="") continue;
			$website->ms_i($dele_id);
			
			if(file_exists("../thumbnails/".$dele_id.".jpg"))
			{
				unlink("../thumbnails/".$dele_id.".jpg");
			}
			
			if(file_exists("../uploaded_images/".$dele_id.".jpg"))
			{
				unlink("../uploaded_images/".$dele_id.".jpg");
			}
			
			$str_images_list=str_replace($dele_id.",","",$str_images_list);
			$str_images_list=str_replace($dele_id,"",$str_images_list);
		}
	}
	
	$database->SQLUpdate_SingleValue
	(
		"listings",
		"id",
		$id,
		"images",
		$str_images_list
	
	);
	$arrAd=$database->DataArray("listings","id=".$id);	

}


$MAX_NUMBER_IMAGES=10;


if($arrAd["images"] != "")
{

	$arrImgs = explode(",", $arrAd["images"]);
	echo "<span class=\"medium-font\">".$M_IMAGES."</span>";
}
else
{
	$arrImgs = array();
	echo "<span class=\"medium-font\">".$M_CURRENTLY_NO_PICS."</span><br/><br/>";
}
?>	

<script>
function Dele(x)
{
	document.getElementById("im"+x).innerHTML = "<img src=\"../images/no_pic.gif\" width=\"200\" />";
	
	document.getElementById("dele_images").value = document.getElementById("dele_images").value + x +",";
	
	
}
</script>

<form action="index.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="ProceedSend" value="1"/>
<input type="hidden" name="dele_images" id="dele_images" >
<input type="hidden" name="category" value="ads">
<input type="hidden" name="action" value="images">
<input type="hidden" name="id" value="<?php echo $id;?>">

<?php
$iPicCounter = 0;


for($i=0;$i<sizeof($arrImgs);$i++)
{
?>


			<?php
			if(isset($arrImgs[$i]) && $arrImgs[$i]!="")	
			{
			?>
				<div style="float:left;width:220px;">
				<br>
				<a href="javascript:Dele('<?php echo $arrImgs[$i];?>')"><img src="images/cancel.gif" alt="<?php echo $EFFACER;?>" width="21" height="20" border="0"></a>
				<br>		
				<span id="im<?php echo $arrImgs[$i];?>">
				
				<img src="../uploaded_images/<?php echo $arrImgs[$i];?>.jpg" alt="" width="200"/>
				
				</span>
				</div>
			<?php
			}
			?>		
		<span style="display:none">
			<input type=file name="userfile<?php echo $i;?>">
		</span>
			
	

<?php

	$iPicCounter++;
	
}
?>
<div class="clear"></div>	

<br/><br/>
<span class="medium-font"><?php echo $M_UPLOAD_MORE;?></span>
<input type="file" name="images[]" id="images" />
<br/><br/>
<input type="submit" value=" <?php echo $M_SAVE;?> " class="adminButton"/>

</form>