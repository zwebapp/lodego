<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php

$id=$_REQUEST["id"];

$website->ms_i($id);

if(isset($_POST["ProceedSend"]))
{
	$website->ms_i($_POST["package"]);
		
	$selected_package=$database->DataArray("packages","id=".$_REQUEST["package"]);
	
$database->SQLUpdate
		(
			"listings",
			array
			(
			
				"package",
				"status",
				"expires"
			),
			array
			(
				$_POST["package"],
				$website->GetParam("AUTO_APPROVE"),
				time()+$selected_package["days"]*24*3600
			),
			"id=".$id
		);
		
		?>
		
		<div class="clear"></div>
<br/>

		<?php
		
		if($selected_package["price"]==0)
		{
	
			echo "<h4>".nl2br(stripslashes($website->GetParam("SUCCESS_MESSAGE_FREE")))."</h4>";
		
		}
		else
		{
		?>
		
			<?php
			echo "<h4>".nl2br(stripslashes($website->GetParam("SUCCESS_MESSAGE_PAID")))."</h4>";
			?>
			<br/><br/>
			<?php echo $M_PLEASE_SELECT_PAYMENT;?>
			
			<?php
			if(trim($website->GetParam("PAYPAL_ID")) !="")
			{
			?>	<br/><br/>
				<form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
				<input type="hidden" name="cmd" value="_xclick">
				<input type="hidden" name="business" value="<?php echo $website->GetParam("PAYPAL_ID");?>">
				<input type="hidden" name="currency_code" value="<?php echo $website->GetParam("CURRENCY_CODE");?>">
				<input type="hidden" name="item_name" value="Payment for ad id#<?php echo $i_submit_id;?> on <?php echo $DOMAIN_NAME;?>">
				<input type="hidden" name="item_number" value="<?php echo $i_submit_id;?>">
				<input type="hidden" name="amount" value="<?php echo number_format($selected_package["price"], 2, '.', '');?>">
				<input type="image"  src="../images/paypal.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
				</form>
			<?php
			}
			?>
			
			<?php
			if(trim($website->GetParam("2CHECKOUT_ID")) !="")
			{
			?>	<br/><br/>
			
				<form target="_blank" action="https://www.2checkout.com/cgi-bin/sbuyers/cartpurchase.2c" method="post">
				<input type="hidden" name="sid" value="<?php echo trim($website->GetParam("2CHECKOUT_ID"));?>"> 
				<input type="hidden" name="cart_order_id" value="<?php echo $i_submit_id;?>"> 
				<input type="hidden" name="total" value="<?php echo number_format($selected_package["price"], 2, '.', '');?>">
				<input type="hidden" name="skip_landing" value="1"> 
				<input type="image" src="../images/2checkout.gif" alt="" border="0">
				</form>
				
			<?php
			}
			?>
			
		<?php
		
		}
		
		?>
		
		
		<br/><br/>
		
		<?php		
				
}
else
{
?>
<br/>
<h3><?php echo $M_RENEW_EXPIRED;?></h3>
<br/>
<div style="width:700px">
<form id="main" action="index.php" method="post"  enctype="multipart/form-data">

<input type="hidden" name="category" value="<?php echo $_REQUEST["category"];?>"/>
<input type="hidden" name="folder" value="<?php echo $_REQUEST["folder"];?>"/>
<input type="hidden" name="page" value="<?php echo $_REQUEST["page"];?>"/>
<input type="hidden" name="id" value="<?php echo $id;?>"/>
<input type="hidden" name="ProceedSend" value="1"/>
	
	
	<fieldset>
		<legend><?php echo $M_CHOOSE_PACKAGE;?></legend>
		<ol id="ad-packages">
		<?php
		$listing_packages = $database->DataTable("packages","WHERE active=1 ORDER BY link_category,price");
		$b_first_package = true;
		$p_link_category = "-";
		$close_div = false;
		
		while($listing_package = mysql_fetch_array($listing_packages))
		{
			if($p_link_category != $listing_package["link_category"])
			{
				if(!$b_first_package) echo '</div>';
				$p_link_category = $listing_package["link_category"];
				echo '<div '.($p_link_category==""?'':'style="display:none"').' id="packages-'.($p_link_category==""?"0":$p_link_category).'">';
				$close_div = true;
			}
		?>
		
			<li style="line-height:1.0em">
				<br/>
				<input name="package" value="<?php echo $listing_package["id"];?>" <?php if(isset($_REQUEST["package"])&&$_REQUEST["package"]==$listing_package["id"]) echo "checked";?> <?php if($b_first_package) echo "checked";?> class="form-radio" type="radio"/>
				<b><?php echo stripslashes($listing_package["name"]);?></b>
				,&nbsp;   
				<?php echo $M_PRICE;?>: 
				<b>
				<?php 
				if($listing_package["price"]==0)
				{
					echo $M_FREE."!";
				}
				else
				{
					echo $website->GetParam("WEBSITE_CURRENCY").number_format($listing_package["price"],2,'.',',');
				}
				?>
				</b>
			<br/><br/>
			<span class="sub-text">
			<?php echo stripslashes($listing_package["description"]);?>
			</span>
			<br/><br/>
			</li>
		
		<?php
			$b_first_package = false;
		}
		
		if($close_div)
		{
			echo '</div>';
			$close_div = false;
		}
		?>
			
			
		</ol>
	</fieldset>
	
	
	(*) <?php echo $M_REQUIRED_FIELDS;?>
	<fieldset>
		<button type="submit"><?php echo $M_SUBMIT;?></button>
	</fieldset>
</form>
</div>
<?php
}
?>