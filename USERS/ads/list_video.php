<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
ms_i($id);
$arrAd=$database->DataArray("re_ads","id=".$id);

if($arrAd["username"]!=$AuthUserName) die("");
?>

<script>
function DeleteVideo()
{
	if(confirm("Are you sure you want to delete the current video?"))
	{
		document.location.href="index.php?category=ads&folder=list&page=video&process_delete=1&id=<?php echo $id;?>";
	}
}

</script>
<?php

$file_types = Array 
(
	array("video/x-ms-wmv","wmv"),
	array("video/mpeg","mpg"),
	array("video/x-msvideo","avi"),
	array("video/avi","avi")
);	

function GetVideoFileType($file_type)
{

	global $file_types;

	foreach($file_types as $c_file_type)
	{
		if($c_file_type[0] == $file_type)
		{
			return $c_file_type[1];
		}
	}
	return "";
}



if(isset($process_form))	
{
	$s_file_id=0;
	$ir=rand(200,100000000);
	$file_name = $_FILES["video_file"]["name"];
  	$file_size = $_FILES["video_file"]["size"];
  	$file_type = $_FILES["video_file"]["type"];
  	$file_extension = GetVideoFileType($file_type);
	
	 if($file_extension == "")
  	{
     	echo "<b><font color=red>This file format is not supported!</font></b>";
  	}
  	else
  	{
  		 $uploadedFile = "../uploaded_videos/" . $ir.".".$file_extension;
	
		if($file_size > $MAX_VIDEO_SIZE)
		{
	    	echo "<b><font color=red>".$FILE_MAX_SIZE_EXCEEDED."</font></b>";
		}
		else 
		if (move_uploaded_file($_FILES["video_file"]['tmp_name'], $uploadedFile))
		{
			$database->SQLUpdate_SingleValue("re_ads","id","'".$id."'","video_id",$ir);
			
			$s_file_id=$ir;
		}
		else
		{
	    	  echo "Error while uploading your file!";
    	}
		
	}
	
}
		
if(isset($process_update))
{
		foreach($file_types as $c_file_type)
		{
				if(file_exists("../uploaded_videos/".$arrAd["video_id"].".".$c_file_type[1]))
				{
					unlink("../uploaded_videos/".$arrAd["video_id"].".".$c_file_type[1]);
				}
		}
}


if(isset($process_delete))
{
		foreach($file_types as $c_file_type)
		{	
				if(file_exists("../uploaded_videos/".$arrAd["video_id"].".".$c_file_type[1]))
				{
			
					unlink("../uploaded_videos/".$arrAd["video_id"].".".$c_file_type[1]);
				}
		}
		$database->SQLUpdate_SingleValue("re_ads","id","'".$id."'","video_id","0");
		$arrAd["video_id"]=0;	
}
?>


<table summary="" border="0" width="95%">
	<tr>
		<td align="right">
		<?php
		if($arrAd["video_id"]>1)
		{
		?>
			<table summary="" border="0">
			  	<tr>
			  		<td><img src="images/link_arrow.gif" width="16" height="16" alt="" border="0"></td>
			  		<td><a href="javascript:DeleteVideo()"><b>CLICK HERE TO DELETE THE VIDEO</b></a></td>
			  	</tr>
			  </table>
		<?php
		}
		?>
		</td>
	</tr>
	<tr>
		<td>

		
			  
		
<?php


if($arrAd["video_id"]>1)
{
?>
<br>
<i>The current video:</i>
<br><center>	  
<?php
	include("../include/video.php");
	$file_types = Array 
	(
		array("video/x-ms-wmv","wmv"),
		array("video/mpeg","mpg"),
		array("video/x-msvideo","avi"),
		array("video/avi","avi")
	);	

	$video_file="";
	$video_file_type="";

	foreach($file_types as $c_file_type)
	{
			if(file_exists("../uploaded_videos/".$arrAd["video_id"].".".$c_file_type[1]))
			{
					$video_file="../uploaded_videos/".$arrAd["video_id"].".".$c_file_type[1];
			}
	}


	if($video_file!="")
	{
 		echo "<script>ShowVideo('".$video_file_type."','".$video_file."',320,240,'center');</script>";
	}
	
}	
?>

</center>
<br><br>
<table summary="" border="0" width="95%">
	<tr>
		<td>
		
		  <i>Update the video</i>	
			<br><br><br>
			
			
			<form action="index.php" method="post" enctype="multipart/form-data">
			<input type="hidden" name="process_form" value="1">
			<input type="hidden" name="process_update" value="1">
			<input type="hidden" name="category" value="<?php echo $category;?>">
			<input type="hidden" name="action" value="<?php echo $action;?>">
			<input type="hidden" name="id" value="<?php echo $id;?>">
			Video File: <input type="file" name="video_file" size="20">
			<br><br><br>
			<input type="submit" value=" Save " class="adminButton">
			
			</form>
			
			
		
		<?php
		generateBackLink("list");
		?>
		
		
		</td>
	</tr>
</table>




	