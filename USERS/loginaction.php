<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
ob_start();
include("../config.php");
if(!$DEBUG_MODE) error_reporting(0);
define("LOGIN_PAGE", "../index.php");
define("SUCCESS_PAGE", "index.php");
define("LOGIN_EXPIRE_AFTER", 3600*24);
$Email = $_POST["Email"];
$Password = $_POST["Password"];
include("../include/Database.class.php");
$database = new Database();
$database->Connect($DBHost, $DBUser,$DBPass );
$database->SelectDB($DBName);

include("../config.php");

if($Email == "" || $Password == "") 
{
	die("<script>document.location.href='".LOGIN_PAGE."?error=no';</script>");
}
else
{

	$strSelect="select * from ".$DBprefix."users where username='".mysql_real_escape_string($Email)."'";
	
	$LoginResult= $database->Query($strSelect);
	$LoginInfo = mysql_fetch_array($LoginResult);

	if(mysql_num_rows($LoginResult)==1 && $LoginInfo["password"] == $Password) 
	{

		$strCookie=$LoginInfo["username"]."~".md5($LoginInfo["password"])."~".(time()+LOGIN_EXPIRE_AFTER);

		setcookie("AuthU",$strCookie, time() + 24*3600, "/");

		$database->Query
		("
			INSERT INTO ".$DBprefix."login_log(username,ip,date,action)
			VALUES('".$LoginInfo["username"]."','".$_SERVER["REMOTE_ADDR"]."','".time()."','login')
		");

		die("<script>document.location.href='".SUCCESS_PAGE."';</script>");
		
	}
	else 
	{

		$database->Query
		("
			INSERT INTO ".$DBprefix."login_log(username,ip,date,action,cookie)
			VALUES('".$Email."','".$_SERVER["REMOTE_ADDR"]."','".time()."','error','')
		");

		die("<script>document.location.href='".LOGIN_PAGE."?error=login';</script>");
	}

}

ob_end_flush();
?>