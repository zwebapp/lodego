<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
function AddNewForm
	(
		$arrTexts,
		$arrNames,
		$arrTypes,
		$strSubmitText,
		$strTable,
		$strSuccessMessage,
		$ajax_call = false,
		$arrExamples = array(),
		$jsValidation ="",
		$arrOtherValues = array()
	)
	{
	$ajax_call = false;
	global $_REQUEST,$lang,$database,$MessageTDLength;
	global $strSpecialHiddenFieldsToAdd;
	
	
	$i_inserted_id = null;
	
	if(isset($_REQUEST["SpecialProcessAddForm"]))
	{

		$arrValues=array();

		for($i=0;$i<sizeof($arrNames);$i++)
		{

			$strName=$arrNames[$i];

			if($arrTypes[$i]=="file")
			{
				$iFileId=insert_image($strName);
				array_push($arrValues,$iFileId);
			
			}
			else
			{

				if(isset($_REQUEST[$strName]))
				{
					$tempValue = $_REQUEST[$strName];
				}
				else
				if(isset($_REQUEST["post_".$strName]))
				{
					$tempValue = $_REQUEST["post_".$strName];
				}

				if($strName == "password")
				{
					$tempValue = md5($tempValue);
				}
				
				array_push($arrValues,$tempValue);
			}
		}

		if(isset($arrOtherValues))
		{
			foreach($arrOtherValues as $arrOtherValue)
			{
				array_push($arrNames, $arrOtherValue[0]);
				array_push($arrValues, $arrOtherValue[1]);
			}
		}
		
		if(isset($_REQUEST["arrNames2"]))
		{
			
			for($i=0;$i<sizeof($_REQUEST["arrNames2"]);$i++)
			{
				array_push($arrNames,$_REQUEST["arrNames2"][$i]);
				array_push($arrValues,$_REQUEST["arrValues2"][$i]);
			}
		}
		
		
		if(!isset($DoNotInsert))
		{
			$i_inserted_id = $database->SQLInsert($strTable,$arrNames,$arrValues);
		}
		
	
		echo "<h3>";
		
		if(!isset($DoNotInsert)  && $i_inserted_id==0)
		{
			echo "Error while inserting the new data.</b>";
		}
		else
		{
			echo $strSuccessMessage;
		}
		
		echo "</h3>";
		
	}
	
	if(isset($_REQUEST["HideFormAfterSumit"]) && isset($_REQUEST["SpecialProcessAddForm"]))
	{
	
	}
	else
	{
		echo "<table>";

		echo "<form id=\"add-form\" ".($ajax_call?"target=\"ajax-ifr\"":"")." ".(isset($jsValidation)&&$jsValidation!=""?"onsubmit='return $jsValidation(this)'":"")." action=\"".($ajax_call?"admin_page":"index").".php\" method=\"post\" enctype=\"multipart/form-data\">";
		
		if(isset($ajax_call)&&$ajax_call)
		{
			echo "<input type=\"hidden\" name=\"ajax_call\" value=\"1\"/>";
		}
				
		if(isset($_REQUEST["folder"]))
		{
			echo "<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST["category"]."\"/>";
			echo "<input type=\"hidden\" name=\"page\" value=\"".$_REQUEST["page"]."\"/>";
			echo "<input type=\"hidden\" name=\"folder\" value=\"".$_REQUEST["folder"]."\"/>";
		}
		else
		{
			echo "<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST["category"]."\"/>";
			echo "<input type=\"hidden\" name=\"action\" value=\"".$_REQUEST["action"]."\"/>";
		}

		echo "<input type=\"hidden\" name=\"SpecialProcessAddForm\" value=\"1\"/>";
			
		if(isset($_REQUEST["FieldsToAdd"]))
		{
			echo $_REQUEST["FieldsToAdd"];
		}

		for($i=0;$i<sizeof($arrTexts);$i++)
		{
			echo "<tr height=24>";

			echo "
				<td valign=\"top\" width=\"".(isset($_REQUEST["message-column-width"])?$_REQUEST["message-column-width"]:"80")."\">
				".$arrTexts[$i]."
				</td>
			";

			echo "<td valign=\"top\">";
			
			if(strstr($arrTypes[$i],"combobox_table")){
				
				list($strType,$strTableName,$strFieldValue,$strFieldName)=explode("~",$arrTypes[$i]);
				
				$oTable=$database->DataTable($strTableName,"");
								
				echo "<select id=\"".$arrNames[$i]."\" name=\"".$arrNames[$i]."\">";
				
				while($oRow=mysql_fetch_array($oTable)){
						echo "<option value=\"".$oRow[$strFieldValue]."\">".$oRow[$strFieldName]."</option>";
				}
				
				echo "</select>";
			}
			else
			if(strstr($arrTypes[$i],"file")){

				$strSize=30;
					echo "<input type=\"file\" name=\"".$arrNames[$i]."\" size=$strSize>";
			}
			else
			if(strstr($arrTypes[$i],"textbox"))
			{

				list($strType,$strSize)=explode("_",$arrTypes[$i]);
					echo "<input type=\"text\" id=\"".$arrNames[$i]."\" name=\"".$arrNames[$i]."\" size=\"".$strSize."\"/>";
					if(isset($arrExamples[$i])) echo "<br/><span class=\"small-font\">".$arrExamples[$i]."</span>";
			}
			else
			if(strstr($arrTypes[$i],"password")){

				list($strType,$strSize)=explode("_",$arrTypes[$i]);
					echo "<input type=password id=\"".$arrNames[$i]."\" name=\"".$arrNames[$i]."\" size=$strSize />";
			}
			else
			if(strstr($arrTypes[$i],"textarea"))
			{
				list($strType,$strCols,$strRows)=explode("_",$arrTypes[$i]);

				echo "<input type=\"hidden\" id=\"post_".$arrNames[$i]."\" name=\"post_".$arrNames[$i]."\"/>
				<textarea id=\"".$arrNames[$i]."\" name=\"".$arrNames[$i]."\" cols=\"".$strCols."\" rows=\"".$strRows."\"></textarea>";
			}
			else
			if(strstr($arrTypes[$i],"javascript~combobox")){

				echo "<select ".(isset($_REQUEST["select-width"])?"style=\"width:".$_REQUEST["select-width"]."px\"":"")." id=\"".$arrNames[$i]."\" name=\"".$arrNames[$i]."\" onChange=javascript:SelectChanged(this)>";

				foreach(explode("_",$arrTypes[$i]) as $strOption){

					if($strOption=="javascript~combobox"){
						continue;
					}

					echo "<option>".str_replace("~"," ",$strOption)."</option>";
				}

			}
			
			else
			if(strstr($arrTypes[$i],"combobox_link_categories"))
			{

				echo "<select id=\"link_category\" name=\"link_category\">";

				echo "<option value=\"\">Please select</option>";
				
				if(file_exists('../categories/categories_'.strtolower($lang).'.php'))
				{
					$categories_content = file_get_contents('../categories/categories_'.strtolower($lang).'.php');
				}
				else
				{
					$categories_content = file_get_contents('../categories/categories_en.php');
				}

				$arrCategories = explode("\n", trim($categories_content));

				$categories=array();
				
				foreach($arrCategories as $str_category)
				{
					list($key,$value)=explode(". ",$str_category);
					$categories[trim($key)]=trim($value);
					
					$i_level = substr_count($key, ".");
					
					echo "<option  value=\"".trim($key)."\">".str_repeat("|&nbsp;&nbsp;&nbsp;&nbsp;", $i_level)."|__".trim($value)."</option>";
				}
				
				echo "</select>";

			}
			else
			if(strstr($arrTypes[$i],"combobox")){

					echo "<select ".(isset($_REQUEST["select-width"])?"style=\"width:".$_REQUEST["select-width"]."px\"":"")." id=\"".$arrNames[$i]."\" name=\"".$arrNames[$i]."\">";

				foreach(explode("_",$arrTypes[$i]) as $strOption){

					if($strOption=="combobox")
					{
						continue;
					}

					$arrOptions = explode("^", $strOption);
					
					if(sizeof($arrOptions) > 1)
					{
						echo "<option value=\"".$arrOptions[1]."\">".str_replace("~"," ",$arrOptions[0])."</option>";
					}
					else
					{
						echo "<option >".str_replace("~"," ",$strOption)."</option>";
					}

				}

				echo "</select>";

			}
			

			echo "</td>";

			echo "</tr>";
		}

		echo "</table>";

		echo "<br>
		
		<input ".($ajax_call?"onmousedown=\"javascript:ShowLoading()\"":"")." id=\"submit-button\" type=\"".($ajax_call?"button":"submit")."\" value=\"".$strSubmitText."\" class=\"adminButton lfloat\"/>
		</form>";

	}
	
	return $i_inserted_id;
}


function RenderTable
	(
		$strTable,
		$oCol,
		$oNames,
		$iWidth,
		$sqlClause,
		$strCheckColumnName,
		$strCheckValue,
		$strFormAction,
		$ajax_call = false,
		$PageSize = 20,
		$IS_RADIO = false,
		$RADIO_VALUE = -1,
		$ORDER_QUERY = ""
	)
{
	$ajax_call = false;
	global $database,$website,$_REQUEST,$_GET,$_POST;
	global $arrHighlightIds;
	global $strHighlightIdName;
	
	global $DBprefix, $action,$category;
	global $URLToAdd;
	global $strExplanationTitle,$PageNumber,$arrTDSizes,$iRTables;
	
	global $SEARCH,$TOTAL_NUMBER_OF_RESULTS,$QUERY_EXECUTED_FOR,$PAGE_SIZE,$customFormEnd,$order,$order_type,$QUERY_TO_EXECUTE;
		
	global $DEBUG_MODE,$SEARCH_IN;
	
	$textSearch = "";
	
	if($DEBUG_MODE)
	{
		$ajax_call = false;
	}
	
	if(!isset($_REQUEST["PageNumber"]))
	{
		$PageNumber=1;
	}
	else
	{
		$PageNumber=intval($_REQUEST["PageNumber"]);
	}
	
	if(isset($_REQUEST["order"]))
	{
		$order = $_REQUEST["order"];
		$order_type = $_REQUEST["order_type"];
		$arrSQLClause = explode("ORDER BY",$sqlClause);
		$strQuery="SELECT * FROM ".$DBprefix.$strTable." ".$arrSQLClause[0]." ORDER BY ".$order." ".$order_type;
	}
	else
	if(isset($_REQUEST["textSearch"]))
	{
		$textSearch = $_REQUEST["textSearch"];
		$website->ms_ew($textSearch);
		$website->ms_ew($_REQUEST["comboSearch"]);
		
		if(trim($sqlClause)!="")
		{
			$strQuery="SELECT * FROM ".$DBprefix.$strTable." ".$sqlClause." AND ".$_REQUEST["comboSearch"]." LIKE '%".$textSearch."%'  ".(isset($ORDER_QUERY)&&$ORDER_QUERY!=""?$ORDER_QUERY:"");
		}
		else
		{
			$strQuery="SELECT * FROM ".$DBprefix.$strTable." WHERE ".$_REQUEST["comboSearch"]." LIKE '%".$textSearch."%'  ".(isset($ORDER_QUERY)&&$ORDER_QUERY!=""?$ORDER_QUERY:"");
		}
	}
	else
	{
		$strQuery="SELECT * FROM ".$DBprefix.$strTable." ".$sqlClause." ".(isset($ORDER_QUERY)&&$ORDER_QUERY!=""?$ORDER_QUERY:"");
	}
	
	if($QUERY_TO_EXECUTE)
	{
		$strQuery=$QUERY_TO_EXECUTE;
	}
	
	$iTotalResults=$database->SQLCount_Query($strQuery);

	
	$oDataTable = $database->Query($strQuery." LIMIT ".(($PageNumber-1)*$PageSize).",".($PageSize)."");
		
	$table_name_p = explode(",", $strTable);
	
	$mysql_fields = $database->GetFieldsInTable($table_name_p[0]);
			
	$iRTables++;
		
	if(sizeof(array_intersect( $oCol,$mysql_fields)) > 0)
	{
	
		echo "<div style=\"float:right\">";
		echo "
		<form ".(isset($ajax_call)&&$ajax_call?"target=\"ajax-ifr\" onsubmit=\"LoadingIcon()\"":"")." action=\"".(isset($ajax_call)&&$ajax_call?"admin_page.php":"index.php")."\" method=\"post\">
		
		
		<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST["category"]."\"/>";
		
		if($ajax_call)
		{
			echo "<input type=\"hidden\" name=\"ajax_load\" value=\"1\"/>";
		}
		
		if(isset($_REQUEST["action"]))
		{
			echo "<input type=\"hidden\" name=\"action\" value=\"".$_REQUEST["action"]."\"/>";
		}
		if(isset($_REQUEST["folder"]))
		{
			echo "<input type=\"hidden\" name=\"folder\" value=\"".$_REQUEST["folder"]."\"/>";
		}
		if(isset($_REQUEST["page"]))
		{
			echo "<input type=\"hidden\" name=\"page\" value=\"".$_REQUEST["page"]."\"/>";
		}
		
		
		
		echo "
		".$SEARCH_IN." <select name=\"comboSearch\">";
		
		
		for($k=0;$k<sizeof($oNames);$k++)
		{
			if(in_array($oCol[$k], $mysql_fields)) 
			{
				if($oCol[$k]=="image_id") continue;
			
				echo "<option value=\"".$oCol[$k]."\" ".(isset($_REQUEST["comboSearch"])&&$_REQUEST["comboSearch"]==$oCol[$k]?"selected":"").">".$oNames[$k]."</option>";
			}
			
		}
		
		echo "</select> 
		&nbsp; 
		<input value=\"".(isset($_REQUEST["textSearch"])?$_REQUEST["textSearch"]:"")."\" type=\"text\" name=\"textSearch\"/> 
		<input type=\"submit\"  value=\" Search \">
		</form>
		</div>
		<div class=\"clear\"></div>
	
		";
	}
	
	echo "
	<script>
	function SubmitForm()
	{
		
		document.getElementById('table-form').submit();
	}
	</script>
	
	<script>
		function CheckAll(source) 
		{
		  checkboxes = document.getElementsByName('CheckList[]');
		  for(var i=0, n=checkboxes.length;i<n;i++) {
			checkboxes[i].checked = source.checked;
			}
		}
	</script>
	";
	
	
	echo "<form ".(isset($ajax_call)&&$ajax_call?"target=\"ajax-ifr\" onsubmit=\"LoadingIcon()\"":"")." action=\"".(isset($ajax_call)&&$ajax_call?"admin_page.php":"index.php")."\" id=\"table-form\" method=\"post\" enctype=\"multipart/form-data\">";
	echo "<input type=\"hidden\" name=\"FormSubmitted\" value=\"1\"/>";
	if(isset($ajax_call)&&$ajax_call)
	{
		echo "<input type=\"hidden\" name=\"ajax_load\" value=\"1\"/>";
	}
			
	if(isset($_REQUEST["folder"]))
	{
		echo "<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST["category"]."\"/>";
		echo "<input type=\"hidden\" name=\"page\" value=\"".$_REQUEST["page"]."\"/>";
		echo "<input type=\"hidden\" name=\"folder\" value=\"".$_REQUEST["folder"]."\"/>";
	}
	else
	{
		echo "<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST["category"]."\"/>";
		echo "<input type=\"hidden\" name=\"action\" value=\"".$_REQUEST["action"]."\"/>";
	}
	
	if(isset($_REQUEST["hidden_fields"]))
	{
		foreach($_REQUEST["hidden_fields"] as $key=>$value)
		{
			echo "\n<input type=\"hidden\" name=\"".$key."\" value=\"".$value."\"/>";
		}
	}
	
	echo "<table cellpadding=\"3\" cellspacing=\"0\" width=\"100%\" style='border-color:#eeeeee;border-width:1px 1px 1px 1px;border-style:solid'>";
	
	echo "<tr class=\"table-tr\" nowrap>";
	
	
	$iTDWidth=0;
	$iDefaultTDWidth=0;
	
	$iTDTotalNumber=sizeof($oCol);
	
	
	if(!isset($arrTDSizes))
	{
		
		$iTDWidth=round(($iWidth-30)/$iTDTotalNumber);
		$arrTDSizes=array_fill(0, sizeof($oCol), $iTDWidth);
		
	}
	else
	{
		$iOccupied=0;
	
		$iTDHaveValues=0;	
				
		foreach($arrTDSizes as $strTDSize){
		
			if($strTDSize!="*"){
				$iOccupied+=intval($strTDSize);		
				$iTDHaveValues++;
			}
			
		}	
		
		if(($iTDTotalNumber-$iTDHaveValues)==0){
		
			$iDefaultTDWidth=round((($iWidth-30)-$iOccupied)/($iTDHaveValues));	
		
		}
		else{
			$iDefaultTDWidth=round((($iWidth-30)-$iOccupied)/($iTDTotalNumber-$iTDHaveValues));	
		}
		
		for($k=0;$k<sizeof($arrTDSizes);$k++){
		
				if($arrTDSizes[$k]!="*"){
					$arrTDSizes[$k]=intval($arrTDSizes[$k]);									
				}
				else{
					$arrTDSizes[$k]=$iDefaultTDWidth;
				}	
							
		}
			
	}

	if(strpos($strCheckColumnName,"#") !== false)
	{
	
	}
	else
	if(trim($strCheckColumnName)!="")
	{
		echo "<td  width=\"40\" class=\"header-td\">
		<input type=\"checkbox\" title=\"".$strCheckColumnName." All\" onClick=\"CheckAll(this)\" />
		</td>";
	}

	$iTDHeaderCounter=0;
	
	
	
	
	if(!isset($order_type)){
		$order_type="desc";
		$strImgName="";
	}
	else
	if($order_type=="asc")
	{
		$order_type="desc";
		$strImgName="up.png";
	}
	else{
		$order_type="asc";
		$strImgName="down.png";
	}
	
	$arrFields=$database->GetFieldsInTable($strTable);
	

	$str_submit_url = "index.php?";
				
	foreach ($_GET as $key=>$value) 
	{ 
		if($key != "order"&&$key!="order_type")
		{
			$str_submit_url .= $key."=".$value."&";
		}
	}
	
	foreach ($oNames as $columnName) 
	{
			echo "<td class=\"header-td\" width=".$arrTDSizes[$iTDHeaderCounter]."  nowrap >
			
			".(in_array($oCol[$iTDHeaderCounter],$arrFields)?("<a  class=\"header-td\" href=\"".$str_submit_url."order=".$oCol[$iTDHeaderCounter]."&order_type=".$order_type."".(isset($URLToAdd)?$URLToAdd:"")."\">"):"")."
			".$columnName."
			</a>
			
			".((isset($_REQUEST["order"])&&$_REQUEST["order"]==$oCol[$iTDHeaderCounter]&&$strImgName!="")?"<img src=\"images/".$strImgName."\" width=\"14\" height=\"14\" style=\"position:relative;top:1px;left:5px\"/>":"")."
			
			</td>";
			$iTDHeaderCounter++;
  	}

	echo "</tr>";

	$boolColor=true;


	
	while ($myArray = mysql_fetch_array($oDataTable))
	{
	
		
			if(isset($arrHighlightIds) && isset($strHighlightIdName) && in_array($myArray[$strHighlightIdName],$arrHighlightIds,false)){
				echo "<tr bgcolor=\"#ffcf00\"  height=20>";
			}
			else{
				echo "<tr bgcolor=\"".($boolColor?"#ffffff":"#f3f3f3")."\"  height=\"30\">";
			}

			
			if(strpos($strCheckColumnName,"#") !== false)
			{
			
			}
			else
			if(trim($strCheckColumnName)!="")
			{
			
				$cVal=$myArray[$strCheckValue];
				echo "<td nowrap>";
				
				if($IS_RADIO)
				{
					echo "<input title=\"".$strCheckColumnName."\" type=\"radio\" name=\"CheckList\" value=\"".$cVal."\" ".($cVal==$RADIO_VALUE?"checked":"")."/>";
				}
				else
				{
					echo "<input title=\"".$strCheckColumnName."\" type=\"checkbox\" name=\"CheckList[]\" value=\"".$cVal."\">";
				}
			
				echo "</td>";
			}


			foreach ($oCol as $columnName) 
			{



				$strParticularCases=particularCases($columnName,$myArray);

				if($strParticularCases!="")
				{
						echo $strParticularCases;
				}
				else
				if($columnName == "date"||$columnName == "date_start"||$columnName == "timestamp")
				{
					if(isset($myArray[$columnName]) && $myArray[$columnName] != "")
					{
						echo "<td class=oMain>".date("d/m/y G:i",$myArray[$columnName])."</td>";
					}
					else
					{
						echo "<td class=oMain>&nbsp;</td>";
					}
				}
				else{
						$val="";

						if(isset($myArray[$columnName]))
						{

									$val=$myArray[$columnName];
						}
						
						if($textSearch!=""&&(isset($_REQUEST["comboSearch"])&&$_REQUEST["comboSearch"]==$columnName))
						{
							
							$val=str_ireplace($textSearch,"<span class=\"yellow-back-text\">".$textSearch."</span>",$val);
							echo "<td>".$val."</td>";
						}
						else
						{
							if(substr($val,0,4) == "http")
							{
								echo "<td><a href=\"".$val."\">".$val."</a></td>";
							}
							else
							{
   								echo "<td>".$val."</td>";
							}
						}
				}
  			}

			echo "</tr>";

			$boolColor=$boolColor?false:true;

	}

	echo "</table>";

	if(strpos($strCheckColumnName,"#") !== false)
	{
		echo "
			<br>
			
			<div class=\"fleft\">
			<input type=\"submit\" value=\" ".str_replace("#","",$strCheckColumnName)." \" class=\"adminButton\"/>
			</div>
		";	
	}
	else
	if(trim($strCheckColumnName)!="")
	{
		echo "
			<br>
			<input type=\"hidden\" name=\"Delete\" value=\"1\"/>
			
			<div class=\"fleft\">
			<input type=\"submit\" value=\" ".$strCheckColumnName." \" class=\"adminButton\"/>
			</div>
		";	
	}
	
	
	
	
	echo "</form>";
	
	
	//table pagination
	
	$strSearchString = "";
			
	foreach ($_GET as $key=>$value) 
	{ 
		if($key != "PageNumber")
		{
			$strSearchString .= $key."=".$value."&";
		}
	}
	
	foreach ($_POST as $key=>$value) 
	{ 
		if($key != "PageNumber")
		{
			$strSearchString .= $key."=".$value."&";
		}
	}
	
	if(ceil($iTotalResults/$PageSize) > 1)
	{

		echo "<br/>";
		
		$inCounter = 0;
		
		if($PageNumber != 1)
		{
			echo "&nbsp; <a ".($ajax_call?"target=\"ajax-ifr\" onmousedown=\"javascript:LoadingIcon()\"":"")." class=\"small-nav-link\" href=\"".(isset($ajax_call)&&$ajax_call?"admin_page.php?ajax_load=1&":"index.php?").$strSearchString."PageNumber=1\"><b><<</b></a> ";
			
			echo "&nbsp; <a ".($ajax_call?"target=\"ajax-ifr\" onmousedown=\"javascript:LoadingIcon()\"":"")." class=\"small-nav-link\" href=\"".(isset($ajax_call)&&$ajax_call?"admin_page.php?ajax_load=1&":"index.php?").$strSearchString."PageNumber=".($PageNumber)."\"><b><</b></a> &nbsp;";
		}
		
		$iStartNumber = $PageNumber;
		
		if($iStartNumber > (ceil($iTotalResults/$PageSize) - 4))
		{
			$iStartNumber = (ceil($iTotalResults/$PageSize) - 4);
		}
		
		if($iStartNumber>3&&$PageNumber<(ceil($iTotalResults/$PageSize) - 2))
		{
			$iStartNumber=$iStartNumber-2;
		}
		
		if($iStartNumber < 1)
		{
			$iStartNumber = 1;
		}
		
		for($i= $iStartNumber ;$i<=ceil($iTotalResults/$PageSize);$i++)
		{
			if($inCounter>=5)
			{
				break;
			}
			
			if($i == $PageNumber)
			{
				echo "<b>".$i."</b> ";
			}
			else
			{
				echo "<a ".($ajax_call?"target=\"ajax-ifr\" onmousedown=\"javascript:LoadingIcon()\"":"")." class=\"small-nav-link\" href=\"".(isset($ajax_call)&&$ajax_call?"admin_page.php?ajax_load=1&":"index.php?").$strSearchString."PageNumber=".$i."\"><b>".$i."</b></a> ";
			}
							
			
			$inCounter++;
		}
		
		if($PageNumber<ceil($iTotalResults/$PageSize))
		{
			echo "&nbsp; <a ".($ajax_call?"target=\"ajax-ifr\" onmousedown=\"javascript:LoadingIcon()\"":"")." class=\"small-nav-link\" href=\"".(isset($ajax_call)&&$ajax_call?"admin_page.php?ajax_load=1&":"index.php?").$strSearchString."PageNumber=".($PageNumber+1)."\"><b>></b></a> ";
			
			echo "&nbsp; <a ".($ajax_call?"target=\"ajax-ifr\" onmousedown=\"javascript:LoadingIcon()\"":"")." class=\"small-nav-link\" href=\"".(isset($ajax_call)&&$ajax_call?"admin_page.php?ajax_load=1&":"index.php?").$strSearchString."PageNumber=".(ceil($iTotalResults/$PageSize))."\"><b>>></b></a> ";
		}
		
		
	}

	//end table pagination
}


function particularCases($columnName,$myArray)
{
	global $_REQUEST,$website,$category,$action,$ID,$EDIT_PICTURE,$iN,$AuthGroup,$lang,$arrFrmPages;

	if($columnName=="show_images")
	{
		$html_content = "<td width=\"80\"><img src=\"images/spacer.gif\" width=\"120\" height=\"1\">";
		
		if($myArray['images']!="")
		{
			$image_ids = explode(",",$myArray['images']);
			
			foreach($image_ids as $image_id)
			{
				if(file_exists("../thumbnails/".$image_id.".jpg"))
				{
					$html_content .= "<a href=\"../uploaded_images/".$image_id.".jpg\" class=\"hover\"><img src=\"../thumbnails/".$image_id.".jpg\" class=\"admin-preview-thumbnail\"/></a>";
				}
			}
			
		}
		else
		{
			$html_content .= "<img src=\"../images/no_pic.gif\" width=\"50\" style=\"float:left;margin-right:10px;margin-bottom:10px\"/>";
		}
		
		$html_content .= "</td>";
		
		return $html_content;
	}
	else
	if($columnName=="url")
	{
		return "<td><b><a target=\"_blank\" href=\"http://".$myArray['url']."\">".$myArray['url']."</a></b>
		
		</td>";
	}
	else
	if($columnName=="site_info")
	{
		$site_info="<td>
		<span class=\"small-text\">
			".$website->text_words(stripslashes($myArray['description']),30)."
		";
		if(trim($myArray["fields"])!="")
		{
			$listing_fields = unserialize($myArray["fields"]);
			
			if(is_array($listing_fields))
			{
				foreach($listing_fields as $key=>$value)
				{
					$site_info.="<br/>
					".stripslashes($key).": <b>".stripslashes($value)."</b>";
				}
			}
		}
		
		$site_info.="<br/>
		</span></td>";
		return $site_info;
	}
	else
	
	if($columnName=="user_info")
	{
		return "<td><b>".stripslashes($myArray['name'])."</b>
		<br>
		<span style=\"font-size:10px\">
			".$myArray['email']."
		</span>
		</td>";
	}
	else
	if($columnName=="package_info")
	{
		$categories = $_REQUEST["categories"];
		
		$listing_category = "";
		
		$category_items = explode(".",$myArray['link_category']);
		$current_id="";
		$b_first = true;
		for($i=0;$i<sizeof($category_items);$i++)
		{
			if(!$b_first)
			{	
				$current_id.=".";
				$listing_category.=" > ";
			}
			
			$current_id .= $category_items[$i];
		
			$b_first = false;
			
			if(isset($categories[$current_id]))
			{
				$listing_category .= $categories[$current_id];
			}
		}
		return "<td>".$listing_category."
		</td>";
		/*
		return "<td>Package: <b>".(isset($_REQUEST["packages"][$myArray['package']])?$_REQUEST["packages"][$myArray['package']]:"[n/a]")."</b>
		<br>
		<span style=\"font-size:11px\">
			".$listing_category."
			
		</span>
		</td>";
		*/
	}
	else
	
	if($columnName=="html_limit"){
		
		$strToDisplay = "";
		
		if(strlen($myArray["html"])<=100)
		{
			$strToDisplay = $myArray["html"];
		}
		else
		{
			$strToDisplay = substr($myArray["html"],0,100)." <a href='index.php?category=".$_REQUEST["category"]."&folder=$action&page=view&id=".$myArray['id']."'>...</a>";
		}
		
		
				return "<td>".$strToDisplay."</td>";
	}
	
	else
	if($columnName=="RenewBanner")
	 {
		 if($myArray['expires'] > time()) 
		 {
			return "<td>&nbsp;</td>";
		 }
		 else
		 {
			return "<td><a href='index.php?category=ads&action=banners&renew=1&banner=".$myArray['id']."' ><img src=images/link_arrow.gif width=16 height=16 border=0></a></td>"; 
		 }
	  
	  } 
	 else 
	if($columnName=="image_id"){
		global $mod;
				return "<td><a href='../uploaded_images/".$myArray['image_id'].".jpg' target=_blank><img src='../thumbnails/".$myArray['image_id'].".jpg' border=\"0\" width=\"100\" height=\"100\"/></a></td>";
	}
	
	else
	if($columnName=="file_id")
	{
		global $mod,$OPEN_FILE;
				return "<td><a href='../file.php?id=".$myArray['file_id']."' target=_blank>file.php?id=".$myArray['file_id']."</a></td>";
	}
	

	else
	if($columnName=="AlbumEdit"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=create&page=edit&id=".$myArray['id']."' ><img src='images/edit.gif' width=23 height=22 border=0></a></td>";
	}
	else
	if($columnName=="ShowComments"){
				return "<td valign=middle><a href='index.php?category=".$_REQUEST["category"]."&folder=$action&page=comments&id=".$myArray['id']."' ><img src='images/preview.gif' width=23 height=22 border=0></a></td>";
	}
	else
	if($columnName=="EditNote")
	{
				
		return "<td width=\"20\"><a href='index.php?category=".$_REQUEST["category"]."&folder=".$_REQUEST["action"]."&page=edit&id=".$myArray['id']."' ><img src=\"images/link_arrow.gif\" width=\"16\" height=\"16\" border=\"0\"/></a></td>";
	}
	else
	if($columnName=="ShowExportReport"){
				return "<td valign=middle><a href='index.php?category=".$_REQUEST["category"]."&folder=history&page=report&id=".$myArray['id']."' ><img src='images/preview.gif' width=23 height=22 border=0></a></td>";
	}
	else
	if($columnName=="ShowFormData"){
				return "<td valign=middle><a href='index.php?category=".$_REQUEST["category"]."&folder=manage&page=data&id=".$myArray['id']."' ><img src='images/preview.gif' width=23 height=22 border=0></a></td>";
	}
	else	
	if($columnName=="ShowFlag"){
				return "<td valign=middle><img src=\"../images/flags/".$myArray["code"].".gif\" /></td>";
	}
	else	
	if($columnName=="ShowSpecialLanguage"){
				return "<td><input type=radio name=\"default_language[]\" onclick=\"javascript:RadioClick(".$myArray["id"].")\" value=\"".$myArray["id"]."\" ".($myArray["default_language"]==1?"checked":"")."></td>";
	}
	else	
	if($columnName=="active"){
				return "<td>".($myArray["active"]==1?"YES":"NO")."</td>";
	}
	else
	if($columnName=="ChangeLanguage"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=".$_REQUEST["action"]."&page=edit&id=".$myArray['id']."' ><img src=\"images/link_arrow.gif\" width=\"16\" height=\"16\" border=\"0\"/></a></td>";
	}
	else
	if($columnName=="ShowPageLink"){
				return "<td><a href='../index.php?page=".$myArray['id']."' target=_blank><img src='images/preview.gif' width=23 height=22 border=0></a></td>";
	}
	else
	if($columnName=="ShowPageEditLink"){
				return "<td><a href='javascript:StartWizard(".$myArray['id'].")' ><img src='images/edit.gif' width=23 height=22 border=0></a></td>";
	}
	else
	if($columnName=="ShowDeleteLink"){
				return "<td><a href='javascript:DeletePage(".$myArray['id'].")'' ><img src='images/cut.gif' width=23 height=22 border=0></a></td>";
	}
	
else
	
	

	if($columnName=="ShowModifierUtilisateur"){
	
		if($myArray['username']=="administrator")
		{
			return "<td>[n/a]</td>";
		}
		else
		{
				return "<td>
						<a href='index.php?category=".$_REQUEST["category"]."&folder=admin&page=edit&id=".$myArray['id']."' >
						<img src='images/link_arrow.gif' width=16 height=16 border=0>
						</a></td>";
		}
	}
	else
	if($columnName=="EditCommonAccount"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=accounts&page=editcommon&UserName=".$myArray['UserName']."' ><img src='../images/edit.gif' width=23 height=22 border=0></a></td>";
	}
	else
	
	if($columnName=="ShowFormDelete"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&action=".$_REQUEST["action"]."&ProceedDelete=yes&id=".$myArray['id']."' ><img src='images/cut.gif' border=0></a></td>";
	}
	else
	if($columnName=="GoogleQuery"){
	
		$strQuery="";
		
			$arrInfo2=explode("?",$myArray["referer"],2);
			
			if(sizeof($arrInfo2)>1)
			{
				$arrInfo = explode("&",$arrInfo2[1]);
		
			
				
				foreach($arrInfo as $strInfo)
				{
					if(substr($strInfo,0,2) == "q=")
					{
					
						$strQuery = str_replace("q=","",$strInfo);
						
						break;
					}
					}
					
				}
			
				return "<td><a href=\"".$myArray["referer"]."\" target=_blank>".(strtoupper(urldecode($strQuery)))."</a></td>";
	}
	else
	if($columnName=="EditAdminUser"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=admin&page=editadmin&id=".$myArray['id']."' ><img src='../images/edit.gif' width=23 height=22 border=0></a></td>";
	}
	else
	if($columnName=="EditCommonUser"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=accounts&page=editcommon&id=".$myArray['id']."' ><img src='../images/edit.gif' width=23 height=22 border=0></a></td>";
	}
	
	else
	if($columnName=="DeleteLanguage"){
				return "<td><a href='javascript:DeleteLanguage(".$myArray['id'].",\"".$myArray['code']."\")' ><img src='images/cancel.gif' width=21 height=20 border=0></a></td>";
	}
	else
	if($columnName=="DeleteTemplate"){
				return "<td><a href='javascript:DeleteTemplate(".$myArray['id'].")' ><img src='images/cancel.gif' width=21 height=20 border=0></a></td>";
	}
	else
	if($columnName=="ModifyTemplate"){
	
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=modify&page=edit&id=".$myArray['id']."' ><img src='images/link_arrow.gif' width=16 height=16 border=0></a></td>";
	}
	else
	if($columnName=="EditPosting"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=my&page=edit&id=".$myArray['id']."' ><img src='images/edit.gif' width=23 height=22 border=0></a></td>";
	}
	
	if($columnName=="ShowDeleteLink"){
				return "<td><a href='javascript:DeletePage(".$myArray['id'].")'' ><img src=\"../images/cut.gif\" border=\"0\" /></a></td>";
	}
	
	else
	if($columnName=="BackupForm"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&action=".$_REQUEST["action"]."&SpecialProcessAddForm=".$myArray['id']."' ><img src='images/copy.gif'  border=0></a></td>";
	}
	else
	if($columnName=="ShowFormEdit")
	{
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=manage&page=edit&id=".$myArray['id']."' ><img src='images/link_arrow.gif' border=0></a></td>";
	}
	else
	if($columnName=="ShowFormPreview"){
				return "<td><a href='index.php?category=".$_REQUEST["category"]."&folder=manage&page=preview&id=".$myArray['id']."' ><img src='images/link_arrow.gif' border=0></a></td>";
	}
	else
	if($columnName=="date_string"){
				return "<td>".$myArray['date']."</td>";
	}
	else
	if($columnName=="ShowAssignForm")
	{
	
				$oResult= "<td>";
				
				$oResult.="<select style=\"width:160px\" name=\"pg_".$myArray['id']."\">";
				$oResult.="<option>Please Select</option>";
				
				if(isset($_REQUEST["frm-pages"]))
				{
					foreach($_REQUEST["frm-pages"]  as $pg)
					{
						list($lng,$page)=explode("_",$pg);
						$oResult.="<option value=\"".$pg."\" ".($myArray['page']==urldecode($pg)?"selected":"").">".urldecode($page)." [".strtoupper($lng)."] </option>";				
					}
				}
				
				

			
				$oResult.="</select>";
				
				$oResult.= "</td>";
				
				return $oResult;
	}
	
	

	return "";
}

function EditParams
	(
		$strListOfParamIds,
		$arrTypes,
		$strSubmitText,
		$strSuccessMessage
	){

	global $EditColumns,$FirstTDAlign,$SelectWidth,$TextboxWidth,$TableWidth;
	global $DBprefix;
	global $SpecialProcessEditParams,$AuthUserName;
	global $firstTDLength;
	global $database,$_REQUEST;

	if(!isset($FirstTDAlign))
	{
		$FirstTDAlign = "right";
	}
	
	if(isset($_REQUEST["SpecialProcessEditParams"])){

		
		foreach(explode(",",$strListOfParamIds) as $i)
		{
			$sql="UPDATE ".$DBprefix.""."settings SET value='".$_REQUEST["val".$i]."' WHERE id=".$i;

			$database->Query($sql);
		}

		echo "
			<h3>".$strSuccessMessage."</h3><br/><br/>";
	}
	
	echo "<form action=\"index.php\" method=\"post\">";
	echo "<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST["category"]."\" />";
	echo "<input type=\"hidden\" name=\"SpecialProcessEditParams\" value=\"1\" />";
		
	if(isset($_REQUEST["action"]))
	{
		echo "<input type=\"hidden\" name=\"action\" value=\"".$_REQUEST["action"]."\"/>";
	}
	else
	{
		echo "<input type=\"hidden\" name=\"folder\" value=\"".$_REQUEST["folder"]."\"/>";
		echo "<input type=\"hidden\" name=\"page\" value=\"".$_REQUEST["page"]."\"/>";
	}

	$oTable=$database->DataTable("settings"," WHERE id in (".$strListOfParamIds.") ORDER BY id");

	$i=0;
	
	echo "<table>";

	while($row=mysql_fetch_array($oTable))	
	{

		if(isset($EditColumns))
		{
			if(($i % $EditColumns) == 0)
			{
				echo "<tr height=25>";			
			}
		}
		else
		{
			echo "<tr height=25>";
		}
	

		echo "<td width=".(isset($_REQUEST["message-column-width"])?$_REQUEST["message-column-width"]:"150")." align=".$FirstTDAlign." >".(strpos(strtolower($row['description']),"color")?"<a href=\"javascript:ShowColorMenu('val".$row['id']."')\">":"")."".$row['description'].":".(strpos(strtolower($row['description']),"color")?"</a>":"")." </td>";
		echo "<td>";

		if(strstr($arrTypes[$i],"file")){

				echo "<input type=file value=\"".$row['value']."\" name=\"val".$row['id']."\" size=30>";
		}
		else
		if(strstr($arrTypes[$i],"textbox"))
		{
			list($strType,$strSize)=explode("_",$arrTypes[$i]);
			
				echo "<input type=text value=\"".$row['value']."\" name=\"val".$row['id']."\" id=\"val".$row['id']."\" size=$strSize ".(isset($TextboxWidth)?"style='width:".$TextboxWidth."'":"").">";
		}
		else
		if(strstr($arrTypes[$i],"textarea"))
		{
			list($strType,$strCols,$strRows)=explode("_",$arrTypes[$i]);

			echo "<textarea  name=\"val".$row['id']."\" cols=$strCols rows=$strRows>".$row['value']."</textarea>";
		}
		else
		if(strstr($arrTypes[$i],"javascript~combobox")){

			echo "<select  name=\"val".$row['id']."\" onChange=javascript:SelectChanged(this)>";

			foreach(explode("_",$arrTypes[$i]) as $strOption){

				if($strOption=="javascript~combobox"){
					continue;
				}

				echo "<option>".str_replace("~"," ",$strOption)."</option>";
			}

		}
		else
		if(strstr($arrTypes[$i],"combobox"))
		{
			
			echo "<select ".(isset($_REQUEST["select-width"])?"style=\"width:".$_REQUEST["select-width"]."px\"":"")." name=\"val".$row['id']."\">";

			foreach(explode("_",$arrTypes[$i]) as $strOption)
			{

				if($strOption=="combobox")
				{
					continue;
				}

				$arrOptions = explode("^", $strOption);

				if(sizeof($arrOptions)>1)
				{
					echo "<option value=\"".$arrOptions[1]."\"";
					if($arrOptions[1]==$row["value"])
					{
						echo " selected";
					}
					echo">".$arrOptions[0]."</option>";
				}
				else
				{
					echo "<option ";
					if($strOption==$row["value"])
					{
						echo " selected";
					}
					echo">".$strOption."</option>";
				
				}

			}

			echo "</select>";

		}
		else
		{
			echo "This type is not supported.";
		}

		$i++;

		echo "</td><td width=30>&nbsp;</td>";
		
		
		if(isset($EditColumns))
		{
			if(($i % $EditColumns) == 0)
			{
				echo "</tr>";
			}
		}
		else
		{
			echo "</tr>";
		}
		
	}



	echo "</table>
		<br>
		
		<input type=\"submit\" class=\"adminButton\" value=\"".$strSubmitText."\"/>
	
	</form>";

}

function LinkTile
		(
			$category,
			$action,
			$text,
			$small_text,
			$color="blue",
			$type="small",
			$no_ajax = false,
			$js_function = ""
		)
{
	global $lang,$currentPage,$currentUser,$DEBUG_MODE,$is_mobile;
	
	$has_param = strpos($action,"=");
	
	if($has_param !== false)
	{
		$no_ajax = true;
		$action = str_replace("-","&",$action);
	}
	
	if(!$currentPage->CheckPermissions($category, $action))
	{
		return "";
	}
	
	if($js_function!="")
	{
		$tileLink = "javascript:".$js_function."()";
	}
	else
	if($no_ajax || (isset($is_mobile)&&$is_mobile) || (isset($DEBUG_MODE)&&$DEBUG_MODE))
	{
		$tileLink = "index.php?category=".$category."&action=".$action;
	}
	else
	{
		$tileLink = "#".$category."-".$action;
	}
	
	
	return	"<a class=\"".$type."-tile ".$color."-back\" href=\"".$tileLink."\">
				<h3>".$text."</h3>
				<h6>".($lang=="en"?strtolower($small_text):$small_text)."</h6>
			</a>";
			
}


function AddEditForm
	(
		$arrTexts,
		$arrEditFields,
		$arrMissedFields,
		$arrTypes,
		$strTableName,
		$strUniqueKey,
		$strCurrentUniqueKeyValue,
		$strSuccessMessage,
		$jsValidation ="",
		$MessageTDLength = 120,
		$HideSubmit = false
	)
	{

	global $DBprefix,$lang;
	global $category,$folder,$page,$action;
	global $SpecialProcessEditForm,$SubmitButtonText;

	global $database,$_REQUEST;

	$oArray=$database->DataArray($strTableName,$strUniqueKey."=".$strCurrentUniqueKeyValue);

	if(isset($_REQUEST["SpecialProcessEditForm"]))
	{

		$arrValues=array();
		$arrEditNames=array();

		for($i=0;$i<sizeof($arrEditFields);$i++)
		{
		
			$strName=$arrEditFields[$i];

			if(in_array($strName,$arrMissedFields))
			{
				continue;
			}

			
			if($arrTypes[$i]=="file")
			{
				$path="../";
				$file_field_name="user_logo";
				include("include/images_processing.php");
				if($str_images_list!="")
				{
					array_push($arrEditNames,"user_logo");
					array_push($arrValues,$str_images_list);
				}
			}
			else
			{
				
				array_push($arrEditNames,$strName);
				$tempValue = "";
				if(isset($_REQUEST[$strName]))
				{
					$tempValue = $_REQUEST[$strName];
				}
				else
				if(isset($_REQUEST["post_".$strName]))
				{
					
					$tempValue = $_REQUEST["post_".$strName];
				}

				array_push($arrValues,$tempValue);
			}
		}

		$database->SQLUpdate($strTableName,$arrEditNames,$arrValues,"$strUniqueKey=$strCurrentUniqueKeyValue");

		echo "<br/><h3>".$strSuccessMessage."</h3><br/><br/><br/>";
		
		$oArray=$database->DataArray($strTableName,$strUniqueKey."=".$strCurrentUniqueKeyValue);

	}
	
	if(true)
	{

	
		echo "<form  ".(isset($_REQUEST["no-form-class"])?"":"class=\"user-form\"")."  id=\"EditForm\" ".(isset($jsValidation)&&$jsValidation!=""?"onsubmit='return $jsValidation(this)'":"")." action=\"index.php\" method=\"post\" enctype=\"multipart/form-data\">";
		echo "<input type=\"hidden\" name=\"category\" value=\"".$_REQUEST["category"]."\">";
		
		if(isset($_REQUEST["folder"])&&isset($_REQUEST["page"]))
		{
			echo "<input type=\"hidden\" name=\"folder\" value=\"".$_REQUEST["folder"]."\"/>";
			echo "<input type=\"hidden\" name=\"page\" value=\"".$_REQUEST["page"]."\"/>";
		}
		else
		{
			echo "<input type=\"hidden\" name=\"action\" value=\"".$_REQUEST["action"]."\"/>";
		}
		echo "<input type=\"hidden\" name=\"".$strUniqueKey."\" value=\"".$strCurrentUniqueKeyValue."\"/>";
		echo "<input type=\"hidden\" name=\"SpecialProcessEditForm\" value=\"1\"/>";

		global $strSpecialHiddenFieldsToAdd;
		if(isset($strSpecialHiddenFieldsToAdd))
		{
			echo $strSpecialHiddenFieldsToAdd;
		}
		
		echo "<fieldset><ol>";
		for($i=0;$i<sizeof($arrTexts);$i++)
		{
		
			echo "\n<li>";
			
			echo '<div class="div_label">';
			
			if($arrTexts[$i]!="")
			{
				echo $arrTexts[$i];
			}
			
			echo '</div>
			<div class="div_field">';

			if($arrEditFields[$i]!="images"&&in_array($arrEditFields[$i],$arrMissedFields))
			{
				
				echo $oArray[$arrEditFields[$i]];
			}
			else
			if(strstr($arrTypes[$i],"combobox_table")){
				
				$strVal="";
				if(isset($oArray[$arrEditFields[$i]]))
				{
					$strVal=$oArray[$arrEditFields[$i]];
				}
							
				list($strType,$strTableName,$strFieldValue,$strFieldName)=explode("~",$arrTypes[$i]);
				
				$oTable=$database->DataTable($strTableName,"");
					
				echo "<select ".(isset($_REQUEST["select-width"])?"style=\"width:".$_REQUEST["select-width"]."px\"":"")." name=\"".$arrEditFields[$i]."\">";
				
				while($oRow=mysql_fetch_array($oTable)){
						echo "<option ".($oRow[$strFieldValue]==$strVal?"selected":"")." value=\"".$oRow[$strFieldValue]."\">".$oRow[$strFieldName]."</option>";
				}
				
				echo "</select>";
			}
			else
			
			if(strstr($arrTypes[$i],"textbox")){

				list($strType,$strSize)=explode("_",$arrTypes[$i]);
				
					$strVal="";
					if(isset($oArray[$arrEditFields[$i]])){
						$strVal=$oArray[$arrEditFields[$i]];
					}
					echo "<input type=text value=\"".stripslashes($strVal)."\" name=\"".$arrEditFields[$i]."\" id=\"".$arrEditFields[$i]."\" size=$strSize>";
			}
			else
			if(strstr($arrTypes[$i],"textarea")){
				list($strType,$strCols,$strRows)=explode("_",$arrTypes[$i]);
				
				$strVal="";
				if(isset($oArray[$arrEditFields[$i]]))
				{
					$strVal=$oArray[$arrEditFields[$i]];
				}
					
				echo "
				<input type=\"hidden\" id=\"post_".$arrEditFields[$i]."\" name=\"post_".$arrEditFields[$i]."\"/>
				<textarea name=\"".$arrEditFields[$i]."\" id=\"".$arrEditFields[$i]."\" cols=\"".($strCols=="100%"?"20":$strCols)."\" rows=\"".$strRows."\" ".($strCols=="100%"?"style=\"width:100% !important\"":"").">".stripslashes($strVal)."</textarea>";
			}
			else
			if(strstr($arrTypes[$i],"images"))
			{
				$strVal="";
			
				if(isset($oArray[$arrEditFields[$i]]))
				{
					$strVal=trim($oArray[$arrEditFields[$i]]);
				}
				
				if($strVal!="")
				{
					$image_ids = explode(",",$strVal);
					
					foreach($image_ids as $image_id)
					{
						if(file_exists("../thumbnails/".$image_id.".jpg"))
						{
							echo "<a href=\"../uploaded_images/".$image_id.".jpg\" class=\"hover\"><img src=\"../thumbnails/".$image_id.".jpg\" class=\"admin-preview-thumbnail\"/></a>";
						}
						
					}
					
				}
				else
				{
					echo "<img src=\"../images/no_pic.gif\" width=\"100\" height=\"100\" style=\"float:left;margin-right:10px;margin-bottom:10px\"/>";
				}
				global $MODIFY;
				echo "<span class=\"lfloat\"><a href=\"index.php?category=ads&action=images&id=".$strCurrentUniqueKeyValue."\">".$MODIFY."</a></span>";
			}
			else
			if(strstr($arrTypes[$i],"combobox_link_categories"))
			{
				$strVal="";
			
				if(isset($oArray[$arrEditFields[$i]])){
					$strVal=$oArray[$arrEditFields[$i]];
				}
				
				echo "<select id=\"link_category\" name=\"link_category\">";

				echo "<option value=\"\">Please select</option>";
				
				if(file_exists('../categories/categories_'.strtolower($lang).'.php'))
				{
					$categories_content = file_get_contents('../categories/categories_'.strtolower($lang).'.php');
				}
				else
				{
					$categories_content = file_get_contents('../categories/categories_en.php');
				}

				$arrCategories = explode("\n", trim($categories_content));

				$categories=array();
				
				foreach($arrCategories as $str_category)
				{
					list($key,$value)=explode(". ",$str_category);
					$categories[trim($key)]=trim($value);
					
					$i_level = substr_count($key, ".");
					
					echo "<option  value=\"".trim($key)."\" ".($strVal==trim($key)?"selected":"").">".str_repeat("|&nbsp;&nbsp;&nbsp;&nbsp;", $i_level)."|__".trim($value)."</option>";
				}
				
				echo "</select>";

			}
			else
			if(strstr($arrTypes[$i],"combobox_special")){
				
				
					
				if($arrEditFields[$i]=="pg")
				{
				
					$strVal="";
					
					if(isset($oArray[$arrEditFields[$i]]))
					{
						$strVal = $oArray[$arrEditFields[$i]];
					}
				
				   	echo "<select ".(isset($_REQUEST["select-width"])?"style=\"width:".$_REQUEST["select-width"]."px\"":"")." name=\"pg\">";
					echo "<option>NONE OF THE PAGES</option>";
				
					global $arrFrmPages;
						
					foreach($arrFrmPages as $pg)
					{
						list($lng,$page)=explode("_",$pg);
						
						echo "<option value=\"".$pg."\" ".($strVal==$pg?"selected":"").">".urldecode($page)." [".strtoupper($lng)."] </option>";				
						
					}
				
					echo "</select>";
				
					echo "</td>";
				
			
				}
				
				
			}
			else
			if(strstr($arrTypes[$i],"file"))
			{
			
				echo "<input type=\"file\" name=\"".$arrEditFields[$i]."\" ".(isset($_REQUEST["select-width"])?"style=\"width:".$_REQUEST["select-width"]."px\"":"")." />";
			}
			else
			if(strstr($arrTypes[$i],"combobox")){

			echo "<select ".(isset($_REQUEST["select-width"])?"style=\"width:".$_REQUEST["select-width"]."px\"":"")." name=\"".$arrEditFields[$i]."\">";

				foreach(explode("_",$arrTypes[$i]) as $strOption){

					if($strOption=="combobox"){
						continue;
					}

					$arrOptions = explode("^", $strOption);

					if(sizeof($arrOptions)>1)
					{
						echo "<option value=\"".$arrOptions[1]."\"";
						if($arrOptions[1]==$oArray[$arrEditFields[$i]]){
							echo " selected";
						}
						echo">".$arrOptions[0]."</option>";
					}
					else
					{
						echo "<option ";
						if($strOption==$oArray[$arrEditFields[$i]]){
							echo " selected";
						}
						echo">".$strOption."</option>";
					
					}

				}

				echo "</select>";

			}

			echo "</div>
			<div class=\"clear\"></div>

			\n</li>";	
		}
		
		echo "</ol></fieldset>";

		echo "<br>";
			
		if(!$HideSubmit)
		{
				echo "<input class=\"adminButton lfloat\" type=\"submit\" value=\"".(isset($SubmitButtonText)?$SubmitButtonText:"Save")."\"/>";
		}
		echo "</form>";
		
	}
}


function generateBackLink($pageAction,$evLinkTexts,$evLinkActions)
{
		global $_REQUEST,$GO_BACK_TO;
		
		echo "<br/><br/>";
		
		echo "<a href=\"index.php?category=".$_REQUEST["category"]."&action=".$pageAction."\">".$GO_BACK_TO." \"".$evLinkTexts[array_search($pageAction,$evLinkActions,false)]."\"</a>";
	
}

function Parameter($i)
{
	global $website;
	
	return $website->params[$i];
}

function aParameter($i)
{
	global $website;
	
	if(isset($website->params[$i]))
	{
		return $website->params[$i];
	}
	else
	{
		return "";
	}
}

function CreateLink($category,$action)
{
	global $is_mobile,$DEBUG_MODE;
	
	if($is_mobile||$DEBUG_MODE)
	{
		return "index.php?category=".$category."&action=".$action;
	}
	else
	{
		return "#".$category."-".$action;
	}
}


function CheckPermissions($category, $page)
{
	return true;
}


function str_show($str, $bret = false)
{
	$str = trim($str);
	$strResult = "";
	
	if(strstr($str,"{"))
	{
		$strVName = substr($str,1,strlen($str)-2);
		global $$strVName;
		$strResult = $$strVName;
	}
	else
	{
		$strResult = $str;
	}

	if($bret)
	{
		return $strResult;
	}

	echo $strResult;
}

function insert_image($strName)
{
	$path="../";
	$input_field=$strName;
	$str_images_list = "";
	include("../include/images_processing.php");
	
	if($str_images_list=="")
	{
		return 0;
	}
	else
	{
		return $str_images_list;
	}
}
?>