<?php
// Deals Portal 
// Copyright (c) All Rights Reserved, NetArt Media 2003-2014
// Check http://www.netartmedia.net/dealsportal for demos and information
?>
<?php
class AdminPage
{
	
	var $templateHTML="";
	var $pageHTML="";
	public $page;
		
	var $arrElements=array("title","menu","content","keywords","description");
	var $arrElementsHTML=array();
	var $templateID=0;
	var $arrPage;
	
	function AdminPage($page_name="")
	{
		$this->page = $page_name;
		
	}
	
	
	function Process($is_mobile=false)
	{
		global $is_mobile;
		global $AuthUserName,$MULTI_LANGUAGE_SITE,$DOMAIN_NAME,$lang,$arrUser,$LoginInfo,$currentUser;
		
		include("../ADMIN/texts_".$lang.".php");

		include("../include/texts_".$lang.".php");
		
		include("pages_structure.php");
		include("wysiwyg/detect_browser.php");
		
		include("include/init_vars.php");
		
		
		global $AuthUserName,$lang,$website,$database, $_REQUEST, $DBprefix;
		$iKEY = "AZ8007";
		$DN=2;
		$this->pageHTML=$website->TemplateHTML;
		include("include/check_php_version.php");
		
		if(isset($_REQUEST["category"]))
		{
			$category = $_REQUEST["category"];
		}
		else
		{
			$category = "home";
		}
		$website->ms_w($category);
		
		$HTML="";
		ob_start();
		if(isset($_REQUEST["folder"])&&isset($_REQUEST["page"]))
		{
			$folder = $_REQUEST["folder"];
			$page = $_REQUEST["page"];
			$website->ms_w($folder);
			$website->ms_w($page);
			
			if($this->CheckPermissions($category,$folder)
			&&file_exists($category."/".$folder."_".$page.".php"))
			{
				include($category."/".$folder."_".$page.".php");
			}
		}
		else
		{
			if(isset($_REQUEST["action"]))
			{
				$action = $_REQUEST["action"];
			}
			else
			{
				$action = "welcome";
			}
		
			$website->ms_w($action);

			if($this->CheckPermissions($category,$action)
			&&file_exists($category."/".$action.".php"))
			{
				include($category."/".$action.".php");
			}
			else
			{
				
			}
		}
		
		if($HTML=="")
		{
			$HTML = ob_get_contents();
		}
		ob_end_clean();

		if($is_mobile)
		{
			$mobile_start = strpos($HTML, '<!--mobile-start-->');
			$mobile_end   = strpos($HTML, '<!--mobile-end-->', $mobile_start);
			
			if($mobile_start !== false && $mobile_end !== false) 
			{
				$HTML   = substr($HTML, $mobile_start, ($mobile_end - $mobile_start));
			}
		}

		
		$this->pageHTML = str_replace("<wsa content/>",$HTML,$this->pageHTML);
				
		
			$this->pageHTML=
			str_replace
			(
				"<wsa languages_menu/>",
				$this->LanguagesMenu(),
				$this->pageHTML
			);
			
		
		
		
		$this->pageHTML=
			str_replace
			(
				"<wsa menu/>",
				$this->StartMenu(),
				$this->pageHTML
			);
			
		
		$website->TemplateHTML=$this->pageHTML;
		
		$str_page_link = "";
		if(isset($_REQUEST["folder"])&&isset($_REQUEST["page"]))
		{
			$str_page_link = "index.php?category=".$_REQUEST["category"]."&page=".$_REQUEST["page"]."&folder=".$_REQUEST["folder"]."&";
		}
		else
		if(isset($_REQUEST["category"])&&isset($_REQUEST["action"]))
		{
			$str_page_link = "index.php?category=".$_REQUEST["category"]."&action=".$_REQUEST["action"]."&";	
		}
		else
		{
			$str_page_link = "index.php?";
		}
		
		if($is_mobile)
		{
			$website->TemplateHTML = 
			str_replace("[MOBILE-LINK]",$str_page_link."switch_mobile=0",$website->TemplateHTML);
			include("include/help_tips.php");
		}
		else
		{
			$website->TemplateHTML = 
			str_replace("[MOBILE-LINK]",$str_page_link."switch_mobile=1",$website->TemplateHTML);
			include("include/help_tips.php");
		}
		
	}
	
	function StartMenu()
	{
		global $M_START,$M_ADD_ON,$lang;
		
		include("../ADMIN/texts_".$lang.".php");
		
		include("../include/texts_".$lang.".php");
		
		$menu_html = "";
		
		include("pages_structure.php");
		
		for($i=0;$i<count($oLinkTexts);$i++)
		{
			$vr1 = ($oLinkActions[$i]."_oLinkTexts");		
			$vr2 = ($oLinkActions[$i]."_oLinkActions");	
		
			$evSLinkTexts=$$vr1;
			$evSLinkActions=$$vr2;
			
			if($this->CheckPermissions($oLinkActions[$i], $evSLinkActions[0]))
			{
				if($oLinkActions[$i]=="home"&&$evSLinkActions[0]=="welcome")
				{
					$menu_html.="\n<li class=\"dropdown\"><a class=\"main-top-link\" href=\"index.php\">".$oLinkTexts[$i]."</a>";
				}
				else
				{
					$menu_html.="\n<li class=\"dropdown\"><a class=\"main-top-link\" href=\"index.php?category=".$oLinkActions[$i]."&action=".$evSLinkActions[0]."\">".$oLinkTexts[$i]."</a>";
				}
			}
			
			if(sizeof($evSLinkTexts)>1)
			{
				$menu_html.="\n<ul class=\"text-left dropdown-menu\">";
		
				for($j=0;$j<count($evSLinkTexts);$j++)
				{
					if(!strstr($evSLinkTexts[$j],$M_ADD_ON))
					{
						if($this->CheckPermissions($oLinkActions[$i], $evSLinkActions[$j]))
						{
							if($oLinkActions[$i]=="home"&&$evSLinkActions[$j]=="welcome")
							{
							  $str_link = "index.php";
							
							}
							else
							{
							  $str_link = "index.php?category=".$oLinkActions[$i]."&action=".$evSLinkActions[$j];
							}
							$menu_html.="\n<li style=\"position:relative;top:-1px\"><a href=\"".$str_link."\">".$evSLinkTexts[$j]."</a></li>";
						}
					}
					else
					{
						if($this->CheckPermissions($evSLinkActions[$j], ""))
						{
							$menu_html.="\n<li style=\"position:relative;top:-1px\"><a href=\"index.php?category=".$evSLinkActions[$j]."&action=home\">".$evSLinkTexts[$j]."</a></li>";
						}
					}
				}
				
				$menu_html.="\n</ul>";
			}
			$menu_html.="\n</li>";
		}
		
		return $menu_html;
	
	}
	
	
	
	function CheckPermissions($category, $action)
	{
		global $currentUser;
		
		
		if
		(
			$currentUser->AuthGroup == "Administrators"
			|| $category == "exit"
		)
		{
			return true;
		}
		else
		if(array_search("@".$currentUser->AuthGroup."@".$category."@".$action, $currentUser->arrPermissions,false))
		{
			return true;
		}
		else
		if($category != "" && $action=="")
		{
			$vr2 = ($category."_oLinkActions");	
			global $$vr2;
			$evLinkActions = $$vr2;

			if(isset($evLinkActions))
			{
				foreach($evLinkActions as $evAction)
				{
					if(array_search("@".$currentUser->AuthGroup."@".$category."@".$evAction, $currentUser->arrPermissions,false))
					{
						return true;
					}
				}
			}
			
			return false;
		}
		else
		{
			return false;
		}
		
	}
	
	function GetHTML()
	{
		global $is_mobile;
		global $AuthUserName,$MULTI_LANGUAGE_SITE,$lang,$arrUser,$LoginInfo,$currentUser;
		
		include("../ADMIN/texts_".$lang.".php");
		
		include("../include/texts_".$lang.".php");
		include("include/init_vars.php");
		
		global $AuthUserName, $lang,$website,$database, $_REQUEST, $DBprefix;
		$iKEY = "AZ8007";
		$DN=2;
		
		if(isset($_REQUEST["category"]))
		{
			$category = $_REQUEST["category"];
		}
		else
		{
			$category = "home";
		}
		$website->ms_w($category);
		
		$HTML="";
		ob_start();
		if(isset($_REQUEST["folder"])&&isset($_REQUEST["page"]))
		{
			$folder = $_REQUEST["folder"];
			$page = $_REQUEST["page"];
			$website->ms_w($folder);
			$website->ms_w($page);
			
			if($this->CheckPermissions($category,$folder)
			&&file_exists($category."/".$folder."_".$page.".php"))
			{
				include($category."/".$folder."_".$page.".php");
			}
		}
		else
		{
			if(isset($_REQUEST["action"]))
			{
				$action = $_REQUEST["action"];
			}
			else
			{
				$action = "welcome";
			}
		
			$website->ms_w($action);
			
			if($this->CheckPermissions($category,$action)
			&&file_exists($category."/".$action.".php"))
			{
				include($category."/".$action.".php");
			}
		}
		
		if($HTML=="")
		{
			$HTML = ob_get_contents();
		}
		ob_end_clean();

		return $HTML;
		
	}
	
	
	function LanguagesMenu()
	{
		global $lang,$AdminPanelLanguages,$_REQUEST,$language;
		
		$language_menu_html = "";
		
		if(isset($_REQUEST["folder"])&&isset($_REQUEST["page"]))
		{
			$strPageLink="category=".$_REQUEST["category"]."&folder=".$_REQUEST["folder"]."&page=".$_REQUEST["page"]."&";
		}
		else
		if(isset($_REQUEST["category"])&&isset($_REQUEST["action"]))
		{
			$strPageLink="category=".$_REQUEST["category"]."&action=".$_REQUEST["action"]."&";
		}
		else
		{
			$strPageLink="";
		}
										
		foreach($AdminPanelLanguages as $arrLang)
		{
			list($languageName,$languageCode)=$arrLang;
			
			if($languageCode == $lang) continue;
			
			$language_menu_html .= "<a class=\"language-link\" href=\"index.php?".$strPageLink."lng=".$languageCode."\">".$languageName."</a> ";
			
		}
		
		global $M_OPEN_MAIN_SITE;
		
		$language_menu_html .= "<a class=\"language-link left-margin-20px\" href=\"../index.php\" target=\"_blank\">".$M_OPEN_MAIN_SITE."</a> ";
			
		return $language_menu_html;
	}
	
	function MobileLink()
	{
	
	}
	
	
	
}
?>