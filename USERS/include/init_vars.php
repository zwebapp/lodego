<?php
// Deals Portal All Rights Reserved
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
$_IS_PRIVATE_SELLER = false;

$CURRENCY_SYMBOL="$";
$ACCEPT_PAYPAL = true;
$PAYPAL_CURRENCY_CODE="USD";
$PAYPAL_ACCOUNT="paypal@paypal.com";
$ACCEPT_2CHECKOUT = false;
$_2CHECKOUT_SID = "";
$ACCEPT_CHECK = true;
$CHEQUE_ADDRESS="[cheque address goes here]";
$ACCEPT_BANK_WIRE_TRANSFER = false;
$BANK_WIRE_TRANSFER_INFO="";
$SYSTEM_DEBUG_MODE = true;
$SITE_FORMAT = "US";
$ADMIN_TEMPLATE_ID = "13";
$MAX_NUMBER_IMAGES = 9;
$IMAGES_IN_DB = true;
$UPLOAD_DIR = "/home/uploaded_images/";
$MAX_IMAGE_SIZE = 1000000;
$image_types = Array 
(
		array("image/jpeg","jpg"),
		array("image/pjpeg","jpg"),
		array("image/bmp","bmp"),
		array("image/gif","gif"),
		array("image/x-png","png")
);	
$USE_GD= false;
$ADDITIONAL_IMAGES_COLUMNS_PER_LINE = 8;
$EMAIL_FROM="info@domain.com";
$NAME_FROM="YOUR NAME";
$CONTACT_EMAIL_SUBJECT="Real Estate Portal message";
$SEND_WELCOME_MESSAGES = false;
$PRIVATE_SELLER_SUBJECT = "seller subject";
$PRIVATE_SELLER_MESSAGE = "seller message";
$AGENT_SIGNUP_SUBJECT = "agent subject";
$AGENT_SIGNUP_MESSAGE = "agent message";
$AFFILIATE_SIGNUP_SUBJECT = "affiliate subject";
$AFFILIATE_SIGNUP_MESSAGE = "affiliate message";
$SEND_EMAIL_NOTIFICATIONS = true;
$NEW_PROPERTY_EMAIL_ALERT_SUBJECT = "New property listed";
$NEW_PROPERTY_EMAIL_ALERT_MESSAGE = "New property meeting your search criteria was listed. Please click on the following link: [LINK]";
$USE_SECURITY_IMAGES = true;
$CAPTCHA_SALT="RE4AX42FQ";
$ADMIN_TEMPLATE_ID=13;
$RESULTS_PER_PAGE = 10;
$FREE_WEBSITE = false;
$PHP_DATE_FORMAT="d/m h:iA";
$WEBSITE_MULTILANGUAGE = true;
$USE_MOD_REWRITE= false;
$AUTO_VALIDATE_AGENT_ADS = true;
$ENABLE_AUTHORIZE_NET_AIM_PAYMENTS = false;
$LOGIN_ID="";
$TRANSACTION_KEY="";
$ENABLE_GOOGLE_MAPS=true;
$GOOGLE_MAPS_KEY="";
$FEATURED_ADS_EXPIRE=30;
$PRICE_FORMAT_THOUSANDS_SEP=",";
$ASK_FOR_ZIP = true;
$ENABLE_ZIP_SEARCH=  true;
$ALLOW_VIDEO = true;
$MAX_FILE_SIZE = 2000000;
$USE_FFMPEG=false; 
$file_types = Array 
(
		array("video/x-ms-wmv","wmv"),
		array("video/mpeg","mpg"),
		array("video/x-msvideo","avi"),
		array("video/avi","avi")
);
$AJAX_LOADING=false;
$AUTHORIZED_IPS_ADMIN_PANEL="";
$SEND_EMAIL_REMINDER_ON_EXPIRED_ADS = false;
$REMINDER_EMAIL_SUBJECT="Your ad has expired";
$REMINDER_EMAIL_TEXT=
			"Your ad posted on realestate23.com has expired.\n
			To see the property or modify it, you may use the link below:\n
			[MODIFY_LINK] \n
			If you would like to renew it, please click on the following link:\n
			[RENEW_LINK]";
?>