<?php
// Deals Portal, http://www.netartmedia.net/dealsportal
// A software product of NetArt Media, All Rights Reserved
// Find out more about our products and services on:
// http://www.netartmedia.net
?>
<?php
$str_images_list="";

if(!isset($_FILES[isset($input_field)?$input_field:'images']))
{

}
else
if(isset($_FILES))
		{
		
			$files=array();
			$fdata=$_FILES[$file_field_name];
			if(is_array($fdata['name']))
			{
				for($i=0;$i<count($fdata['name']);++$i)
				{
					if(trim($fdata['name'][$i])=="") continue;
					
					$size	= GetImageSize($fdata['tmp_name'][$i]);
					$mime	= $size['mime'];
					
					if (substr($mime, 0, 6) != 'image/') continue;


					$files[]=array
					(
						'name'    =>$fdata['name'][$i],
						'type'  => $fdata['type'][$i],
						'tmp_name'=>$fdata['tmp_name'][$i],
						'mime' => $mime, 
						'size'  => $fdata['size'][$i]  
					);
				}
			}else $files[]=$fdata;
		
			
			$is_first_image = true;
			
			foreach ($files as $file) 
			{ 
				if(trim($file['tmp_name'])=="") continue;
				
				$i_random=rand(200,100000000);
	
				$save_file_name = (isset($path)?$path:"")."uploaded_images/" .$i_random.".jpg";
			
				 list($width, $height) = getimagesize($file['tmp_name']) ; 

				if($width < 500)
				{
					$modwidth = $width;
				}
				else
				{
					$modwidth = 500; 
				}
				
				
				$diff =  $modwidth / $width;
				
				$modheight = $height * $diff; 
				
				
				$tn = imagecreatetruecolor($modwidth, $modheight) ; 
				
				
				$file_mime="";
				if(isset($file['mime']))
				{
					$file_mime=$file['mime'];
				}
				else
				if(isset($file['type']))
				{
					$file_mime=$file['type'];
				}
				
				
				switch ($file_mime)
				{
					case 'image/gif':
						$creationFunction	= 'ImageCreateFromGif';
						$outputFunction		= 'ImagePng';
						$mime				= 'image/png'; // We need to convert GIFs to PNGs
					break;
					
					case 'image/x-png':
					case 'image/png':
						$creationFunction	= 'ImageCreateFromPng';
						$outputFunction		= 'ImagePng';
					break;
					
					default:
						$creationFunction	= 'ImageCreateFromJpeg';
						$outputFunction	 	= 'ImageJpeg';
					
					break;
				}

				$image	= $creationFunction($file['tmp_name']);

				
				imagecopyresampled($tn, $image, 0, 0, 0, 0, $modwidth, $modheight, $width, $height) ; 
				imagejpeg($tn, $save_file_name, 70) ; 
				
				//thumbnails generation
					if($width < 100)
					{
						$thumb_width = $width;
					}
					else
					{
						$thumb_width = 100; 
					}
					$thumb_diff = $thumb_width / $width;
					$thumb_height = $height * $thumb_diff; 
					$thumb = imagecreatetruecolor($thumb_width, $thumb_height) ; 
					
					imagecopyresampled($thumb, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height) ; 
					imagejpeg($thumb, (isset($path)?$path:"")."thumbnails/" .$i_random.".jpg", 90) ; 
					
				//end thumbnails
			
				
				if($str_images_list!="")
				{
					$str_images_list.=",";
				}
				
				$str_images_list.=$i_random;
				
				$is_first_image = false;
			}
		}
?>