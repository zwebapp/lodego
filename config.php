<?php
$DOMAIN_NAME = "www.lodego.dev";

//MYSQL DATABASE ACCESS SETTINGS
$DBHost="localhost";
$DBUser="root";
$DBPass="";
$DBName="lodego";
$DBprefix="websiteadmin_";

$DEBUG_MODE=false;
$MULTI_LANGUAGE_SITE = false;
$AdminPanelLanguages=
array
(
	array("English","en")
);
?>